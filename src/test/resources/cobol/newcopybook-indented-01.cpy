01  ECR_REC.                                                                    
   05  ECR_RECORD.                                                              
       10  ECR_RECORD_TYPE                          PIC X(04).                  
           88 88_E1_HSP_REC                         VALUE 'ECR1'.               
           88 88_E2_CLAIM_REC                       VALUE 'ECR2'.               
           88 88_E3_DETAIL_REC                      VALUE 'ECR3'.               
       10  ECR_RECORD_KEY.                                                      
           15  ECR_CURRENT_DT_CCYYMMDD              PIC 9(08).                  
           15  ECR_CURRENT_TIME_HHMMSSHH            PIC 9(08).                  
           15  ECR_RECORD_NUM                       PIC 9(03).                  
       10  FILLER_001                               PIC X(8391).                
       10  ECR_SORT2_KEY.                                                       
           15  FILLER_002                           PIC X(07).                  
           15  ECR_SORT2_TPA_VENDOR                 PIC X(05).                  
           15  ECR_SORT2_HDR_TLR                    PIC X(01).                  
           15  FILLER_003                           PIC X(16).                  
       10  ECR_SORT_KEY.                                                        
           15  ECR_SORT_DOC_NO                      PIC 9(15).                  
           15  ECR_SORT_RECORD_TYPE                 PIC X(02).                  
           15  ECR_SORT_SEQ_NO                      PIC 9(03).                  
           15  ECR_SORT_RECORD_NUMBER               PIC 9(03).                  
           15  ECR_SORT_FORM_TYPE                   PIC X(01).                  
           15  ECR_SORT_SITE_NO                     PIC X(05).                  
           15  ECR_SORT_PLAN_NO                     PIC X(15).                  
           15  ECR_SORT_PRV_ID                      PIC X(09).                  
           15  ECR_SORT_FLD_OFC                     PIC X(03).                  
           15  ECR_SORT_PAPER_ELEC                  PIC X(01).                  
01  E1_HSP_REC REDEFINES ECR_REC.                                               
   05  E1_HSP_RECORD.                                                           
       10  FILLER_004                               PIC X(23).                  
       10  E1_EC_AREA.                                                          
           15  E1_DOC_NO                            PIC X(20).                  
           15  E1_CLAIM_STATUS                      PIC X(02).                  
               88  88_E1_CS_THROUGH_FORMAT          VALUE '01'.                 
               88  88_E1_CS_THROUGH_CLRHOUSE        VALUE '02'.                 
               88  88_E1_CS_THROUGH_SPLIT           VALUE '03'.                 
               88  88_E1_CS_DIVERTED_SUBCTL         VALUE '10'.                 
               88  88_E1_CS_DIVERTED_CPR            VALUE '11'.                 
               88  88_E1_CS_DIVERTED_PMMS           VALUE '12'.                 
               88  88_E1_CS_DIVERTED_PRV            VALUE '13'.                 
               88  88_E1_CS_DIVERTED_QPM            VALUE '14'.                 
               88  88_E1_CS_DIVERTED_SOC            VALUE '15'.                 
               88  88_E1_CS_LOADED_PLUS             VALUE '20'.                 
               88  88_E1_CS_LOADED_PERCENT          VALUE '21'.                 
               88  88_E1_CS_LOADED_I                VALUE '22'.                 
               88  88_E1_CS_LOADED_V                VALUE '23'.                 
               88  88_E1_CS_LOADED_R                VALUE '24'.                 
               88  88_E1_CS_LOADED_Q                VALUE '25'.                 
               88  88_E1_CS_NOT_LOADED              VALUE '30'.                 
               88  88_E1_CS_NOT_LOADED_CPR_PRT      VALUE '31'.                 
               88  88_E1_CS_NOT_LOADED_PMMS_PRT     VALUE '32'.                 
               88  88_E1_CS_NOT_LOADED_PRV_PRT      VALUE '33'.                 
               88  88_E1_CS_NOT_LOADED_PRI_SCR      VALUE '34'.                 
               88  88_E1_CS_REJECTED                VALUE '40'.                 
               88  88_E1_CS_REJECTED_CPR            VALUE '41'.                 
               88  88_E1_CS_REJECTED_CLRHOUSE       VALUE '42'.                 
               88  88_E1_CS_ROUTED_CLRHOUSE         VALUE '50'.                 
               88  88_E1_CS_ROUTED_CLRHOUSE_P       VALUE '51'.                 
               88  88_E1_CS_ROUTED_CLRHOUSE_E       VALUE '52'.                 
               88  88_E1_CS_ROUTED_CPR              VALUE '53'.                 
               88  88_E1_CS_ROUTED_CPR_P            VALUE '54'.                 
               88  88_E1_CS_ROUTED_CPR_E            VALUE '55'.                 
               88  88_E1_CS_ROUTED_LOAD             VALUE '56'.                 
               88  88_E1_CS_ROUTED_LOAD_P           VALUE '57'.                 
               88  88_E1_CS_ROUTED_LOAD_E           VALUE '58'.                 
               88  88_E1_CS_DIVERTED_CLAIM          VALUES '10'                 
                                                      THRU '19'.                
               88  88_E1_CS_LOADED_CLAIM            VALUES '20'                 
                                                      THRU '29'.                
               88  88_E1_CS_NOT_LOADED_CLAIM        VALUES '30'                 
                                                      THRU '39'.                
               88  88_E1_CS_REJECTED_CLAIM          VALUES '40'                 
                                                      THRU '49'.                
               88  88_E1_CS_ROUTED_CLAIM            VALUES '50'                 
                                                      THRU '59'.                
           15  E1_OLD_DOC_NUM                       PIC X(20).                  
           15  E1_CLAIM_SOURCE                      PIC X(03).                  
           15  FILLER REDEFINES E1_CLAIM_SOURCE.                                
               20 E1_ORIG_SOURCE                    PIC X(02).                  
               20 E1_RCV_IND                        PIC X(01).                  
           15  E1_FORM_TYPE                         PIC X(01).                  
           15  FILLER_006                           PIC X(04).                  
           15  E1_ERR_CNT                           PIC 9(02).                  
           15  E1_ERR_CODES OCCURS 5 TIMES          PIC 9(05).                  
           15  E1_L_CODE                            PIC X(01).                  
           15  E1_DOS_LIMIT_IND                     PIC X(01).                  
           15  E1_DOS_CNT                           PIC 9(02).                  
           15  E1_DOS_AREA  OCCURS 10 TIMES                                     
                            INDEXED BY E1_DOS_NDX.                              
               20  E1_FROM_DOS                      PIC X(08).                  
           15  E1_CAS_LOCATION                      PIC X(01).                  
               88 88_E1_PRV_CAS   VALUE 'P'.                                    
               88 88_E1_DET_CAS   VALUE 'D'.                                    
               88 88_E1_BOTH_CAS  VALUE 'B'.                                    
               88 88_E1_NO_CAS    VALUE ' '         ,LOW_VALUES.                         
           15  FILLER_007                           PIC X(21).                  
           15  E1_MULTIPLE_RECORDS                  PIC X(01).                  
           15  E1_CPR_ADD_PAT                       PIC X(01).                  
           15  E1_REJECT_CODE                       PIC X(07).                  
           15  E1_REJECT_CD_IND                     PIC X(01).                  
           15  E1_TRACK                             PIC X(01).                  
           15  E1_TRACK_DT                          PIC 9(07).                  
           15  FILLER_008                           PIC X(05).                  
           15  E1_HEADER_FILE_CONTROL_NO            PIC 9(15).                  
           15  E1_LOAD_PROCESS_INDICATOR            PIC X(02).                  
           15  E1_SWEEP_PROCESS_INDICATOR           PIC X(02).                  
           15  E1_INVALID_AMOUNT_INDICATOR          PIC X(01).                  
           15  FILLER_009                           PIC X(174).                 
       10  E1_PROCLAIM_AREA.                                                    
           15  E1_INS_GRP_ID_NO                     PIC X(20).                  
           15  E1_OP_LOC                            PIC X(13).                  
           15  FILLER REDEFINES E1_OP_LOC.                                      
               20 E1_SOC                            PIC X(05).                  
               20 FILLER_011                        PIC X(08).                  
           15  E1_FLD_OFC                           PIC X(03).                  
           15  E1_PROCLAIM_PAT_REL                  PIC X.                      
           15  E1_POLICY                            PIC X(08).                  
           15  E1_SCHEDULE                          PIC X(08).                  
           15  E1_CLASS                             PIC X(04).                  
           15  E1_TYPE_CERT                         PIC X(01).                  
           15  E1_ASSOC_NO                          PIC X(04).                  
           15  E1_TYPE_OF_EIN                       PIC X(01).                  
           15  FILLER_012                           PIC X(40).                  
           15  E1_PREDETERMINATION_IND              PIC X(01).                  
           15  E1_CERT_OPLOC                        PIC X(13).                  
           15  E1_TLC                               PIC X(03).                  
           15  E1_PRVMTCH_IND_RETURN_CODE           PIC X(04).                  
           15  E1_PRVMTCH_COMB_CD                   PIC X(01).                  
           15  E1_PRVMTCH_STATUS                    PIC X(01).                  
           15  E1_PRVMTCH_HS_STATUS                 PIC X(01).                  
           15  E1_PRVMTCH_PRV_TYPE                  PIC 9(02).                  
           15  E1_NOT_COV_AMT_IND                   PIC X(01).                  
           15  E1_GCEMPDOC_ACC_SICK                 PIC X(01).                  
           15  E1_CROSS_YEARS                       PIC X(01).                  
           15  E1_PAPER_ELEC_IND                    PIC X(01).                  
               88 88_E1_PAPER_CLAIM                 VALUE 'P'.                  
               88 88_E1_ELEC_CLAIM                  VALUE 'E'.                  
           15  E1_PRVMTCH_ASSN_INACTIVE             PIC X(01).                  
           15  E1_PRVMTCH_GRP_RETURN_CODE           PIC X(04).                  
           15  E1_PRVMTCH_GRP_ASSOC                 PIC X(04).                  
           15  E1_IRS_DOCPRV_NEEDED                 PIC X(01).                  
           15  E1_PR_DOCPRV_NEEDED                  PIC X(01).                  
           15  FILLER_013                           PIC X(15).                  
           15  E1_CHS_PAY_LOC                       PIC X(03).                  
           15  E1_PRVMTCH_IND_SURCHARGE             PIC X(03).                  
           15  E1_PRVMTCH_GRP_SURCHARGE             PIC X(03).                  
           15  E1_CLM_SPR_RATE                      PIC 9(08)V99.               
           15  E1_ENDSTATE_IND                      PIC X(01).                  
           15  E1_REMARKS_IND                       PIC X(01).                  
           15  E1_CLM_COB_IND                       PIC X(01).                  
           15  E1_DET_COB_IND                       PIC X(01).                  
           15  E1_CAS_FIELDS    OCCURS 2 TIMES.                                 
               20 E1_CLM_LIMITING_CHARGE            PIC 9(08)V99.               
               20 E1_CLM_DEDUCTIBLE_AMOUNT          PIC 9(08)V99.               
               20 E1_CLM_DED_MAJ_MED_AMOUNT         PIC 9(08)V99.               
           15  E1_UNITS_IND                         PIC X(01).                  
           15  E1_NDC_IND                           PIC X(01).                  
           15  E1_DET_DED_IND                       PIC X(01).                  
           15  E1_DET_LIMITING_IND                  PIC X(01).                  
           15  E1_DET_COV_IND                       PIC X(01).                  
           15  E1_ELGSUB_RETURN_CODE                PIC X(04).                  
           15  E1_ELGSUB_RETURN_ELG_ERR             PIC 99.                     
           15  E1_PLACE_SERVICE_ALL_11              PIC X.                      
           15  E1_AMI_TRANSLATOR_RC                 PIC X(2).                   
           15  E1_TLC_DATE_FLAG                     PIC X(1).                   
           15  E1_SURCHARGE_AMOUNT                PIC S9(07)V99.                
           15  FILLER_014                           PIC X(10).                  
           15  E1_PMMS_SEQ_NUMBER_I                 PIC 9(04).                  
           15  E1_PMMS_SEQ_NUMBER_G                 PIC 9(04).                  
           15  E1_PRVSELC_BYPASS_GRP_IND            PIC X(01).                  
           15  E1_PRVSELC_BYPASS_INDV_IND           PIC X(01).                  
           15  FILLER_015                           PIC X(14).                  
       10  E1_CLEARINGHOUSE_AREA.                                               
           15  E1_CHS_TRACKING_NUM                  PIC X(15).                  
           15  FILLER REDEFINES E1_CHS_TRACKING_NUM.                            
               20  E1_CHS_DATE_PROC                 PIC X(07).                  
               20  FILLER_017                       PIC X(08).                  
           15  FILLER REDEFINES E1_CHS_TRACKING_NUM.                            
               20  E1_CHS_TRACKING_NO               PIC X(15).                  
           15  E1_CHS_CLM_REF_NUM                   PIC X(15).                  
           15  E1_CHS_SUBMITTER_DCN                 PIC X(32).                  
           15  E1_CHS_SUBMITTING_SITE               PIC X(05).                  
           15  E1_CHS_REJECT_CODE                   PIC X(07).                  
           15  E1_CHS_REJECT_DT                     PIC X(08).                  
           15  E1_CHS_PLAN_NO                       PIC X(15).                  
           15  FILLER_019                           PIC X(15).                  
           15  E1_CHS_MEMBER_ID                     PIC X(15).                  
           15  E1_CHS_FRM_DT                        PIC X(08).                  
           15  E1_CHS_CPR_SITE                      PIC X(03).                  
           15  FILLER_020                           PIC X(34).                  
       10  E1_5010_E_CODE_COUNT                     PIC 9(03).                  
       10  E1_PAT_REASON_FOR_VISIT_COUNT            PIC 9(03).                  
       10  E1_5010_E_CODE_AREA OCCURS 12 TIMES.                                 
           15 E1_5010_E_CODE                       PIC X(12).                   
           15 E1_E_5010_CODE_ICD_VERSION           PIC 99.                      
           15 E1_E_5010_CODE_POA_IND               PIC X.                       
       10  E1_PAT_REASON_FOR_VISIT_AREA                                         
             OCCURS 3 TIMES.                                                    
           15  E1_PAT_REASON_FOR_VISIT_CODE        PIC X(12).                   
           15  E1_PAT_REASON_ICD_VERSION           PIC 99.                      
       10  E1_TPO_AREA.                                                         
           15  E1_TPO_AREA_USED                     PIC X(01).                  
           15  E1_TPO_SOURCE                        PIC X(03).                  
           15  E1_TPO_FREE_FORM_REMARKS             PIC X(50).                  
           15  FILLER_021                           PIC X(07).                  
       10  E1_5010_OTHER_DIAGNOSIS_COUNT            PIC 9(03).                  
       10  E1_5010_OTHER_DIAGNOSIS_AREA                                         
             OCCURS 24 TIMES.                                                   
           15  E1_501O_OTHER_DIAGNOSIS_CODE         PIC X(12).                  
           15  E1_5010_OTHER_DIAG_ICD_VERSION       PIC 99.                     
           15  E1_5010_OTHER_DIAG_POA_IND           PIC X.                      
       10  E1_FIRST_IMAGE_AREA.                                                 
           15  E1_FST_CLM_FORM_DT                   PIC 9(06).                  
           15  E1_FST_FLEX_IND                      PIC X(01).                  
           15  E1_FST_FLEX_AMT                      PIC 9(06)V99.               
           15  E1_FST_COB_IND                       PIC X(01).                  
           15  E1_FST_COB_YEAR                      PIC X(02).                  
           15  E1_FST_ORTHO_IND                     PIC X(01).                  
           15  E1_FST_TMJ_IND                       PIC X(01).                  
           15  E1_FST_ATTACH_IND                    PIC X(01).                  
           15  E1_FST_X_PROC_IND                    PIC X(01).                  
           15  E1_FST_EMP_SIGN                      PIC X(01).                  
           15  E1_FST_EMP_XRAY                      PIC X(01).                  
           15  E1_FST_PROS_NEW_OLD                  PIC X(01).                  
           15  E1_FST_PROS_REPL_RSN                 PIC X(01).                  
           15  E1_FST_PROS_REPL_DT                  PIC 9(08).                  
           15  E1_FST_NARRIATIVE                    PIC X(01).                  
           15  E1_FST_PPO_CHG_IND                   PIC X(01).                  
           15  E1_FST_PPO_CHA_FLAG                  PIC X(02).                  
           15  E1_FST_PPO_ID                        PIC 9(09).                  
           15  E1_FST_AUTO_PCT_IND                  PIC X(01).                  
           15  E1_FST_AUTO_AMT                      PIC 9(05)V99.               
           15  E1_FST_PRV_TYPE                      PIC 9(04).                  
           15  E1_FST_DOC_APPROVED_AMT              PIC 9(05)V99.               
           15  E1_FST_DOC_PAID_AMT                  PIC 9(05)V99.               
           15  E1_FST_DOC_DISALLOW_AMT              PIC 9(05)V99.               
           15  E1_FST_DOC_REASON_CD                 PIC X(01).                  
           15  E1_FST_ACCT_DIVERT                   PIC X(05).                  
           15  E1_FST_MAIL_ROOM_DT                  PIC X(08).                  
           15  E1_FST_DOC_DEDUCT_AMT                PIC 9(05)V99.               
           15  E1_FST_HSA_IND                       PIC X(01).                  
           15  E1_ATTACH_IND2                       PIC X(01).                  
           15  E1_APPEAL_IND                        PIC X(01).                  
           15  E1_EOB_IND                           PIC X(01).                  
           15  FILLER_022                           PIC X(88).                  
       10  E1_LOOP_2310_AMBULANCE_INFO.                                         
           15  E1_AMBULANCE_PICK_UP_LOC.                                        
              20  E1_AMBULANCE_PICK_UP_ADDR1        PIC X(55).                  
              20  E1_AMBULANCE_PICK_UP_ADDR2        PIC X(55).                  
              20  E1_AMBULANCE_PICK_UP_CITY         PIC X(30).                  
              20  E1_AMBULANCE_PICK_UP_STATE        PIC X(02).                  
              20  E1_AMBULANCE_PICK_UP_ZIP          PIC 9(15).                  
           15  E1_AMBULANCE_DROP_OFF_LOC.                                       
              20  E1_AMBULANCE_DROP_OFF_ADDR1       PIC X(55).                  
              20  E1_AMBULANCE_DROP_OFF_ADDR2       PIC X(55).                  
              20  E1_AMBULANCE_DROP_OFF_CITY        PIC X(30).                  
              20  E1_AMBULANCE_DROP_OFF_STATE       PIC X(02).                  
              20  E1_AMBULANCE_DROP_OFF_ZIP         PIC 9(15).                  
           15  E1_AMBULANCE_TRANSPORT_DIST          PIC 9(15).                  
       10  E1_ANES_RELATED_COUNT                    PIC 9(03).                  
       10  E1_ANES_RELATED_AREA                                                 
             OCCURS 2 TIMES.                                                    
           15  E1_ANES_CODE                         PIC X(12).                  
           15  E1_ANES_CODE_QUAL                    PIC X(03).                  
       10  E1_AMBULANCE_CERTIFICATION.                                          
           15  E1_AMBULANCE_CERT_CODE1              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE2              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE3              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE4              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE5              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE6              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE7              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE8              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE9              PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE10             PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE11             PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE12             PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE13             PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE14             PIC X(03).                  
           15  E1_AMBULANCE_CERT_CODE15             PIC X(03).                  
           15  E1_AMBULANCE_EMERGENCY_IND           PIC X.                      
       10  E1_MEDICARE_AREA.                                                    
           15  E1_PATIENT_HIC_NUMBER                PIC X(12).                  
           15  E1_CLAIM_MEDICARE_CLAIM_NO           PIC X(17).                  
           15  FILLER_023                           PIC X(369).                 
       10  E1_5010_COND_CODE_COUNT                  PIC 9(03).                  
       10  E1_PWK_AREA_COUNT                        PIC 9(03).                  
       10  E1_5010_COND_CODE                                                    
                 OCCURS 24 TIMES.                                               
           15  E1_5010_COND_CODE_DATA               PIC X(12).                  
       10  E1_PWK_AREA                                                          
             OCCURS 10 TIMES.                                                   
           15  E1_DOCUMENT_TYPE                     PIC X(02).                  
           15  E1_REPORT_TRANMISSION_CODE           PIC X(02).                  
           15  E1_ATTACHMENT_CONTROL_NUM            PIC X(20).                  
       10  E1_MEDICAID_AREA.                                                    
           15  E1_MCD_CODE                          PIC X(06).                  
           15  E1_MCD_TAX_ID                        PIC X(09).                  
           15  FILLER_024                           PIC X(03).                  
           15  E1_MCD_AMT                           PIC 9(07)V99.               
           15  FILLER_025                           PIC X(18).                  
       10  E1_5010_OTHER_PROC_COUNT                 PIC X(03).                  
       10  E1_5010_OTHER_PROC                                                   
         OCCURS 24 TIMES.                                                       
           15  E1_5010_OTHER_PROC_CODE          PIC X(10).                      
           15  E1_5010_OTHER_PROC_CODE_QUAL     PIC X(03).                      
           15  E1_5010_OTHER_PROC_DT            PIC X(08).                      
           15  E1_5010_OTHER_PROC_CCYYMMDD REDEFINES                            
               E1_5010_OTHER_PROC_DT.                                           
               20  E1_5010_OTHER_PROC_CC        PIC X(02).                      
               20  E1_5010_OTHER_PROC_YYMMDD    PIC X(06).                      
           15  E1_5010_OTHER_PROC_ICD_VER       PIC 99.                         
       10  E1_CPR_UPD_AREA OCCURS 5 TIMES.                                      
           15  E1_CPR_UPD_UPDATED_YN                PIC X(01).                  
           15  E1_CPR_UPD_UPDATED_TYPE              PIC X(01).                  
           15  E1_CPR_UPD_PROV_SUB_ID               PIC X(04).                  
           15  E1_CPR_UPD_PAT_NAME_F                PIC X(25).                  
           15  E1_CPR_UPD_PAT_SEX                   PIC X(01).                  
           15  E1_CPR_UPD_PAT_BRTH_DT               PIC X(08).                  
           15  E1_CPR_UPD_PAT_REL                   PIC X(02).                  
           15  E1_CPR_UPD_PAT_MEMB                  PIC X(15).                  
           15  E1_CPR_UPD_PAT_SITE                  PIC X(05).                  
           15  E1_CPR_UPD_INS_SSN                   PIC X(09).                  
           15  E1_CPR_UPD_OP_LOC                    PIC X(13).                  
           15  E1_CPR_UPD_TYPE_CERT                 PIC X(01).                  
           15  E1_CPR_UPD_FLD_OFC                   PIC X(03).                  
           15  E1_CPR_UPD_REJECTION_CODE            PIC X(02).                  
           15  E1_CPR_UPD_POS_FLAG                  PIC X(01).                  
           15  E1_CPR_UPD_OPERATOR_ID               PIC X(07).                  
           15  E1_CPR_UPD_UPDATED_DATE              PIC X(07).                  
           15  E1_CPR_UPD_UPDATED_TIME              PIC X(08).                  
           15  E1_CPR_ERROR                         PIC X(05).                  
       10  E1_GCDOC_DOC_CAUSE_AREA.                                             
           15  E1_GCDOC_CAUSES  OCCURS 10 TIMES.                                
               20 E1_GCDOC_DOC_CAUSE                PIC X(05).                  
           15  FILLER_026                           PIC X(353).                 
       10  E1_CAS_CLM_ONE OCCURS 5 TIMES.                                       
           15  E1_ADJ_CLM_GROUP_CODEA               PIC X(02).                  
           15  E1_ADJ_CLM_GROUP_AREAA OCCURS 6 TIMES.                           
               20 E1_ADJ_CLM_REASON_CODEA           PIC X(05).                  
               20 E1_ADJ_CLM_AMOUNTA                PIC 9(08)V99.               
               20 E1_ADJ_CLM_QUANTITYA              PIC 9(15).                  
       10  E1_CAS_CLM_TWO OCCURS 5 TIMES.                                       
           15  E1_ADJ_CLM_GROUP_CODEB               PIC X(02).                  
           15  E1_ADJ_CLM_GROUP_AREAB OCCURS 6 TIMES.                           
               20 E1_ADJ_CLM_REASON_CODEB           PIC X(05).                  
               20 E1_ADJ_CLM_AMOUNTB                PIC 9(08)V99.               
               20 E1_ADJ_CLM_QUANTITYB              PIC 9(15).                  
       10  E1_CASA_CLM_COUNT                        PIC 9(03).                  
       10  E1_CASB_CLM_COUNT                        PIC 9(03).                  
       10  E1_PROVIDER_STD_AREA.                                                
           15  E1_STD_BILL_PRV_AREA.                                            
               20 E1_STD_BILL_PRV_LNAME             PIC X(35).                  
               20 E1_STD_BILL_PRV_FNAME             PIC X(25).                  
               20 E1_STD_BILL_PRV_ADDR_1            PIC X(35).                  
               20 E1_STD_BILL_PRV_ADDR_2            PIC X(35).                  
               20 E1_STD_BILL_PRV_CITY              PIC X(30).                  
               20 E1_STD_BILL_PRV_STATE             PIC X(02).                  
               20 E1_STD_BILL_PRV_ZIP               PIC X(11).                  
               20 E1_STD_INVALID_BILL_NAME          PIC X(01).                  
               20 E1_STD_INVALID_BILL_ZIP           PIC X(01).                  
               20 E1_STD_INVALID_BILL_ADR           PIC X(01).                  
               20 E1_STD_INV_STD_BILL_ADR           PIC X(01).                  
               20 E1_STD_FIN_BILL_CODES.                                        
                  25  E1_STD_FIN_BILL_OUTSET        PIC X(03).                  
                  25  E1_STD_FIN_BILL_RETURN_CD     PIC X(02).                  
                  25  E1_STD_FIN_BILL_ADDR_INFO     PIC X(10).                  
                  25  E1_STD_FIN_BILL_REASON_CDS    PIC X(12).                  
           15  E1_STD_REND_PRV_AREA.                                            
               20 E1_STD_REND_PRV_LNAME             PIC X(35).                  
               20 E1_STD_REND_PRV_FNAME             PIC X(25).                  
               20 E1_STD_REND_PRV_MI                PIC X(1).                   
               20 E1_STD_REND_PRV_GENERATION        PIC X(4).                   
               20 E1_STD_REND_PRV_ADDR_1            PIC X(35).                  
               20 E1_STD_REND_PRV_ADDR_2            PIC X(35).                  
               20 E1_STD_REND_PRV_CITY              PIC X(30).                  
               20 E1_STD_REND_PRV_STATE             PIC X(02).                  
               20 E1_STD_REND_PRV_ZIP               PIC X(11).                  
               20 E1_STD_INVALID_REND_NAME          PIC X(01).                  
               20 E1_STD_INVALID_REND_ZIP           PIC X(01).                  
               20 E1_STD_INVALID_REND_ADR           PIC X(01).                  
               20 E1_STD_INV_STD_REND_ADR           PIC X(01).                  
               20 E1_STD_FIN_REND_CODES.                                        
                  25  E1_STD_FIN_REND_OUTSET        PIC X(03).                  
                  25  E1_STD_FIN_REND_RETURN_CD     PIC X(02).                  
                  25  E1_STD_FIN_REND_ADDR_INFO     PIC X(10).                  
                  25  E1_STD_FIN_REND_REASON_CDS    PIC X(12).                  
           15  E1_STD_REND_FAC_AREA.                                            
               20 E1_STD_REND_FAC_NAME              PIC X(35).                  
               20 E1_STD_REND_FAC_ADDR_1            PIC X(35).                  
               20 E1_STD_REND_FAC_ADDR_2            PIC X(35).                  
               20 E1_STD_REND_FAC_CITY              PIC X(30).                  
               20 E1_STD_REND_FAC_STATE             PIC X(02).                  
               20 E1_STD_REND_FAC_ZIP               PIC X(11).                  
               20 E1_STD_INV_REND_FAC_NAME          PIC X(01).                  
               20 E1_STD_INV_REND_FAC_ZIP           PIC X(01).                  
               20 E1_STD_INV_REND_FAC_ADR           PIC X(01).                  
               20 E1_STD_INV_STD_FAC_ADR            PIC X(01).                  
               20 E1_STD_FIN_FAC_CODES.                                         
                  25  E1_STD_FIN_FAC_OUTSET         PIC X(03).                  
                  25  E1_STD_FIN_FAC_RETURN_CD      PIC X(02).                  
                  25  E1_STD_FIN_FAC_ADDR_INFO      PIC X(10).                  
                  25  E1_STD_FIN_FAC_REASON_CDS     PIC X(12).                  
       10  E1_CARELINK_INFO_CLAIM.                                              
           15  E1_TPO_EDI_CLAIM_NUM                 PIC X(30).                  
           15  E1_CIGNA_UNIQUE_CLAIM_NUM            PIC X(30).                  
           15  E1_INITIAL_CLAIM_RCPT_DT             PIC X(08).                  
           15  E1_CARELINK_ATTACH_IND               PIC X(02).                  
           15  E1_ADJUSTED_CLAIM_IND_REASON         PIC X(04).                  
           15  E1_TPO_CLAIM_DRG_CODE                PIC X(04).                  
           15  E1_TPO_PRICING_METHOD_CODE           PIC X(02).                  
           15  E1_CLAIM_TRANS_TYPE                  PIC X(03).                  
           15  E1_CLAIM_DISPO_DT                    PIC X(08).                  
           15  E1_CLAIM_DISPO_CODE                  PIC X(04).                  
           15  E1_CLAIM_DISPO_REASON_CODE           PIC X(05).                  
           15  E1_RTE_TO_LOC_CD                     PIC X(04).                  
           15  E1_EXTL_RTE_TO_LOC_CD                PIC X(04).                  
           15  E1_EXTL_RTE_FROM_LOC_CD              PIC X(04).                  
           15  E1_EXTL_RTNG_REQ_IND                 PIC X(01).                  
           15  E1_CLAIM_TOT_PD_AMT                  PIC X(13).                  
           15  E1_TPO_CLAIM_NUMBER                  PIC X(30).                  
           15  FILLER_027                           PIC X(97).                  
       10 E1_CLAIM_QUANTITY.                                                    
           15  E1_CQ_COVERED_DAYS                   PIC X(04).                  
           15  E1_CQ_COINSURANCE_DAYS               PIC X(04).                  
           15  E1_CQ_LIFETIME_RESERVE_DAYS          PIC X(04).                  
           15  E1_CQ_NON_COVERED_DAYS               PIC X(04).                  
       10  E1_ALLIANCE_DATA.                                                    
           15  E1_EMPLE_MEMBER_OF_ALLIANCE          PIC X(01).                  
           15  E1_EMPLE_PRNT_NT_ID                  PIC X(05).                  
           15  E1_EMPLE_PARTY_ID                    PIC X(05).                  
           15  E1_CHC_ROLE_CD                       PIC X(02).                  
           15  E1_ACCOUNT_CP_ID                     PIC X(05).                  
           15  E1_SERVICE_FROM_DATE                 PIC X(08).                  
           15  E1_SERVICE_TO_DATE                   PIC X(08).                  
       10  E1_SHARED_ADMIN_DATA.                                                
           15  E1_SHARED_ADMIN_CP_ENGINE            PIC X(03).                  
           15  E1_PPR_ENDSTATE_ELIG_IND             PIC X(01).                  
           15  E1_SHARED_ADMIN_LIMIT_ROUTE          PIC X(01).                  
           15  E1_SHARED_ADMIN_ECR_FLAG             PIC X(01).                  
           15  E1_SHARED_ADMIN_ACCEPT_277           PIC X(01).                  
       10  E1_ELGSUB_ALLIANCE_DATA.                                             
           15  E1_ELGSUB_ALLIANCE_ID                PIC X(01).                  
           15  E1_ELGSUB_EMPLE_PRNT_NT_ID           PIC X(05).                  
           15  E1_ELGSUB_EMPLE_PARTY_ID             PIC X(05).                  
           15  E1_ELGSUB_CHC_ROLE_CD                PIC X(02).                  
           15  E1_ELGSUB_ACCOUNT_CP_ID              PIC X(05).                  
       10  E1_RECEIVE_DATE.                                                     
           15  E1_RECEIVE_DATE_CC                   PIC 9(02).                  
           15  E1_RECEIVE_DATE_YY                   PIC 9(02).                  
           15  E1_RECEIVE_DATE_MM                   PIC 9(02).                  
           15  E1_RECEIVE_DATE_DD                   PIC 9(02).                  
       10  E1_TPA_PPR_FOUND_SW                      PIC X(01).                  
           88 E1_TPA_PPR_FOUND                       VALUE 'Y'.                 
           88 E1_TPA_PPR_NOT_FOUND                   VALUE 'N'.                 
       10  E1_SHARED_ADMIN_PRINT_FLAG               PIC X(01).                  
       10  E1_CIGNA_ADJ_CLAIM_DOC_NUM               PIC X(30).                  
       10  E1_BN_CLM_ERROR_TYPE                    PIC X(01).                   
       10  E1_BN_CLM_ERROR_CODE                    PIC S9(02).                  
       10  E1_BN_CLM_ADJ_REVIEW                    PIC X(01).                   
       10  E1_BN_CLM_DISP                          PIC X(01).                   
       10  E1_BN_CLM_ALLIANCE_ADD_INFO             PIC X(01).                   
       10  E1_BN_CLM_PRICING_GREATER_IND           PIC X(01).                   
       10  E1_BN_CLM_PRICING_ERR_IND               PIC X(01).                   
       10  E1_BN_CLM_ALLIANCE_AMT                  PIC S9(06)V99.               
       10  E1_BN_CLM_ALLIANCE_PCT_IND              PIC X(01).                   
       10  E1_BN_CLM_ALLIANCE_RNC                  PIC X(20).                   
       10  E1_BN_CLM_PRC_RNC                       PIC X(04).                   
       10  E1_BN_CLM_PRP_RNC                       PIC X(04).                   
       10  E1_BN_CLM_PMHS_RNC                      PIC X(04).                   
       10  E1_BN_CLM_NTWK_IND                      PIC X(01).                   
       10  E1_BN_CLM_DISCOUNT_AMT                  PIC S9(06)V99.               
       10  E1_BN_CLM_ALLOWED_AMT                   PIC S9(06)V99.               
       10  E1_BN_CLM_TOTAL_CHARGE                  PIC S9(06)V99.               
       10  E1_MCX_NON_COVERED_CHARGE               PIC 9(08)V99.                
       10  E1_MCX_CLM_LIMITING_CHARGE_1            PIC 9(08)V99.                
       10  E1_PORTICO_RETURNED_DATA.                                            
           15  E1_PORTICO_GRP_RETURN_CODE          PIC X(04).                   
             88   88_E1_GRP_PORTICO_SUCCESS        VALUE '0500'.                
             88   88_E1_GRP_PORTICO_FAIL           VALUE '1500'.                
             88   88_E1_GRP_HBOR_EAS_ERRORS        VALUE '1501'.                
             88   88_E1_GRP_HBOR_CALL_OFF          VALUE '1502'.                
           15  E1_PORTICO_GRP_ASSOC                PIC X(04).                   
           15  E1_PORTICO_IND_RETURN_CODE          PIC X(04).                   
             88   88_E1_IND_PORTICO_SUCCESS        VALUE '0500'.                
             88   88_E1_IND_PORTICO_FAIL           VALUE '1500'.                
             88   88_E1_IND_HBOR_EAS_ERRORS        VALUE '1501'.                
             88   88_E1_IND_HBOR_CALL_OFF          VALUE '1502'.                
           15  E1_PORTICO_IND_ASSOC                PIC X(04).                   
           15  E1_PORTICO_VENDOR_ID                PIC X(30).                   
           15  E1_PORTICO_PROVIDER_ID              PIC X(30).                   
       10  E1_ADDITIONAL_DATA.                                                  
           15  E1_COB_DISCREPANCY_IND              PIC X.                       
           15  E1_LPI_IND                          PIC X.                       
           15  E1_EMERGENCY_IND                    PIC X.                       
           15  E1_DOFR_IND                         PIC X.                       
       10  E1_FULL_TIN_ASSOC_DATA.                                              
           15  E1_FTIN_ASSC_NO                     PIC X(07).                   
           15  E1_FTIN_PRVMTCH_GRP_ASSC            PIC X(07).                   
           15  E1_FTIN_PORTICO_GRP_ASSC            PIC X(07).                   
           15  E1_FTIN_PORTICO_IND_ASSC            PIC X(07).                   
       10  FILLER_028                              PIC X(726).                  
       10 E1_CLAIM_INTAKE_EDI_CLAIM_ID.                                         
           15 E1_CI_EDI_INTRCHG_KEY.                                            
              20 E1_CI_CCJJJ                        PIC X(5).                   
              20 E1_CI_INDICATOR                    PIC X.                      
              20 E1_CI_UNIQUE_KEY                   PIC X(6).                   
           15 E1_CI_NUM_OF_SPLITS                   PIC X(3).                   
           15 E1_KEY_FILLER_029                     PIC X(12).                  
       10  E1_SORT2_KEY.                                                        
           15  FILLER_030                           PIC X(07).                  
           15  E1_SORT2_TPA_VENDOR                  PIC X(05).                  
           15  E1_SORT2_HDR_TLR                     PIC X(01).                  
           15  FILLER_031                           PIC X(16).                  
       10  E1_SORT_KEY                              PIC X(57).                  
01  E2_CLAIM_REC REDEFINES ECR_REC.                                             
   05  E2_CLAIM_RECORD.                                                         
       10  FILLER_032                               PIC X(23).                  
     07 E2_INIT1.                                                               
       10  E2_CLAIM_SYSTEM_IND                      PIC X(01).                  
           88 A_MEDICOM_RECORD                   VALUE 'M'.                     
           88 A_CIGNA_CLAIMS_RECORD              VALUE 'C'.                     
           88 A_DENTACOM_RECORD                  VALUE 'D'.                     
           88 A_NEW_WORLD_RECORD                 VALUE 'W'.                     
       10  E2_TRANSACTION_SET_HEADER.                                           
           15  E2_TRANS_SET_IDENTIFIER              PIC X(03).                  
           15  E2_TRANS_SET_CONTROL_NUM             PIC X(09).                  
           15  E2_PROCESS_DT                        PIC X(08).                  
           15  E2_PROCESS_DT_CCYYMMDD REDEFINES                                 
               E2_PROCESS_DT.                                                   
               20   E2_PROCESS_DT_CC                PIC 9(02).                  
               20   E2_PROCESS_DT_YYMMDD            PIC 9(06).                  
           15  E2_X12_VERSION                       PIC X(4).                   
           15  FILLER_033                           PIC X(16).                  
           15  E2_ELIG_EAV_CLM_STATUS_CD            PIC X(05).                  
           15  E2_ELIG_SORTEAV_SPLIT_IND            PIC X(01).                  
           15  E2_COMPLIANCE_IND                    PIC X(01).                  
           15  E2_SUBMITTER_CREATE_TIME             PIC X(04).                  
           15  FILLER_034                           PIC X(11).                  
       10  E2_BEGINNING_SEGMENT.                                                
           15  E2_TRANS_SET_REFERENCE_NUM           PIC X(30).                  
           15  E2_SUBMITTER_CREATE_DT               PIC 9(08).                  
           15  FILLER_035                           PIC 9(02).                  
       10  E2_OCCURS_COUNTS.                                                    
           15  E2_SUBSCRIBER_CARRIER_COUNT          PIC 9(03).                  
           15  E2_OCCURRENCE_DT_COUNT               PIC 9(03).                  
           15  E2_REMARKS_CLM_COUNT                 PIC 9(03).                  
           15  E2_OTHER_PROC_COUNT                  PIC 9(03).                  
           15  E2_VALUE_INFORMATION_COUNT           PIC 9(03).                  
           15  E2_OCCURRENCE_CODE_COUNT             PIC 9(03).                  
           15  E2_DIAGNOSIS_CODE_COUNT              PIC 9(03).                  
           15  E2_COND_CODE_COUNT                   PIC 9(03).                  
           15  E2_ERROR_MESSAGE_COUNT               PIC 9(03).                  
           15  E2_CASA_CLM_COUNT                    PIC 9(03).                  
           15  E2_CASB_CLM_COUNT                    PIC 9(03).                  
           15  FILLER_036                           PIC X(14).                  
       10  E2_REMIT_CD                              PIC X(01).                  
       10  E2_LOOP_1000_SUBMITTER.                                              
           15  E2_SUBMITTER_NAME                    PIC X(35).                  
           15  E2_SUBMITTER_ID                      PIC X(20).                  
           15  E2_DENTIST_LICENSE_NUM               PIC X(30).                  
           15  E2_VENDOR_ID_CHAT                    PIC X(30).                  
           15  E2_PROVIDER_ID_CHAT                  PIC X(30).                  
           15  E2_REF_PROV_UPIN_NUMBER              PIC X(20).                  
           15  E2_MEDICARE_RISK_IND                 PIC X(01).                  
           15  E2_PROVIDER_ID_CHAT_QUAL             PIC X(02).                  
           15  FILLER_037                           PIC X(07).                  
       10  E2_BILL_PRV_TAXONOMY                     PIC X(30).                  
       10  E2_LOOP_2000_BILL_PRV_ID.                                            
           15  E2_BILL_PRV_ID                       PIC X(30).                  
           15  E2_BILL_PRV_SPEC_CODE                PIC X(03).                  
           15  E2_BILL_PRV_EIN_TYPE                 PIC X(03).                  
           15  E2_BILLING_SEC_ID_QUALIFIER          PIC X(02).                  
           15  E2_BILL_PRV_MIDDLE_INIT              PIC X(01).                  
           15  E2_BILL_PRV_GENERATION               PIC X(04).                  
           15  E2_BILL_PRV_DESIGNATION              PIC X(02).                  
           15  E2_BILL_PRV_ROLE                     PIC X(01).                  
           15  FILLER_038                           PIC X(07).                  
       10  E2_LOOP_2010_BILL_PROVIDER.                                          
           15  E2_BILL_PRV_LAST_NAME                PIC X(35).                  
           15  E2_BILL_PRV_FIRST_NAME               PIC X(25).                  
           15  E2_BILL_PRV_ADDR_1                   PIC X(35).                  
           15  E2_BILL_PRV_ADDR_2                   PIC X(35).                  
           15  E2_BILL_PRV_CITY                     PIC X(30).                  
           15  E2_BILL_PRV_STATE                    PIC X(02).                  
           15  E2_BILL_PRV_ZIP                      PIC X(11).                  
           15  E2_BILL_PRV_PHONE_NUM                PIC X(80).                  
           15  E2_BILL_PRV_FAX_NUM                  PIC X(80).                  
           15  E2_BILL_PRV_NATL_ID                  PIC X(20).                  
     07 E2_INIT1_END.                                                           
       10  E2_LOOP_2100_SUBSCR_CARRIER OCCURS 1 TIMES.                          
           15  E2_PAYER_RESPONSIBILITY              PIC X(01).                  
           15  E2_SUBSCR_ACCT_NUM                   PIC X(30).                  
           15  E2_SUBSCR_ACCT_NAME                  PIC X(35).                  
           15  E2_OI_QUALIFIER                      PIC X(03).                  
           15  E2_COB_IND                           PIC X(01).                  
           15  FILLER_039                           PIC X(19).                  
     07  E2_LOOP_2100_END.                                                      
       10  E2_MEDICAL_RECORD_NUMBER                 PIC X(30).                  
       10  E2_CCR_CLAIM_KEY.                                                    
           15  E2_CURRENT_DATE_CCYYMMDD             PIC 9(08).                  
           15  E2_CLAIM_ID                          PIC X(14).                  
           15  E2_RENDERING_PRV_SPLIT_IND           PIC X(01).                  
           15  E2_ELIG_SPLIT_IND                    PIC X(01).                  
           15  E2_ELIG_SPLIT_IND2                   PIC X(01).                  
           15  E2_ELIG_SPLIT_IND3                   PIC X(01).                  
           15  E2_ELIG_SPLIT_IND4                   PIC X(01).                  
       10  E2_CLAIM_INTAKE_KEY REDEFINES                                        
               E2_CCR_CLAIM_KEY.                                                
           15 E2_CI_KEY_INTRCHG.                                                
              20  E2_CI_KEY_CCJJJ                   PIC X(5).                   
              20  E2_CI_KEY_INDICATOR               PIC X.                      
              20  E2_CI_KEY_UNIQUE_KEY              PIC X(6).                   
           15 E2_CI_KEY_NUM_OF_SPLITS               PIC X(3).                   
           15  E2_CI_KEY_FILLER_040                 PIC X(12).                  
       10  E2_RENDER_PRV_TAXONOMY                   PIC X(30).                  
       10  E2_REFERR_DENTIST_LICENSE_NBR            PIC X(30).                  
       10  E2_REFERR_PRV_LOCATION                   PIC X(30).                  
       10  E2_RENDER_DENTIST_LICENSE_NBR            PIC X(30).                  
       10  E2_RENDER_FAC_NATL_ID                    PIC X(20).                  
       10  E2_RENDER_PRV_NATL_ID                    PIC X(20).                  
       10  E2_REFERR_PRV_NATL_ID                    PIC X(20).                  
       10  E2_BILL_CPF_PRV_ID                       PIC X(07).                  
       10  E2_REND_CPF_PRV_ID                       PIC X(07).                  
       10  E2_REND_SPECIALTY_CD_1                   PIC X(03).                  
       10  E2_REND_SPECIALTY_CD_2                   PIC X(03).                  
       10  FILLER_041                               PIC X(10).                  
       10  E2_LOOP_2110_EMPLE_NAME_ADDR.                                        
           15  E2_EMPLE_LAST_NAME                   PIC X(35).                  
           15  E2_EMPLE_FIRST_NAME                  PIC X(25).                  
           15  E2_EMPLE_GENERATION                  PIC X(10).                  
           15  E2_EMPLE_ID_NUM                      PIC X(20).                  
           15  E2_EMPLE_SSN                         PIC X(09).                  
           15  E2_EMPLE_ID_QUALIFIER                PIC X(02).                  
           15  E2_EMPLE_MIDDLE_INIT                 PIC X(01).                  
           15  FILLER_042                           PIC X(08).                  
           15  E2_EMPLE_CED_PERSON_ID               PIC 9(15).                  
           15  E2_EMPLE_ADDR_1                      PIC X(35).                  
           15  E2_EMPLE_ADDR_2                      PIC X(35).                  
           15  E2_EMPLE_CITY                        PIC X(30).                  
           15  E2_EMPLE_STATE                       PIC X(02).                  
           15  E2_EMPLE_ZIP                         PIC X(11).                  
           15  E2_EMPLE_DOB                         PIC X(08).                  
           15  E2_EMPLE_DOB_CCYYMMDD REDEFINES                                  
               E2_EMPLE_DOB.                                                    
               20   E2_EMPLE_DOB_CC                 PIC X(02).                  
               20   E2_EMPLE_DOB_YYMMDD             PIC X(06).                  
           15  E2_EMPLE_SEX                         PIC X(01).                  
           15  E2_EMPLE_FCO_NUM                     PIC 9(03).                  
           15  E2_EMPLE_COPY_DATABASE               PIC X(01).                  
           15  E2_AMI_IN_FLAG                       PIC X(01).                  
           15  FILLER_043                           PIC X(19).                  
       10  E2_LOOP_2200_PAT_INFO.                                               
           15  E2_RELATIONSHIP_TO_EMPLE             PIC X(02).                  
           15  E2_PAT_EMPLOYMENT_STATUS             PIC X(02).                  
           15  E2_PAT_STUDENT_STATUS                PIC X(01).                  
           15  E2_PAT_DODEATH                       PIC X(08).                  
           15  E2_PAT_DODEATH_CCYYMMDD REDEFINES                                
               E2_PAT_DODEATH.                                                  
               20 E2_PAT_DODEATH_CC                 PIC X(02).                  
               20 E2_PAT_DODEATH_YYMMDD             PIC X(06).                  
           15  E2_PAT_WEIGHT                        PIC 9(10).                  
           15  E2_PAT_WEIGHT_QUAL                   PIC X(02).                  
           15  FILLER_044                           PIC X(18).                  
       10  E2_LOOP_2210_PAT_NAME_ADDR.                                          
           15  E2_PAT_CED_PERSON_ID                 PIC 9(15).                  
           15  E2_PAT_LAST_NAME                     PIC X(35).                  
           15  E2_PAT_FIRST_NAME                    PIC X(25).                  
           15  E2_PAT_MIDDLE_NAME                   PIC X(25).                  
           15  E2_PAT_GENERATION                    PIC X(10).                  
           15  E2_PAT_ADDR_1                        PIC X(35).                  
           15  E2_PAT_ADDR_2                        PIC X(35).                  
           15  E2_PAT_CITY                          PIC X(30).                  
           15  E2_PAT_STATE                         PIC X(02).                  
           15  E2_PAT_ZIP                           PIC X(11).                  
           15  E2_PAT_DOB                           PIC X(08).                  
           15  E2_PAT_DOB_CCYYMMDD REDEFINES                                    
               E2_PAT_DOB.                                                      
               20  E2_PAT_DOB_CC                    PIC X(02).                  
               20  E2_PAT_DOB_YYMMDD                PIC X(06).                  
           15  E2_PAT_SEX                           PIC X(01).                  
           15  E2_PAT_FCO_NUM                       PIC 9(03).                  
           15  FILLER_045                           PIC X(01).                  
           15  E2_LEGAL_REP_LAST_NAME               PIC X(35).                  
           15  E2_LEGAL_REP_FIRST_NAME              PIC X(25).                  
           15  E2_LEGAL_REP_MIDDLE_NAME             PIC X(25).                  
           15  E2_LEGAL_REP_GENERATIN               PIC X(10).                  
           15  E2_LEGAL_REP_ADDR_1                  PIC X(35).                  
           15  E2_LEGAL_REP_ADDR_2                  PIC X(35).                  
           15  E2_LEGAL_REP_CITY                    PIC X(30).                  
           15  E2_LEGAL_REP_STATE                   PIC X(02).                  
           15  E2_LEGAL_REP_ZIP                     PIC X(11).                  
           15  E2_PAT_EMPLR_LAST_NAME               PIC X(35).                  
           15  E2_PAT_EMPLR_FIRST_NAME              PIC X(25).                  
           15  E2_PAT_EMPLR_MIDDLE_NAME             PIC X(25).                  
           15  E2_PAT_EMPLR_GENERATION              PIC X(10).                  
           15  E2_PAT_EMPLR_ADDR_1                  PIC X(35).                  
           15  E2_PAT_EMPLR_ADDR_2                  PIC X(35).                  
           15  E2_PAT_EMPLR_CITY                    PIC X(30).                  
           15  E2_PAT_EMPLR_STATE                   PIC X(02).                  
           15  E2_PAT_EMPLR_ZIP                     PIC X(11).                  
           15  FILLER_046                           PIC X(20).                  
       10  E2_LOOP_2300_CLAIM_DATA.                                             
         12 E2_INIT2.                                                           
           15  E2_PAT_NUM                           PIC X(38).                  
           15  E2_DELAY_REASON_CODE                 PIC X(02).                  
           15  E2_EOB_INDICATOR                     PIC X(01).                  
           15  FILLER_047                           PIC X(02).                  
           15  E2_CLAIM_TOTAL_CHARGES               PIC 9(08)V99.               
           15  E2_CLAIM_FILING_IND                  PIC X(02).                  
           15  E2_CLAIM_TYPE                        PIC X(02).                  
           15  E2_TYPE_OF_BILL_1                    PIC X(02).                  
           15  E2_TYPE_OF_BILL_2                    PIC X(02).                  
           15  E2_TYPE_OF_BILL_3                    PIC X(01).                  
           15  E2_MEDICARE_ACCEPTS_ASSIGN           PIC X(01).                  
           15  E2_ASSIGNS_BENS_IND                  PIC X(01).                  
           15  E2_RELEASE_INFO_IND                  PIC X(01).                  
           15  E2_EMPLE_SIGNATURE_IND               PIC X(01).                  
           15  E2_ACCIDENT_IND_1                    PIC X(03).                  
           15  E2_ACCIDENT_IND_2                    PIC X(03).                  
           15  E2_ACCIDENT_IND_3                    PIC X(03).                  
           15  E2_PLACE_OF_ACCIDENT                 PIC X(02).                  
           15  E2_ACCIDENT_COUNTRY                  PIC X(03).                  
           15  E2_LVL_OF_SVC                        PIC X(03).                  
           15  E2_RELEASE_INFO_DT                   PIC X(08).                  
           15  E2_RELEASE_INFO_DT_CCYYMMDD REDEFINES                            
               E2_RELEASE_INFO_DT.                                              
               20  E2_RELEASE_INFO_DT_CC            PIC X(02).                  
               20  E2_RELEASE_INFO_DT_YYMMDD        PIC X(06).                  
           15  E2_ACCIDENT_DT                       PIC X(08).                  
           15  E2_ACCIDENT_DT_CCYYMMDD REDEFINES                                
               E2_ACCIDENT_DT.                                                  
               20  E2_ACCIDENT_DT_CC                PIC X(02).                  
               20  E2_ACCIDENT_DT_YYMMDD            PIC X(06).                  
           15  E2_ACCIDENT_HOUR                     PIC X(04).                  
           15  E2_ADMISSION_DT                      PIC X(08).                  
           15  E2_ADMISSION_DT_CCYYMMDD REDEFINES                               
               E2_ADMISSION_DT.                                                 
               20  E2_ADMISSION_DT_CC               PIC X(02).                  
               20  E2_ADMISSION_DT_YYMMDD           PIC X(06).                  
           15  E2_ADMISSION_HOUR                    PIC X(04).                  
           15  E2_DISCHARGE_DT                      PIC X(08).                  
           15  E2_DISCHARGE_DT_CCYYMMDD REDEFINES                               
               E2_DISCHARGE_DT.                                                 
               20  E2_DISCHARGE_DT_CC               PIC X(02).                  
               20  E2_DISCHARGE_DT_YYMMDD           PIC X(06).                  
           15  E2_DISCHARGE_HOUR                    PIC X(04).                  
           15  E2_STATEMT_PER_FRM                   PIC X(08).                  
           15  E2_STATEMT_PER_FRM_CCYYMMDD REDEFINES                            
               E2_STATEMT_PER_FRM.                                              
               20 E2_STATEMT_PER_FRM_CC             PIC X(02).                  
               20 E2_STATEMT_PER_FRM_YYMMDD         PIC X(06).                  
           15  E2_STATEMT_PER_TO                    PIC X(08).                  
           15  E2_STATEMT_PER_TO_CCYYMMDD REDEFINES                             
               E2_STATEMT_PER_TO.                                               
               20  E2_STATEMT_PER_TO_CC             PIC X(02).                  
               20  E2_STATEMT_PER_TO_YYMMDD         PIC X(06).                  
           15  E2_MEDICAL_SVC_FRM                   PIC X(08).                  
           15  E2_MEDICAL_SVC_FRM_CCYYMMDD REDEFINES                            
               E2_MEDICAL_SVC_FRM.                                              
               20  E2_MEDICAL_SVC_FRM_CC            PIC X(02).                  
               20  E2_MEDICAL_SVC_FRM_YYMMDD        PIC X(06).                  
           15  E2_MEDICAL_SVC_TO                    PIC X(08).                  
           15  E2_MEDICAL_SVC_TO_CCYYMMDD REDEFINES                             
               E2_MEDICAL_SVC_TO.                                               
               20  E2_MEDICAL_SVC_TO_CC             PIC X(02).                  
               20  E2_MEDICAL_SVC_TO_YYMMDD         PIC X(06).                  
           15  E2_PRE_EXIST_DT                      PIC X(08).                  
           15  E2_PRE_EXIST_DT_CCYYMMDD REDEFINES                               
               E2_PRE_EXIST_DT.                                                 
               20  E2_PRE_EXIST_DT_CC               PIC X(02).                  
               20  E2_PRE_EXIST_DT_YYMMDD           PIC X(06).                  
           15  E2_DT_FIRST_CONSULT                  PIC X(08).                  
           15  E2_DT_FIRST_CONSULT_CCYYMMDD REDEFINES                           
               E2_DT_FIRST_CONSULT.                                             
               20  E2_DT_FIRST_CONSULT_CC           PIC X(02).                  
               20  E2_DT_FIRST_CONSULT_YYMMDD       PIC X(06).                  
           15  E2_MATERNITY_DT                      PIC X(08).                  
           15  E2_MATERNITY_DT_CCYYMMDD REDEFINES                               
               E2_MATERNITY_DT.                                                 
               20  E2_MATERNITY_DT_CC               PIC X(02).                  
               20  E2_MATERNITY_DT_YYMMDD           PIC X(06).                  
         12 E2_INIT2_END.                                                       
           15  E2_OCCURRENCE_DT                                                 
                 OCCURS 10 TIMES.                                               
               20  E2_OCCURRENCE_FRM                PIC X(08).                  
               20  E2_OCCURRENCE_FRM_CCYYMMDD REDEFINES                         
                   E2_OCCURRENCE_FRM.                                           
                   25  E2_OCCURRENCE_FRM_CC         PIC X(02).                  
                   25  E2_OCCURRENCE_FRM_YYMMDD     PIC X(06).                  
               20  E2_OCCURRENCE_TO                 PIC X(08).                  
               20  E2_OCCURRENCE_TO_CCYYMMDD REDEFINES                          
                   E2_OCCURRENCE_TO.                                            
                   25  E2_OCCURRENCE_TO_CC          PIC X(02).                  
                   25  E2_OCCURRENCE_TO_YYMMDD      PIC X(06).                  
         12 E2_INIT3.                                                           
           15  E2_TYPE_OF_ADMISSION                 PIC X(01).                  
           15  E2_SOURCE_OF_ADMISSION               PIC X(01).                  
           15  E2_PAT_STATUS                        PIC X(02).                  
           15  E2_EST_TREAT_MONTHS                  PIC 9(15).                  
           15  E2_TREAT_MONTHS_REMAIN               PIC 9(15).                  
           15  E2_TRACTION_DEVICE_IND               PIC X(01).                  
           15  E2_APPLIANCE_DESCRIPTION             PIC X(80).                  
           15  E2_PARTICIPATING_PRV_IND             PIC X(01).                  
           15  E2_APPLIANCE_PLC_DT                  PIC X(08).                  
           15  E2_APPLIANCE_PLC_DT_CCYYMMDD REDEFINES                           
               E2_APPLIANCE_PLC_DT.                                             
               20  E2_APPLIANCE_PLC_DT_CC           PIC X(02).                  
               20  E2_APPLIANCE_PLC_DT_YYMMDD       PIC X(06).                  
           15  E2_ESCROW_AMOUNT                     PIC 9(13)V99.               
           15  E2_GTWY_ATTACH_CNTRL_NUM             PIC X(17).                  
           15  FILLER_048                           PIC X(04).                  
           15  E2_DOCUMENT_TYPE2                    PIC X(02).                  
           15  E2_DOCUMENT_TYPE                     PIC X(02).                  
           15  E2_REPORT_TRANMISSION_CODE           PIC X(02).                  
           15  E2_ATTACHMENT_CONTROL_NUM            PIC X(20).                  
           15  E2_DRG_CODE                          PIC X(30).                  
           15  E2_ENCOUNTER_FLAG                    PIC X(02).                  
           15  FILLER_049                           PIC X(05).                  
           15  E2_CONTRACT_AMOUNT                   PIC 9(08)V99.               
           15  FILLER_050                           PIC X(05).                  
           15  E2_PAT_AMOUNT_PAID                   PIC 9(08)V99.               
           15  FILLER_051                           PIC X(05).                  
           15  E2_BALANCE_DUE_AMOUNT                PIC 9(08)V99.               
           15  E2_SUBMITTER_DCN                     PIC X(30).                  
           15  E2_REFERRAL_NUM                      PIC X(30).                  
           15  E2_TREAT_AUTHORIZE_CODE              PIC X(30).                  
           15  E2_HMO_CODE                          PIC X(30).                  
           15  E2_CLAIM_TPO_AUTHORIZE_NUM           PIC X(30).                  
         12 E2_INIT3_END.                                                       
           15  E2_REMARKS_CLM                                                   
                 OCCURS 10 TIMES.                                               
              20  E2_REMARKS_CLM_DATA               PIC X(80).                  
         12 E2_INIT4.                                                           
           15  E2_LOOP_2320_SUBSCRIB_CARRIER                                    
               OCCURS 2 TIMES.                                                  
               20  E2_PRIMACY_CODE                  PIC X(01).                  
               20  E2_RELATIONSHIP_CODE             PIC X(02).                  
               20  E2_ACCOUNT_NUMBER                PIC X(30).                  
               20  E2_ACCOUNT_NAME                  PIC X(60).                  
               20  E2_INSURANCE_TYPE                PIC X(03).                  
               20  E2_OI_ALLOWED_AMT                PIC 9(08)V99.               
               20  E2_OI_PAID_AMT                   PIC 9(08)V99.               
               20  E2_OI_ALLOWED_AMT_IND            PIC X(01).                  
               20  E2_OI_PAID_AMT_IND               PIC X(01).                  
               20  E2_APPROVED_AMT                  PIC 9(08)V99.               
               20  E2_COVERED_AMT                   PIC 9(08)V99.               
               20  E2_DISCOUNT_AMT                  PIC 9(08)V99.               
               20  E2_NON_COVERED_CHARGE            PIC 9(08)V99.               
               20  E2_TOTAL_DENIED_AMT              PIC 9(08)V99.               
               20  E2_TOTAL_SUBMITTED_CHARGES       PIC 9(08)V99.               
               20  E2_DRG_OUTLIER                   PIC 9(08)V99.               
               20  E2_PAYER_TO_PATIENT_AMT          PIC 9(08)V99.               
               20  E2_PATIENT_RESPONS_AMT           PIC 9(08)V99.               
               20  E2_MEDICARE_PAID_AMT             PIC 9(08)V99.               
               20  E2_MEDICARE_100PC                PIC 9(08)V99.               
               20  E2_MEDICARE_80PC                 PIC 9(08)V99.               
               20  E2_ALLOCATED_PART_A_AMT          PIC 9(08)V99.               
               20  E2_ALLOCATED_PART_B_AMT          PIC 9(08)V99.               
               20  E2_MEDICARE_COVERED_DAYS_UNITS   PIC X(15).                  
               20  E2_MEDICARE_REIMBURSEMENT_RATE   PIC X(10).                  
               20  E2_MEDICARE_HCPC_PAYABLE_AMT     PIC 9(08)V99.               
               20  E2_MEDICARE_REMARKS_CODE1        PIC X(30).                  
               20  E2_MEDICARE_REMARKS_CODE2        PIC X(30).                  
               20  E2_MEDICARE_REMARKS_CODE3        PIC X(30).                  
               20  E2_MEDICARE_REMARKS_CODE4        PIC X(30).                  
               20  E2_MEDICARE_REMARKS_CODE5        PIC X(30).                  
               20  E2_MEDICARE_ESRD_PAYMENT_AMT     PIC 9(08)V99.               
               20  E2_MEDICARE_NONPAY_PROF_COMP     PIC 9(08)V99.               
               20  E2_OI_ASSIGNMENT_BENEFIT_IND     PIC X(01).                  
               20  E2_OTH_INS_CARRIER               PIC X(35).                  
               20  E2_OTH_INS_CARRIER_ID            PIC X(20).                  
               20  E2_OTH_INS_CARRIER_NATL_ID       PIC X(20).                  
               20  E2_OTH_INSUREDS_SSN              PIC X(20).                  
               20  E2_REMAINING_PAT_LIABILITY       PIC 9(13)V99.               
               20  FILLER_052                       PIC X(05).                  
          12  E2_PRINCIPAL_PROC_ICD_VERSION         PIC 99.                     
          12  E2_ADMITTING_DIAG_ICD_VERSION         PIC 99.                     
          12  E2_PRINCIPAL_DIAG_ICD_VERSION         PIC 99.                     
          12  E2_PRINCIPAL_DIAG_POA_IND             PIC X.                      
         12 FILLER_053                              PIC X(40).                  
         12 E2_INIT5.                                                           
           15  E2_PRINCIPAL_PROC_CODE               PIC X(27).                  
           15  E2_PRINCIPAL_PROC_CODE_QUAL          PIC X(03).                  
           15  E2_PRINCIPAL_PROC_DT                 PIC X(08).                  
           15  E2_PRINCIPAL_PROC_CCYYMMDD REDEFINES                             
               E2_PRINCIPAL_PROC_DT.                                            
               20  E2_PRINCIPAL_PROC_CC             PIC X(02).                  
               20  E2_PRINCIPAL_PROC_YYMMDD         PIC X(06).                  
         12 E2_INIT5_END.                                                       
           15  E2_OTHER_PROC                                                    
                 OCCURS 5 TIMES.                                                
               20  E2_OTHER_PROC_CODE               PIC X(27).                  
               20  E2_OTHER_PROC_CODE_QUAL          PIC X(03).                  
               20  E2_OTHER_PROC_DT                 PIC X(08).                  
               20  E2_OTHER_PROC_CCYYMMDD REDEFINES                             
                   E2_OTHER_PROC_DT.                                            
                   25  E2_OTHER_PROC_CC             PIC X(02).                  
                   25  E2_OTHER_PROC_YYMMDD         PIC X(06).                  
           15  E2_VALUE_INFORMATION                                             
                 OCCURS 12 TIMES.                                               
               20  E2_VALUE_CODES                   PIC X(02).                  
               20  FILLER_054                       PIC X(05).                  
               20  E2_VALUE_AMOUNTS                 PIC 9(08)V99.               
               20  E2_VALUE_QUANTITY                PIC 9(15).                  
           15  E2_OCCURRENCE_CODE                                               
                 OCCURS 10 TIMES.                                               
               20  E2_OCCURRENCE_CODE_DATA          PIC X(30).                  
           15  E2_ADMITTING_DIAGNOSIS               PIC X(30).                  
           15  E2_PRINCIPAL_DIAGNOSIS               PIC X(30).                  
           15  E2_E_CODE                            PIC X(30).                  
           15  E2_OTHER_DIAGNOSIS_AREA.                                         
               20  E2_OTHER_DIAGNOSIS_CODE                                      
                 OCCURS 9 TIMES                     PIC X(30).                  
           15  E2_COND_CODE                                                     
                 OCCURS 7 TIMES.                                                
               20  E2_COND_CODE_DATA                PIC X(30).                  
           15  FILLER_055                           PIC X(05).                  
           15  E2_TOTAL_TPO_ALLOWED_AMOUNT          PIC 9(08)V99.               
           15  E2_CLAIM_TPO_ID_NUM                  PIC X(30).                  
           15  E2_DAYS_APPROVED                     PIC 9(15).                  
           15  E2_UNITS_APPROVED                    PIC 9(15).                  
           15  E2_CLAIM_REJECT_MESSAGE              PIC X(02).                  
           15  FILLER_056                           PIC X(02).                  
           15  E2_TOOTH_NUMBERS                                                 
                  OCCURS 35 TIMES.                                              
               20  E2_TOOTH_NUMBER                  PIC X(02).                  
           15  E2_ELIGIBILITY_SPLIT_IND2            PIC X(01).                  
           15  E2_BEHAVIORAL_CLAIM_IND              PIC X(01).                  
           15  FILLER_057                           PIC X(06).                  
     07 E2_INIT6.                                                               
       10  E2_LOOP_2310_PRV_INFO.                                               
           15  E2_ATTENDING_PRV_NATL_ID             PIC X(20).                  
           15  E2_RENDERING_FAC_NAME                PIC X(35).                  
           15  E2_RENDERING_FAC_ID_NUM              PIC X(20).                  
           15  E2_RENDERING_FAC_ADDR_1              PIC X(35).                  
           15  E2_RENDERING_FAC_ADDR_2              PIC X(35).                  
           15  E2_RENDERING_FAC_CITY                PIC X(30).                  
           15  E2_RENDERING_FAC_STATE               PIC X(02).                  
           15  E2_RENDERING_FAC_ZIP                 PIC X(11).                  
           15  E2_RENDERING_PRV_LAST_NAME           PIC X(35).                  
           15  E2_RENDERING_PRV_FIRST_NAME          PIC X(25).                  
           15  E2_RENDERING_PRV_MIDDLE_NAME         PIC X(25).                  
           15  E2_RENDERING_PRV_GENERATION          PIC X(10).                  
           15  E2_RENDERING_PRV_ID_NUM              PIC X(20).                  
           15  E2_RENDERING_PRV_SPCLTY_CODE         PIC X(03).                  
           15  E2_RENDERING_PRV_ADDR_1              PIC X(35).                  
           15  E2_RENDERING_PRV_ADDR_2              PIC X(35).                  
           15  E2_RENDERING_PRV_CITY                PIC X(30).                  
           15  E2_RENDERING_PRV_STATE               PIC X(02).                  
           15  E2_RENDERING_PRV_ZIP                 PIC X(11).                  
           15  E2_RENDERING_PRV_TELEPHONE           PIC X(80).                  
           15  E2_ATTENDING_PRV_LAST_NAME           PIC X(35).                  
           15  E2_ATTENDING_PRV_FIRST_NAME          PIC X(25).                  
           15  E2_ATTENDING_PRV_MIDDLE_NAME         PIC X(25).                  
           15  E2_ATTENDING_PRV_GENERATION          PIC X(10).                  
           15  E2_ATTENDING_PRV_ID_NUM              PIC X(20).                  
           15  E2_REFERRING_PRV_LAST_NAME           PIC X(35).                  
           15  E2_REFERRING_PRV_FIRST_NAME          PIC X(25).                  
           15  E2_REFERRING_PRV_MIDDLE_NAME         PIC X(25).                  
           15  E2_REFERRING_PRV_GENERATION          PIC X(10).                  
           15  E2_REFERRING_PRV_ID_NUM              PIC X(20).                  
           15  E2_PRV_SIGNATURE                     PIC X(01).                  
           15  E2_RENDERING_PRV_EIN_IND             PIC X(02).                  
           15  E2_RENDER_PRV_DESIGNATION            PIC X(02).                  
           15  E2_RENDER_PRV_ROLE                   PIC X(01).                  
           15  E2_RENDER_FACILITY_ID_TYPE           PIC X(03).                  
           15  E2_RENDER_FACILITY_ID_QUAL           PIC X(02).                  
           15  E2_RENDER_FACILTY_DESIGNATION        PIC X(02).                  
           15  FILLER_058                           PIC X(08).                  
       10  E2_LOOP_2320_SUBSCR_INS.                                             
           15  FILLER_059                           PIC X(01).                  
           15  E2_RELATIONSHIP_CD                   PIC X(02).                  
           15  E2_ACCT_NUM                          PIC X(30).                  
           15  E2_ACCT_NAME                         PIC X(35).                  
           15  E2_INS_TYPE                          PIC X(03).                  
           15  E2_EMPLOYMENT_STATUS                 PIC X(02).                  
           15  E2_MEDICARE_DEDUCTIBLE               PIC 9(13)V99.               
           15  E2_OI_ALLOWED_AMOUNT                 PIC 9(13)V99.               
           15  E2_OI_PAID_AMOUNT                    PIC 9(13)V99.               
           15  E2_OI_CLAIM_FILING_IND               PIC X(02).                  
           15  E2_OI_ASSIGN_BENEFIT_IND_OLD         PIC X(01).                  
           15  E2_PAT_SIGNATURE                     PIC X(01).                  
           15  E2_OTH_INS_CARRIER_OLD               PIC X(35).                  
           15  E2_OTH_INS_CARRIER_ADDR_1            PIC X(35).                  
           15  E2_OTH_INS_CARRIER_ADDR_2            PIC X(35).                  
           15  E2_OTH_INS_CARRIER_CITY              PIC X(30).                  
           15  E2_OTH_INS_CARRIER_STATE             PIC X(02).                  
           15  E2_OTH_INS_CARRIER_ZIP               PIC X(11).                  
           15  E2_OTH_INSUREDS_LAST_NAME            PIC X(35).                  
           15  E2_OTH_INSUREDS_FIRST_NAME           PIC X(25).                  
           15  E2_OTH_INSUREDS_MIDDLE_NAME          PIC X(25).                  
           15  E2_OTH_INSUREDS_GENERATION           PIC X(10).                  
           15  E2_OTH_INSUREDS_SSN_OLD              PIC X(20).                  
           15  E2_OTH_INSUREDS_EMPLE_ID_NUM         PIC X(20).                  
           15  E2_OTH_INSUREDS_EMPLR_NAME           PIC X(35).                  
           15  E2_OTH_INSUREDS_EMPLR_ADDR1          PIC X(35).                  
           15  E2_OTH_INSUREDS_EMPLR_ADDR2          PIC X(35).                  
           15  E2_OTH_INSUREDS_EMPLR_CITY           PIC X(30).                  
           15  E2_OTH_INSUREDS_EMPLR_STATE          PIC X(02).                  
           15  E2_OTH_INSUREDS_EMPLR_ZIP            PIC X(11).                  
           15  FILLER_060                           PIC X(20).                  
       10  E2_TOTAL_SVC_REC_LN_COUNT                PIC 9(03).                  
     07 E2_INIT6_END.                                                           
       10  E2_ERROR_MESSAGE_CODE                                                
            OCCURS 10 TIMES.                                                    
           15  E2_ERROR_MESSAGE_DATA                PIC X(03).                  
       10  E2_TOOTH_STATUSES OCCURS 35 TIMES.                                   
           15  E2_TOOTH_STATUS                      PIC X(02).                  
       10  E2_SPP_FIELDS.                                                       
           15  E2_PATIENT_ID_QUALIFIER              PIC X(02).                  
           15  E2_PATIENT_ID                        PIC X(30).                  
           15  E2_PAY_TO_PRV.                                                   
               20 E2_PAY_TO_PRV_LAST_NAME           PIC X(35).                  
               20 E2_PAY_TO_PRV_FIRST_NAME          PIC X(25).                  
               20 E2_PAY_TO_PRV_MIDDLE_NAME         PIC X(25).                  
               20 E2_PAY_TO_PRV_GENERATION          PIC X(10).                  
               20 E2_PAY_TO_PRV_ID_QUALIFIER        PIC X(02).                  
               20 E2_PAY_TO_PRV_ID                  PIC X(30).                  
               20 E2_PAY_TO_PRV_NATL_ID             PIC X(30).                  
               20 E2_PAY_TO_PRV_ADDR_LINE_1         PIC X(55).                  
               20 E2_PAY_TO_PRV_ADDR_LINE_2         PIC X(55).                  
               20 E2_PAY_TO_PRV_CITY                PIC X(30).                  
               20 E2_PAY_TO_PRV_STATE               PIC X(02).                  
               20 E2_PAY_TO_PRV_ZIP                 PIC X(15).                  
               20 E2_PAY_TO_SEC_ID_QUALIFIER        PIC X(03).                  
               20 E2_PAY_TO_SEC_ID                  PIC X(30).                  
               20 E2_PAY_TO_PRV_SPEC_CODE           PIC X(30).                  
               20 E2_PAY_TO_PRV_DESIGNATION         PIC X(02).                  
       10  E2_REND_PRV_STATE                        PIC X(02).                  
       10  E2_REND_PRV_ZIP                          PIC X(11).                  
       10  FILLER_061                               PIC X(10).                  
       10 E2_CLAIM_INTAKE_EDI_CLAIM_ID.                                         
           15 E2_CI_EDI_INTRCHG_KEY.                                            
              20 E2_CI_CCJJJ                       PIC X(5).                    
              20 E2_CI_INDICATOR                   PIC X.                       
              20 E2_CI_UNIQUE_KEY                  PIC X(6).                    
           15 E2_CI_NUM_OF_SPLITS                  PIC X(3).                    
           15 E2_KEY_FILLER_062                    PIC X(12).                   
       10  E2_SORT2_KEY.                                                        
            15  FILLER_063                          PIC X(07).                  
            15  E2_SORT2_TPA_VENDOR                 PIC X(05).                  
            15  E2_SORT2_HDR_TLR                    PIC X(01).                  
            15  FILLER_064                          PIC X(16).                  
       10  E2_SORT_KEY                              PIC X(57).                  
01  E3_DETAIL_REC REDEFINES ECR_REC.                                            
   05  E3_DETAIL_RECORD.                                                        
       10  FILLER_065                               PIC X(23).                  
       10  E3_OCCURS_COUNTS.                                                    
           15  E3_TOOTH_SURFACE_COUNT               PIC 9(03).                  
           15  E3_REMARKS_DET_COUNT                 PIC 9(03).                  
       10  E3_LOOP_2400_SVC_LN_INFO.                                            
           15  E3_SVC_LN_TYPE                       PIC X(01).                  
               88 E3_MEDICAL                    VALUE 'M'.                      
               88 E3_HOSPITAL                   VALUE 'H'.                      
               88 E3_DENTAL                     VALUE 'D'.                      
               88 E3_DRUG                       VALUE 'R'.                      
               88 E3_DME                        VALUE 'E'.                      
               88 E3_ANESTHESIA                 VALUE 'A'.                      
           15  E3_MEDICAL_SVC_LN.                                               
               20  E3_MEDICAL_PROC_CODE             PIC X(40).                  
               20  E3_MEDICAL_PROC_CODE_REDEF                                   
                       REDEFINES E3_MEDICAL_PROC_CODE.                          
                   25  E3_MED_PROC_CODE             PIC X(39).                  
                   25  E3_ELIG_SORTEAV_SPLIT_IND    PIC X(01).                  
               20  E3_MED_PROC_MODIFIER_1           PIC X(02).                  
               20  E3_MED_PROC_MODIFIER_2           PIC X(02).                  
               20  E3_MED_PROC_MODIFIER_3           PIC X(02).                  
               20  E3_MED_PROC_MODIFIER_4           PIC X(02).                  
               20  FILLER_066                       PIC X(05).                  
               20  E3_MEDICAL_CHARGES               PIC 9(08)V99.               
               20  E3_MEDICAL_UNITS                 PIC 9(15).                  
               20  E3_PLACE_OF_SVC                  PIC X(02).                  
               20  E3_TYPE_OF_SVC                   PIC X(02).                  
               20  E3_DIAGNOSIS_POINTER_1           PIC 9(02).                  
               20  E3_DIAGNOSIS_POINTER_2           PIC 9(02).                  
               20  E3_DIAGNOSIS_POINTER_3           PIC 9(02).                  
               20  E3_DIAGNOSIS_POINTER_4           PIC 9(02).                  
               20  FILLER_067                       PIC X(15).                  
               20  E3_EMERGENCY_IND                 PIC X(01).                  
               20  FILLER_068                       PIC X(16).                  
               20  E3_MED_ALLOWED_AMOUNT            PIC 9(08)V99.               
               20  E3_MED_UNITS_OF_MEASURE          PIC X(02).                  
               20  FILLER_069                       PIC X(66).                  
           15  E3_HOSPITAL_SVC_LN                                               
                 REDEFINES E3_MEDICAL_SVC_LN.                                   
               20  E3_REVENUE_CODE                  PIC X(40).                  
               20  E3_REVENUE_PROC_CODE             PIC X(40).                  
               20  E3_REVENUE_PROC_MODIFIER_1       PIC X(02).                  
               20  E3_REVENUE_PROC_MODIFIER_2       PIC X(02).                  
               20  E3_REVENUE_PROC_MODIFIER_3       PIC X(02).                  
               20  E3_REVENUE_PROC_MODIFIER_4       PIC X(02).                  
               20  FILLER_070                       PIC X(05).                  
               20  E3_REVENUE_CHARGE_AMOUNT         PIC 9(08)V99.               
               20  E3_REVENUE_UNITS                 PIC 9(15).                  
               20  E3_REVENUE_DAYS                  PIC 9(15).                  
               20  E3_HOSPITAL_ROOM_RATES           PIC 9(08)V99.               
               20  FILLER_071                       PIC X(05).                  
               20  E3_HOS_NOT_COV_AMOUNT            PIC 9(08)V99.               
               20  E3_HOS_ALLOWED_AMOUNT            PIC 9(08)V99.               
               20  FILLER_072                       PIC X(01).                  
               20  E3_REVENUE_UNITS_QUALIFIER       PIC X(02).                  
               20  FILLER_073                       PIC X(29).                  
           15  E3_DENTAL_SVC_LN                                                 
                 REDEFINES E3_MEDICAL_SVC_LN.                                   
               20  E3_ADA_CODE                      PIC X(40).                  
               20  E3_DEN_PROC_CODE_MOD_1           PIC X(02).                  
               20  E3_DEN_PROC_CODE_MOD_2           PIC X(02).                  
               20  E3_DEN_PROC_CODE_MOD_3           PIC X(02).                  
               20  E3_DEN_PROC_CODE_MOD_4           PIC X(02).                  
               20  FILLER_074                       PIC X(05).                  
               20  E3_DENTAL_CHARGE_AMOUNT          PIC 9(08)V99.               
               20  E3_DENTAL_PLACE_SVC              PIC X(02).                  
               20  E3_ORAL_CAVITY_DESIGNATION_1     PIC X(03).                  
               20  E3_ORAL_CAVITY_DESIGNATION_2     PIC X(03).                  
               20  E3_ORAL_CAVITY_DESIGNATION_3     PIC X(03).                  
               20  E3_ORAL_CAVITY_DESIGNATION_4     PIC X(03).                  
               20  E3_ORAL_CAVITY_DESIGNATION_5     PIC X(03).                  
               20  E3_PROSTHESIS_INLAY_CROWN        PIC X(01).                  
               20  E3_NUM_OF_PROCS                  PIC 9(15).                  
               20  E3_PRV_AGREEMENT                 PIC X(01).                  
               20  E3_PREDETERMINATION_IND          PIC X(01).                  
               20  E3_DIAGNOSIS_CODE_POINTER_1      PIC 9(02).                  
               20  E3_DIAGNOSIS_CODE_POINTER_2      PIC 9(02).                  
               20  E3_DIAGNOSIS_CODE_POINTER_3      PIC 9(02).                  
               20  E3_DIAGNOSIS_CODE_POINTER_4      PIC 9(02).                  
               20  E3_ADA_SET_NUMBER                PIC 9(01).                  
               20  E3_DEN_ALLOWED_AMOUNT            PIC 9(08)V99.               
               20  FILLER_075                       PIC X(83).                  
           15  E3_DRUG_SVC_LN                                                   
                 REDEFINES E3_MEDICAL_SVC_LN.                                   
               20  E3_PRESCRIPTION_NUM              PIC X(30).                  
               20  E3_NDC_CODE                      PIC X(40).                  
               20  FILLER_076                       PIC X(130).                 
           15  E3_DME_SVC_LN                                                    
                 REDEFINES E3_MEDICAL_SVC_LN.                                   
               20  E3_DME_SVC                       PIC X(40).                  
               20  E3_DME_PROC_CODE_MOD_1           PIC X(02).                  
               20  E3_DME_PROC_CODE_MOD_2           PIC X(02).                  
               20  E3_DME_PROC_CODE_MOD_3           PIC X(02).                  
               20  E3_DME_PROC_CODE_MOD_4           PIC X(02).                  
               20  E3_DME_TIME_QUALIFIER            PIC X(02).                  
               20  E3_DME_QUANTITY                  PIC 9(15).                  
               20  FILLER_077                       PIC X(05).                  
               20  E3_DME_RENTAL_PRICE              PIC 9(08)V99.               
               20  FILLER_078                       PIC X(05).                  
               20  E3_DME_PURCHASE_PRICE            PIC 9(08)V99.               
               20  E3_DME_BILL_FREQUENCY            PIC X(01).                  
               20  E3_DME_ALLOWED_AMOUNT            PIC 9(08)V99.               
               20  E3_DME_UNITS                     PIC 9(15).                  
               20  E3_DME_UNITS_OF_MEASURE          PIC X(02).                  
               20  E3_DME_PLACE_OF_SERVICE          PIC X(02).                  
               20  FILLER_079                       PIC X(75).                  
           15  E3_ANES_SVC_LN                                                   
                 REDEFINES E3_MEDICAL_SVC_LN.                                   
               20  E3_ANES_PROC_CODE                PIC X(40).                  
               20  E3_ANES_PROC_CODE_MOD_1          PIC X(02).                  
               20  E3_ANES_PROC_CODE_MOD_2          PIC X(02).                  
               20  E3_ANES_PROC_CODE_MOD_3          PIC X(02).                  
               20  E3_ANES_PROC_CODE_MOD_4          PIC X(02).                  
               20  E3_ANES_PLACE_SVC                PIC X(02).                  
               20  FILLER_080                       PIC X(05).                  
               20  E3_ANES_CHARGE_AMOUNT            PIC 9(08)V99.               
               20  E3_ANES_DIAGNOSIS_POINTER_1      PIC 9(02).                  
               20  E3_ANES_DIAGNOSIS_POINTER_2      PIC 9(02).                  
               20  E3_ANES_DIAGNOSIS_POINTER_3      PIC 9(02).                  
               20  E3_ANES_DIAGNOSIS_POINTER_4      PIC 9(02).                  
               20  E3_ANES_MINUTES                  PIC 9(13)V99.               
               20  E3_ANES_ALLOWED_AMOUNT           PIC 9(08)V99.               
               20  E3_ANES_UNITS_OF_MEASURE         PIC X(02).                  
               20  FILLER_081                       PIC X(100).                 
           15  E3_TOOTH_SURFACE_ID                                              
                 OCCURS 32 TIMES.                                               
               20  E3_DENTAL_CODE                   PIC X(30).                  
               20  E3_TOOTH_SURFACE_1               PIC X(02).                  
               20  E3_TOOTH_SURFACE_2               PIC X(02).                  
               20  E3_TOOTH_SURFACE_3               PIC X(02).                  
               20  E3_TOOTH_SURFACE_4               PIC X(02).                  
               20  E3_TOOTH_SURFACE_5               PIC X(02).                  
           15  FILLER_082                           PIC X(89).                  
           15  E3_PROCEDURE_CODE_QUALIFIER          PIC X(02).                  
           15  E3_SVD_DET_COUNT                     PIC 9(03).                  
           15  E3_CASA_DET_COUNT                    PIC 9(03).                  
           15  E3_CASB_DET_COUNT                    PIC 9(03).                  
           15  E3_LOOP_2400_ADDTNL_LN_INFO.                                     
            17  ECRC4_INIT7.                                                    
             20  E3_RENDER_FAC_NATL_ID            PIC X(20).                    
             20  E3_RENDER_PRV_NATL_ID            PIC X(20).                    
             20  E3_REFERR_PRV_NATL_ID            PIC X(20).                    
             20  FILLER_083                       PIC X(30).                    
             20  E3_SVC_TAX_AMOUNT                PIC S9(08)V99.                
             20  E3_FAC_TAX_AMOUNT                PIC S9(08)V99.                
             20  E3_SALES_TAX_AMOUNT              PIC S9(08)V99.                
             20  E3_LINE_ITEM_CONTROL_NBR         PIC X(30).                    
             20  E3_NDC_CODE_LIN                  PIC X(48).                    
             20  FILLER_084                       PIC X(933).                   
               20  E3_DME_MED_NEC_IND               PIC X(02).                  
             17  ECRC4_INIT8.                                                   
               20  E3_MED_HOS_SVC_FRM               PIC X(08).                  
               20  E3_MED_HOS_SVC_FRM_CCYYMMDD REDEFINES                        
                   E3_MED_HOS_SVC_FRM.                                          
                   25 E3_MED_HOS_SVC_FRM_CC         PIC X(02).                  
                   25 E3_MED_HOS_SVC_FRM_YYMMDD                                 
                                                    PIC X(06).                  
               20  E3_MED_HOS_SVC_TO                PIC X(08).                  
               20  E3_MED_HOS_SVC_TO_CCYYMMDD REDEFINES                         
                   E3_MED_HOS_SVC_TO.                                           
                   25 E3_MED_HOS_SVC_TO_CC          PIC X(02).                  
                   25 E3_MED_HOS_SVC_TO_YYMMDD                                  
                                                    PIC X(06).                  
               20  E3_DT_RX_FRM                     PIC X(08).                  
               20  E3_DT_RX_FRM_CCYYMMDD REDEFINES                              
                   E3_DT_RX_FRM.                                                
                   25  E3_DT_RX_FRM_CC              PIC X(02).                  
                   25  E3_DT_RX_FRM_YYMMDD          PIC X(06).                  
               20  E3_DT_RX_TO                      PIC X(08).                  
               20  E3_DT_RX_TO_CCYYMMDD REDEFINES                               
                   E3_DT_RX_TO.                                                 
                   25  E3_DT_RX_TO_CC               PIC X(02).                  
                   25  E3_DT_RX_TO_YYMMDD           PIC X(06).                  
               20  E3_DME_DT_SVC_FRM                PIC X(08).                  
               20  E3_DME_DT_SVC_FRM_CCYYMMDD REDEFINES                         
                   E3_DME_DT_SVC_FRM.                                           
                   25 E3_DME_DT_SVC_FRM_CC          PIC X(02).                  
                   25 E3_DME_DT_SVC_FRM_YYMMDD                                  
                                                    PIC X(06).                  
               20  E3_DME_DT_SVC_TO                 PIC X(08).                  
               20  E3_DME_DT_SVC_TO_CCYYMMDD REDEFINES                          
                   E3_DME_DT_SVC_TO.                                            
                   25 E3_DME_DT_SVC_TO_CC           PIC X(02).                  
                   25 E3_DME_DT_SVC_TO_YYMMDD                                   
                                                    PIC X(06).                  
               20  E3_ANES_SVC_FRM                  PIC X(08).                  
               20  E3_ANES_SVC_FRM_CCYYMMDD REDEFINES                           
                   E3_ANES_SVC_FRM.                                             
                   25 E3_ANES_SVC_FRM_CC            PIC X(02).                  
                   25 E3_ANES_SVC_FRM_YYMMDD        PIC X(06).                  
               20  E3_ANES_SVC_TO                   PIC X(08).                  
               20  E3_ANES_SVC_TO_CCYYMMDD REDEFINES                            
                   E3_ANES_SVC_TO.                                              
                   25 E3_ANES_SVC_TO_CC             PIC X(02).                  
                   25 E3_ANES_SVC_TO_YYMMDD         PIC X(06).                  
               20  E3_TEST_RESULTS                  PIC X(20).                  
               20  FILLER_085                       PIC X(30).                  
            17  ECRC4_INIT8_END.                                                
             20  FILLER_086                       PIC X(05).                    
             20  E3_PRESCRIPTION_CHARGE           PIC S9(08)V99.                
             20  FILLER_087                       PIC X(05).                    
             20  E3_DME_CHARGE_AMOUNT             PIC S9(08)V99.                
             20  FILLER_088                       PIC X(15).                    
             20  E3_REMARKS                                                     
                   OCCURS 10 TIMES.                                             
                 25  E3_REMARKS_DT_DATA           PIC X(80).                    
             20  FILLER_089                       PIC X(05).                    
             20  E3_TPO_ALLOWED_AMOUNT            PIC 9(08)V99.                 
             20  E3_TPO_REJECTION_MESSAGE         PIC X(02).                    
     12 E3_INIT9.                                                               
           15  E3_PRIOR_PLCMT_DT                    PIC X(08).                  
           15  E3_PRIOR_PLCMT_DT_CCYYMMDD REDEFINES                             
               E3_PRIOR_PLCMT_DT.                                               
               20  E3_PRIOR_PLCMT_DT_CC             PIC X(02).                  
               20  E3_PRIOR_PLCMT_DT_YYMMDD         PIC X(06).                  
           15  E3_TPO_ID_NUMBER_DET                 PIC X(30).                  
           15  E3_TPO_MESSAGE_CODE_1                PIC X(04).                  
           15  E3_TPO_MESSAGE_CODE_2                PIC X(04).                  
           15  E3_TPO_DISCOUNT_AMOUNT               PIC 9(08)V99.               
           15  E3_TPO_PRICING_METHD_CODE_DET        PIC X(02).                  
           15  FILLER_090                           PIC X(25).                  
           15  E3_LOOP_2420_PRV_INFO.                                           
               20  E3_RENDER_FAC_NAME               PIC X(35).                  
               20  E3_RENDER_FAC_ID_NUM             PIC X(20).                  
               20  E3_RENDER_PRV_LAST_NAME          PIC X(35).                  
               20  E3_RENDER_PRV_FIRST_NAME         PIC X(25).                  
               20  E3_RENDER_PRV_MIDDLE_NAME        PIC X(25).                  
               20  E3_RENDER_PRV_GENERATION         PIC X(10).                  
               20  E3_RENDER_PRV_ID_NUM             PIC X(20).                  
               20  E3_RENDER_PRV_SPCLTY_CODE        PIC X(03).                  
               20  FILLER_091                       PIC X(113).                 
               20  E3_REFERR_PRV_LAST_NAME          PIC X(35).                  
               20  E3_REFERR_PRV_FIRST_NAME         PIC X(25).                  
               20  E3_REFERR_PRV_MIDDLE_NAME        PIC X(25).                  
               20  E3_REFERR_PRV_GENERATION         PIC X(10).                  
               20  E3_REFERR_PRV_ID_NUM             PIC X(20).                  
               20  E3_LIMITING_CHARGE               PIC 9(13)V99.               
           15  FILLER_092                           PIC X(05).                  
           15  E3_LOOP_2430_COB_SVC_LN_ADJ OCCURS 2 TIMES.                      
               20  FILLER_093                       PIC X(05).                  
               20  E3_ADJ_AMT_PAID_BY_OTH_INS       PIC 9(08)V99.               
               20  E3_ADJ_PROC_CODE                 PIC X(40).                  
               20  E3_ADJ_PROC_CODE_MOD_1           PIC X(02).                  
               20  E3_ADJ_PROC_CODE_MOD_2           PIC X(02).                  
               20  E3_ADJ_PROC_CODE_MOD_3           PIC X(02).                  
               20  E3_ADJ_PROC_CODE_MOD_4           PIC X(02).                  
               20  E3_ADJ_REVENUE_CODE              PIC X(40).                  
               20  E3_ADJ_PAID_UNITS                PIC 9(15).                  
               20  E3_ADJ_REBUNDLED_LN_NUM          PIC 9(06).                  
               20  FILLER_094                       PIC X(35).                  
               20  E3_ADJ_OI_DT_SVC_FRM             PIC X(08).                  
               20  E3_ADJ_OI_DT_SVC_FRM_CCYYMMDD REDEFINES                      
                   E3_ADJ_OI_DT_SVC_FRM.                                        
                   25  E3_ADJ_OI_DT_SVC_FRM_CC      PIC X(02).                  
                   25  E3_ADJ_OI_DT_SVC_FRM_YYMMDD                              
                                                    PIC X(06).                  
               20  E3_ADJ_OI_DT_SVC_TO              PIC X(08).                  
               20  E3_ADJ_OI_DT_SVC_TO_CCYYMMDD REDEFINES                       
                   E3_ADJ_OI_DT_SVC_TO.                                         
                   25  E3_ADJ_OI_DT_SVC_TO_CC       PIC X(02).                  
                   25  E3_ADJ_OI_DT_SVC_TO_YYMMDD                               
                                                    PIC X(06).                  
               20  E3_OTH_INS_CARRIER_ID            PIC X(80).                  
       10  E3_CAS_DET_ONE OCCURS 5 TIMES.                                       
           15  E3_ADJ_OI_GROUP_CODEA                PIC X(02).                  
           15  E3_ADJ_OI_GROUP_AREAA OCCURS 6 TIMES.                            
               20 E3_ADJ_OI_DISALLOWED_AMOUNTA      PIC 9(08)V99.               
               20 E3_ADJ_OI_ADJ_REASON_CODEA        PIC X(05).                  
               20 E3_ADJ_SERVICES_ADJUSTEDA         PIC 9(15).                  
       10  E3_AMBULANCE_CERTIFICATION.                                          
           15  E3_AMBULANCE_CERT_CODE1              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE2              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE3              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE4              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE5              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE6              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE7              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE8              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE9              PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE10             PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE11             PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE12             PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE13             PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE14             PIC X(03).                  
           15  E3_AMBULANCE_CERT_CODE15             PIC X(03).                  
       10  E3_AMBULANCE_EMERGENCY_IND               PIC X.                      
       10  E3_AMBULANCE_PAT_COUNT                   PIC 99.                     
       10  E3_OB_ANES_ADDITIONAL_UNITS              PIC 9(15).                  
       10  FILLER_095                               PIC X(228).                 
       10  E3_REMAINING_PAT_LIABILITY             PIC 9(13)V99.                 
       10  E3_PROCLAIM_DETAIL_AREA.                                             
           15  E3_PROV_DET_TYPE_SERVICE             PIC X(03).                  
           15  E3_PROV_DET_TLC                      PIC X(03).                  
           15  FILLER_096                           PIC X(04).                  
           15  E3_PROV_DET_PREPRICE_AMT             PIC 9(07)V99.               
           15  E3_PROV_DET_PREPRICE_IND             PIC X(01).                  
           15  E3_DET_RMK_CODE                      PIC X(01).                  
           15  E3_DET_PROC_TYPE                     PIC X(01).                  
           15  E3_DET_PROC_TYPE2                    PIC X(01).                  
           15  E3_PROV_DET_PAID                     PIC 9(07)V99.               
           15  E3_PROV_DET_APPROVED                 PIC 9(07)V99.               
           15  E3_VALID_MOD_1                       PIC X(01).                  
           15  E3_VALID_MOD_2                       PIC X(01).                  
           15  E3_VALID_MOD_3                       PIC X(01).                  
           15  E3_VALID_MOD_4                       PIC X(01).                  
           15  E3_RP_EDIT_TOOTH                     PIC X(01).                  
           15  E3_RP_EDIT_SURF                      PIC X(01).                  
           15  E3_MAP_MOD_1                         PIC X(02).                  
           15  E3_MAP_MOD_2                         PIC X(02).                  
           15  E3_MAP_MOD_3                         PIC X(02).                  
           15  E3_MAP_MOD_4                         PIC X(02).                  
           15  FILLER_097                           PIC X(04).                  
           15  E3_RET_CODE                          PIC X(01).                  
           15  E3_RET_PROC_CODE                     PIC X(05).                  
           15  E3_RET_TOOTH_NO                      PIC X(02).                  
           15  E3_RET_QUADRANT                      PIC X(02).                  
           15  E3_RET_SURFACE                       PIC X(04).                  
           15  E3_CAS_FIELDS OCCURS 2 TIMES.                                    
               20 E3_DET_LIMITING_CHARGE            PIC 9(08)V99.               
               20 E3_DET_DEDUCTIBLE_AMOUNT          PIC 9(08)V99.               
               20 E3_DET_MAJ_MED_AMOUNT             PIC 9(08)V99.               
           15  E3_GCDOCDET_TYP_SRV_HIER_IND         PIC X(01).                  
           15  E3_GCDOCDET_NO_OCCUR_QUAL            PIC X(01).                  
           15  E3_GCDOCDET_TYPE_PROC_CODE           PIC X(01).                  
           15  FILLER_098                           PIC X(10).                  
           15  E3_CAS_FIELDS_TWO OCCURS 2 TIMES.                                
               20 E3_DET_OI_DISALLOW_AMT            PIC 9(08)V99.               
               20 E3_DET_OI_PAID_FLAG               PIC X(01).                  
           15  E3_PROCLAIM_UNITS_QUAL               PIC X(01).                  
           15 FILLER_099                            PIC X(131).                 
       10  E3_5010_PWK_AREA_COUNT              PIC 9(03).                       
       10  E3_5010_PWK_AREA                                                     
                 OCCURS 10 TIMES.                                               
           15  E3_5010_DOCUMENT_TYPE             PIC X(02).                     
           15  E3_5010_RPT_TRANMISSION_CODE      PIC X(02).                     
           15  E3_5010_ATTACHMENT_CNTRL_NUM      PIC X(20).                     
       10  FILLER_100                              PIC X(37).                   
       10  E3_LOOP_2310_AMBULANCE_INFO.                                         
         15  E3_AMBULANCE_PICK_UP_LOC.                                          
             20  E3_AMBULANCE_PICK_UP_ADDR1     PIC X(55).                      
             20  E3_AMBULANCE_PICK_UP_ADDR2     PIC X(55).                      
             20  E3_AMBULANCE_PICK_UP_CITY      PIC X(30).                      
             20  E3_AMBULANCE_PICK_UP_STATE     PIC X(02).                      
             20  E3_AMBULANCE_PICK_UP_ZIP       PIC 9(15).                      
         15  E3_AMBULANCE_DROP_OFF_LOC.                                         
               20  E3_AMBULANCE_DROP_OFF_ADDR1    PIC X(55).                    
               20  E3_AMBULANCE_DROP_OFF_ADDR2    PIC X(55).                    
               20  E3_AMBULANCE_DROP_OFF_CITY     PIC X(30).                    
               20  E3_AMBULANCE_DROP_OFF_STATE    PIC X(02).                    
               20  E3_AMBULANCE_DROP_OFF_ZIP      PIC 9(15).                    
         15  E3_AMBULANCE_TRANSPORT_DIST     PIC 9(15).                         
       10  E3_CAS_DET_TWO OCCURS 5 TIMES.                                       
           15  E3_ADJ_OI_GROUP_CODEB               PIC X(02).                   
           15  E3_ADJ_OI_GROUP_AREAB OCCURS 6 TIMES.                            
               20 E3_ADJ_OI_DISALLOWED_AMOUNTB     PIC 9(08)V99.                
               20 E3_ADJ_OI_ADJ_REASON_CODEB       PIC X(05).                   
               20 E3_ADJ_SERVICES_ADJUSTEDB        PIC 9(15).                   
       10  FILLER_101                              PIC X(25).                   
       10  E3_CARELINK_INFO_PROVIDER.                                           
           15  E3_RENDER_PRV_SEC_ID_QUAL_DET       PIC X(02).                   
           15  E3_RENDER_PRV_SEC_ID_DET            PIC X(20).                   
           15  E3_RENDER_PRV_DESIGNATION_DET       PIC X(02).                   
           15  E3_RENDER_PRV_ROLE_DET              PIC X(01).                   
           15  E3_RENDER_PRV_EIN_IND_DET           PIC X(02).                   
           15  FILLER_102                          PIC X(53).                   
       10  E3_CARELINK_INFO_SERVICE.                                            
           15  E3_CP_MESSAGE_CODE_1                PIC X(04).                   
           15  E3_CP_MESSAGE_CODE_2                PIC X(04).                   
           15  E3_DISALLOWED_AMT_TBP               PIC 9(08)V99.                
           15  E3_CODE_REVIEW_POINTER              PIC X(04).                   
           15  E3_CODE_REVIEW_PROC_CODE            PIC X(05).                   
           15  E3_CODE_REVIEW_MODIFIER_1           PIC X(02).                   
           15  E3_CODE_REVIEW_MODIFIER_2           PIC X(02).                   
           15  E3_CODE_REVIEW_MODIFIER_3           PIC X(02).                   
           15  E3_CODE_REVIEW_MODIFIER_4           PIC X(02).                   
           15  E3_SERVICE_LINE_ID                  PIC X(20).                   
           15  E3_SERVICE_LINE_INCLUSION_ID        PIC X(01).                   
           15  FILLER_103                          PIC X(50).                   
       10  E3_MED_PROC_CODE_MAP                    PIC X(5).                    
       10  E3_CP_MESSAGE_CODE_1_MAP                PIC X(04).                   
       10  E3_CP_MESSAGE_CODE_2_MAP                PIC X(04).                   
       10  E3_BN_DET_ERROR_TYPE                    PIC X(01).                   
       10  E3_BN_DET_ERROR_CODE                    PIC 9(04).                   
       10  E3_BN_PTNR_REM_CD                       PIC X(20).                   
       10  E3_BN_PRC_RNC_CD                        PIC X(04).                   
       10  E3_BN_PRP_RNC_CD                        PIC X(04).                   
       10  E3_BN_PMHS_RNC_CD                       PIC X(04).                   
       10  E3_BN_PROCESS_IND                       PIC X(03).                   
       10  E3_BN_INCLUDE_IND                       PIC X(01).                   
       10  E3_BN_ADD_INFO_IND                      PIC X(01).                   
       10  E3_BN_RNC_NOT_FOUND                     PIC X(01).                   
       10  E3_BN_DET_DISCOUNT_AMT                  PIC S9(06)V99.               
       10  E3_BN_DET_ALLOWED_AMT                   PIC S9(06)V99.               
       10  E3_BN_DET_PCT_IND                       PIC X(01).                   
       10  E3_BN_DET_CC_UPDT                       PIC X(01).                   
       10  E3_BN_DET_CC_PROC                       PIC X(05).                   
       10  E3_BN_DET_CC_MOD                        PIC X(02).                   
       10  E3_BN_DET_CC_ERR                        PIC X(01).                   
       10  E3_BN_DET_CC_PTR                        PIC 9(02).                   
       10  E3_BN_DET_AWP                           PIC 9(02).                   
       10  E3_BN_DET_ADJ_REVIEW                    PIC X(01).                   
       10  E3_BN_DET_INCLUDE_IND                   PIC X(01).                   
       10  E3_MCX_DET_LIMITING_CHARGE_1          PIC 9(08)V99.                  
       10  FILLER_104                              PIC X(310).                  
       10 E3_CLAIM_INTAKE_EDI_CLAIM_ID.                                         
           15 E3_CI_EDI_INTRCHG_KEY.                                            
              20 E3_CI_CCJJJ                       PIC X(5).                    
              20 E3_CI_INDICATOR                   PIC X.                       
              20 E3_CI_UNIQUE_KEY                  PIC X(6).                    
           15 E3_CI_NUM_OF_SPLITS                  PIC X(3).                    
           15 E3_KEY_FILLER_105                    PIC X(12).                   
       10  E3_SORT2_KEY.                                                        
           15  FILLER_106                          PIC X(07).                   
           15  E3_SORT2_TPA_VENDOR                 PIC X(05).                   
           15  E3_SORT2_HDR_TLR                    PIC X(01).                   
           15  E3_DATE_SERV_THRU                   PIC X(8).                    
           15  E3_DATE_OF_SERVICE                  PIC X(8).                    
       10  E3_SORT_KEY                             PIC X(57).                   
