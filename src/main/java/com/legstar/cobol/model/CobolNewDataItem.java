/*******************************************************************************
 * Copyright (c) 2015 LegSem.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Contributors:
 *     LegSem - initial API and implementation
 ******************************************************************************/
package com.legstar.cobol.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Model of a COBOL data entry.
 *
 */
public class CobolNewDataItem {

    /** Level in the hierarchy this element was parsed from. */
  //  private int levelNumber = 1;

    /** Cobol element name. */
    private String name = "FILLER";

    private int size;

    private String type;

    /** Line number in the original source file this element was parsed from. */
  //  private int srceLine;

    /** Ordered list of direct children. */
    private List <CobolNewDataItem> children = new LinkedList <CobolNewDataItem>();


    /**
     * No argument constructor.
     */
    public CobolNewDataItem() {

    }

    /**
     * Constructor used when the level number will be determined at a later
     * time.
     *
     * @param cobolName the data item COBOL name
     */
    public CobolNewDataItem(final String cobolName) {
        this.name = cobolName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    //    /**
//     * Constructor used when the level number is known.
//     *
//     * @param levelNumber the data item COBOL name
//     * @param cobolName the data item COBOL name
//     */
//   public CobolNewDataItem(final int levelNumber, final String cobolName) {
//        this.levelNumber = levelNumber;
//        this.cobolName = cobolName;
//    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the Level in the hierarchy
     */
  /*  public int getLevelNumber() {
        return levelNumber;
    }*/

    /**
     * @param levelNumber the Level in the hierarchy to set
     */
   /* public void setLevelNumber(final int levelNumber) {
        this.levelNumber = levelNumber;
    }*/



    /**
     * @return the ordered list of direct children
     */
    public List <CobolNewDataItem> getChildren() {
        return children;
    }

    /**
     * @param children the ordered list of direct children to set
     */
    public void setChildren(final List <CobolNewDataItem> children) {
        this.children = children;
    }

    /**
     * @return true if this data item is a structure (group).
     */
   /* public boolean isStructure() {
        return (getChildren().size() > 0);
    }
*/
    /**
     * Represents a range between two literals.
     */
   /* public static class Range {
        *//** Range start. *//*
        private String _from;
        *//** Range stop. *//*
        private String _to;

        *//**
         * Constructor for immutable class.
         *
         * @param from range start
         * @param to range stop
         *//*
        public Range(final String from, final String to) {
            _from = from;
            _to = to;
        }*/

        /**
         * @return the range start
         */
    /*    public String getFrom() {
            return _from;
        }*/

        /**
         * @return the range stop
         */
   /*     public String getTo() {
            return _to;
        }

        *//** {@inheritDoc} *//*
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append('{');
            sb.append("from:" + getFrom());
            sb.append(',');
            sb.append("to:" + getTo());
            sb.append('}');
            return sb.toString();
        }
    }*/




    /**
     * Adds list elements to a string builder.
     *
     * @param sb the string builder
     * @param list list of elements
     * @param title name of elements list
     */
    private void toStringList(final StringBuilder sb, final List < ? > list,
                              final String title) {
        sb.append(',');
        sb.append(title + ":[");
        boolean first = true;
        for (Object child : list) {
            if (!first) {
                sb.append(",");
            } else {
                first = false;
            }
            sb.append(child.toString());
        }
        sb.append("]");
    }

}
