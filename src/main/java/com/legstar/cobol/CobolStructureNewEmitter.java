package com.legstar.cobol;

import com.legstar.cobol.model.CobolDataItem;
import com.legstar.cobol.model.CobolDataItem.Range;
import com.legstar.cobol.model.CobolNewDataItem;
import com.legstar.coxb.CobolUsage.Usage;

import org.antlr.runtime.*;
import org.antlr.runtime.BitSet;
import org.antlr.runtime.tree.*;

import java.util.*;


public class CobolStructureNewEmitter extends TreeParser {

    public static final String[] tokenNames = new String[]{
            "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ALL_CONSTANT", "ALPHANUM_LITERAL_FRAGMENT",
            "ALPHANUM_LITERAL_STRING", "APOST", "ARE_KEYWORD", "ASCENDING_KEYWORD",
            "BINARY_KEYWORD", "BLANK_KEYWORD", "BY_KEYWORD", "CHARACTER_KEYWORD",
            "CONDITION_LEVEL", "CONTINUATION_CHAR", "CONTINUED_ALPHANUM_LITERAL_FRAGMENT",
            "DATA_ITEM_LEVEL", "DATA_NAME", "DATE_FORMAT_KEYWORD", "DATE_KEYWORD",
            "DATE_PATTERN", "DBCS_LITERAL_STRING", "DECIMAL_POINT", "DEPENDING_KEYWORD",
            "DESCENDING_KEYWORD", "DISPLAY_1_KEYWORD", "DISPLAY_KEYWORD", "DOUBLE_FLOAT_KEYWORD",
            "EXTERNAL_KEYWORD", "FLOAT_PART2", "FUNCTION_POINTER_KEYWORD", "GLOBAL_KEYWORD",
            "GROUP_USAGE_KEYWORD", "HEX_LITERAL_STRING", "HIGH_VALUE_CONSTANT", "INDEXED_KEYWORD",
            "INDEX_KEYWORD", "INT", "IS_KEYWORD", "JUSTIFIED_KEYWORD", "KEY_KEYWORD",
            "LEFT_KEYWORD", "LETTER", "LOW_VALUE_CONSTANT", "NATIONAL_HEX_LITERAL_STRING",
            "NATIONAL_KEYWORD", "NATIONAL_LITERAL_STRING", "NATIVE_BINARY_KEYWORD",
            "NEWLINE", "NULL_CONSTANT", "OCCURS_KEYWORD", "ON_KEYWORD", "PACKED_DECIMAL_KEYWORD",
            "PERIOD", "PICTURE_CHAR", "PICTURE_KEYWORD", "PICTURE_PART", "POINTER_KEYWORD",
            "PROCEDURE_POINTER_KEYWORD", "QUOTE", "QUOTE_CONSTANT", "REDEFINES_KEYWORD",
            "RENAMES_KEYWORD", "RENAMES_LEVEL", "RIGHT_KEYWORD", "SEPARATE_KEYWORD",
            "SIGNED_INT", "SIGN_KEYWORD", "SIGN_LEADING_KEYWORD", "SIGN_TRAILING_KEYWORD",
            "SINGLE_FLOAT_KEYWORD", "SPACE", "SPACE_CONSTANT", "SYNCHRONIZED_KEYWORD",
            "THROUGH_KEYWORD", "TIMES_KEYWORD", "TO_KEYWORD", "USAGE_KEYWORD", "VALUE_KEYWORD",
            "WHEN_KEYWORD", "WHITESPACE", "ZERO_CONSTANT", "ZERO_LITERAL_STRING",
            "BINARY", "BLANKWHENZERO", "CONDITION", "DATA_ITEM", "DATEFORMAT", "DECIMAL_LITERAL",
            "DEPENDINGON", "DISPLAY", "DISPLAY1", "DOUBLEFLOAT", "EXTERNAL", "FIXEDARRAY",
            "FLOAT_LITERAL", "FUNCTIONPOINTER", "GLOBAL", "GROUPUSAGENATIONAL", "HBOUND",
            "INDEX", "JUSTIFIEDRIGHT", "KEY", "LBOUND", "LEADING", "LEFT", "LEVEL",
            "LITERAL", "NAME", "NATIONAL", "NATIVEBINARY", "PACKEDDECIMAL", "PICTURE",
            "PICTURESTRING", "POINTER", "PROCEDUREPOINTER", "RANGE", "REDEFINES",
            "RENAME", "RIGHT", "SEPARATE", "SIGN", "SINGLEFLOAT", "SYNCHRONIZED",
            "TRAILING", "USAGE", "VALUE", "VARARRAY"
    };
    public static final int EOF = -1;
    public static final int ALL_CONSTANT = 4;
    public static final int ALPHANUM_LITERAL_FRAGMENT = 5;
    public static final int ALPHANUM_LITERAL_STRING = 6;
    public static final int APOST = 7;
    public static final int ARE_KEYWORD = 8;
    public static final int ASCENDING_KEYWORD = 9;
    public static final int BINARY_KEYWORD = 10;
    public static final int BLANK_KEYWORD = 11;
    public static final int BY_KEYWORD = 12;
    public static final int CHARACTER_KEYWORD = 13;
    public static final int CONDITION_LEVEL = 14;
    public static final int CONTINUATION_CHAR = 15;
    public static final int CONTINUED_ALPHANUM_LITERAL_FRAGMENT = 16;
    public static final int DATA_ITEM_LEVEL = 17;
    public static final int DATA_NAME = 18;
    public static final int DATE_FORMAT_KEYWORD = 19;
    public static final int DATE_KEYWORD = 20;
    public static final int DATE_PATTERN = 21;
    public static final int DBCS_LITERAL_STRING = 22;
    public static final int DECIMAL_POINT = 23;
    public static final int DEPENDING_KEYWORD = 24;
    public static final int DESCENDING_KEYWORD = 25;
    public static final int DISPLAY_1_KEYWORD = 26;
    public static final int DISPLAY_KEYWORD = 27;
    public static final int DOUBLE_FLOAT_KEYWORD = 28;
    public static final int EXTERNAL_KEYWORD = 29;
    public static final int FLOAT_PART2 = 30;
    public static final int FUNCTION_POINTER_KEYWORD = 31;
    public static final int GLOBAL_KEYWORD = 32;
    public static final int GROUP_USAGE_KEYWORD = 33;
    public static final int HEX_LITERAL_STRING = 34;
    public static final int HIGH_VALUE_CONSTANT = 35;
    public static final int INDEXED_KEYWORD = 36;
    public static final int INDEX_KEYWORD = 37;
    public static final int INT = 38;
    public static final int IS_KEYWORD = 39;
    public static final int JUSTIFIED_KEYWORD = 40;
    public static final int KEY_KEYWORD = 41;
    public static final int LEFT_KEYWORD = 42;
    public static final int LETTER = 43;
    public static final int LOW_VALUE_CONSTANT = 44;
    public static final int NATIONAL_HEX_LITERAL_STRING = 45;
    public static final int NATIONAL_KEYWORD = 46;
    public static final int NATIONAL_LITERAL_STRING = 47;
    public static final int NATIVE_BINARY_KEYWORD = 48;
    public static final int NEWLINE = 49;
    public static final int NULL_CONSTANT = 50;
    public static final int OCCURS_KEYWORD = 51;
    public static final int ON_KEYWORD = 52;
    public static final int PACKED_DECIMAL_KEYWORD = 53;
    public static final int PERIOD = 54;
    public static final int PICTURE_CHAR = 55;
    public static final int PICTURE_KEYWORD = 56;
    public static final int PICTURE_PART = 57;
    public static final int POINTER_KEYWORD = 58;
    public static final int PROCEDURE_POINTER_KEYWORD = 59;
    public static final int QUOTE = 60;
    public static final int QUOTE_CONSTANT = 61;
    public static final int REDEFINES_KEYWORD = 62;
    public static final int RENAMES_KEYWORD = 63;
    public static final int RENAMES_LEVEL = 64;
    public static final int RIGHT_KEYWORD = 65;
    public static final int SEPARATE_KEYWORD = 66;
    public static final int SIGNED_INT = 67;
    public static final int SIGN_KEYWORD = 68;
    public static final int SIGN_LEADING_KEYWORD = 69;
    public static final int SIGN_TRAILING_KEYWORD = 70;
    public static final int SINGLE_FLOAT_KEYWORD = 71;
    public static final int SPACE = 72;
    public static final int SPACE_CONSTANT = 73;
    public static final int SYNCHRONIZED_KEYWORD = 74;
    public static final int THROUGH_KEYWORD = 75;
    public static final int TIMES_KEYWORD = 76;
    public static final int TO_KEYWORD = 77;
    public static final int USAGE_KEYWORD = 78;
    public static final int VALUE_KEYWORD = 79;
    public static final int WHEN_KEYWORD = 80;
    public static final int WHITESPACE = 81;
    public static final int ZERO_CONSTANT = 82;
    public static final int ZERO_LITERAL_STRING = 83;
    public static final int BINARY = 84;
    public static final int BLANKWHENZERO = 85;
    public static final int CONDITION = 86;
    public static final int DATA_ITEM = 87;
    public static final int DATEFORMAT = 88;
    public static final int DECIMAL_LITERAL = 89;
    public static final int DEPENDINGON = 90;
    public static final int DISPLAY = 91;
    public static final int DISPLAY1 = 92;
    public static final int DOUBLEFLOAT = 93;
    public static final int EXTERNAL = 94;
    public static final int FIXEDARRAY = 95;
    public static final int FLOAT_LITERAL = 96;
    public static final int FUNCTIONPOINTER = 97;
    public static final int GLOBAL = 98;
    public static final int GROUPUSAGENATIONAL = 99;
    public static final int HBOUND = 100;
    public static final int INDEX = 101;
    public static final int JUSTIFIEDRIGHT = 102;
    public static final int KEY = 103;
    public static final int LBOUND = 104;
    public static final int LEADING = 105;
    public static final int LEFT = 106;
    public static final int LEVEL = 107;
    public static final int LITERAL = 108;
    public static final int NAME = 109;
    public static final int NATIONAL = 110;
    public static final int NATIVEBINARY = 111;
    public static final int PACKEDDECIMAL = 112;
    public static final int PICTURE = 113;
    public static final int PICTURESTRING = 114;
    public static final int POINTER = 115;
    public static final int PROCEDUREPOINTER = 116;
    public static final int RANGE = 117;
    public static final int REDEFINES = 118;
    public static final int RENAME = 119;
    public static final int RIGHT = 120;
    public static final int SEPARATE = 121;
    public static final int SIGN = 122;
    public static final int SINGLEFLOAT = 123;
    public static final int SYNCHRONIZED = 124;
    public static final int TRAILING = 125;
    public static final int USAGE = 126;
    public static final int VALUE = 127;
    public static final int VARARRAY = 128;

    // delegates
    public TreeParser[] getDelegates() {
        return new TreeParser[]{};
    }

    // delegators


    public Map<String, CobolDataItem> getMainDataEntries() {
        return mainDataEntries;
    }

    public void setMainDataEntries(Map<String, CobolDataItem> mainDataEntries) {
        this.mainDataEntries = mainDataEntries;
    }

    public Map<String, CobolNewDataItem> getMainDataEntriesMap() {
        return mainDataEntriesMap;
    }

    public void setMainDataEntriesMap(Map<String, CobolNewDataItem> mainDataEntriesMap) {
        this.mainDataEntriesMap = mainDataEntriesMap;
    }

    public CobolStructureNewEmitter(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }

    public CobolStructureNewEmitter(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }


    @Override
    public String[] getTokenNames() {
        return CobolStructureNewEmitter.tokenNames;
    }

    @Override
    public String getGrammarFileName() {
        return "com\\legstar\\cobol\\CobolStructureEmitter.g";
    }

    private Map<String, CobolDataItem> mainDataEntries;
    private Map<String, CobolNewDataItem> mainDataEntriesMap;

    // $ANTLR start "cobdata"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:33:1: cobdata[List < CobolDataItem > dataEntries] : ( data_entry[$dataEntries] )* ;
    public final void cobdata(Map<String, CobolDataItem> dataEntries,
                              Map<String, CobolNewDataItem> dataEntriesMap) throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:34:6: ( ( data_entry[$dataEntries] )* )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:34:9: ( data_entry[$dataEntries] )*
            {
                // com\\legstar\\cobol\\CobolStructureEmitter.g:34:9: ( data_entry[$dataEntries] )*
                loop1:
                while (true) {
                    int alt1 = 2;
                    int LA1_0 = input.LA(1);
                    if (((LA1_0 >= CONDITION && LA1_0 <= DATA_ITEM) || LA1_0 == RENAME)) {
                        alt1 = 1;
                    }

                    switch (alt1) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:34:9: data_entry[$dataEntries]
                        {
                            pushFollow(FOLLOW_data_entry_in_cobdata57);
                            data_entry(dataEntries, dataEntriesMap);
                            state._fsp--;

                        }
                        break;

                        default:
                            break loop1;
                    }
                }

            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "cobdata"


    protected static class data_entry_scope {
        CobolDataItem dataEntry;
        CobolNewDataItem dataEntryMap;
    }

    protected Stack<CobolStructureNewEmitter.data_entry_scope> data_entry_stack = new Stack<CobolStructureNewEmitter.data_entry_scope>();


    // $ANTLR start "data_entry"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:37:1: data_entry[List < CobolDataItem > dataEntries] : ( data_description_entry | rename_description_entry | condition_description_entry );
    public final void data_entry(Map<String, CobolDataItem> dataEntries,
                                 Map<String, CobolNewDataItem> dataEntriesMap) throws RecognitionException {

        data_entry_stack.push(new CobolStructureNewEmitter.data_entry_scope());

        data_entry_stack.peek().dataEntry = new CobolDataItem();
        data_entry_stack.peek().dataEntry.setSrceLine(((CommonTree) input.LT(1)).getLine());

        data_entry_stack.peek().dataEntryMap = new CobolNewDataItem();
      //  data_entry_stack.peek().dataEntryMap.setSrceLine(((CommonTree) input.LT(1)).getLine());


        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:48:5: ( data_description_entry | rename_description_entry | condition_description_entry )
            int alt2 = 3;
            switch (input.LA(1)) {
                case DATA_ITEM: {
                    alt2 = 1;
                }
                break;
                case RENAME: {
                    alt2 = 2;
                }
                break;
                case CONDITION: {
                    alt2 = 3;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 2, 0, input);
                    throw nvae;
            }
            switch (alt2) {
                case 1:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:48:9: data_description_entry
                {
                    pushFollow(FOLLOW_data_description_entry_in_data_entry98);
                    data_description_entry();
                    state._fsp--;

                }
                break;
                case 2:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:49:9: rename_description_entry
                {
                    pushFollow(FOLLOW_rename_description_entry_in_data_entry108);
                    rename_description_entry();
                    state._fsp--;

                }
                break;
                case 3:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:50:9: condition_description_entry
                {
                    pushFollow(FOLLOW_condition_description_entry_in_data_entry118);
                    condition_description_entry();
                    state._fsp--;

                }
                break;

            }


            String cobolName = data_entry_stack.peek().dataEntry.getCobolName();
            String type=data_entry_stack.peek().dataEntry.getCobolUsage();;
            String pic=data_entry_stack.peek().dataEntry.getPicture();

            dataEntries.put(cobolName, data_entry_stack.peek().dataEntry);
            data_entry_stack.peek().dataEntryMap.setName(cobolName);
            data_entry_stack.peek().dataEntryMap.setType(type);
            String sizeSt=getPicSize(pic);
            if (sizeSt!=null) {
                data_entry_stack.peek().dataEntryMap.setSize(Integer.valueOf(sizeSt));

            }
            dataEntriesMap.put(cobolName, data_entry_stack.peek().dataEntryMap);


        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
            data_entry_stack.pop();
        }
    }
    // $ANTLR end "data_entry"


    // $ANTLR start "data_description_entry"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:56:1: data_description_entry : ^( DATA_ITEM data_item_level ( data_item_name )? ( clauses )* ( data_entry[$data_entry::dataEntry.getChildren()] )* ) ;
    public final void data_description_entry() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:57:5: ( ^( DATA_ITEM data_item_level ( data_item_name )? ( clauses )* ( data_entry[$data_entry::dataEntry.getChildren()] )* ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:57:9: ^( DATA_ITEM data_item_level ( data_item_name )? ( clauses )* ( data_entry[$data_entry::dataEntry.getChildren()] )* )
            {
                match(input, DATA_ITEM, FOLLOW_DATA_ITEM_in_data_description_entry140);
                match(input, Token.DOWN, null);
                pushFollow(FOLLOW_data_item_level_in_data_description_entry142);
                data_item_level();
                state._fsp--;

                // com\\legstar\\cobol\\CobolStructureEmitter.g:57:37: ( data_item_name )?
                int alt3 = 2;
                int LA3_0 = input.LA(1);
                if ((LA3_0 == NAME)) {
                    alt3 = 1;
                }
                switch (alt3) {
                    case 1:
                        // com\\legstar\\cobol\\CobolStructureEmitter.g:57:37: data_item_name
                    {
                        pushFollow(FOLLOW_data_item_name_in_data_description_entry144);
                        data_item_name();
                        state._fsp--;

                    }
                    break;

                }

                // com\\legstar\\cobol\\CobolStructureEmitter.g:57:53: ( clauses )*
                loop4:
                while (true) {
                    int alt4 = 2;
                    int LA4_0 = input.LA(1);
                    if ((LA4_0 == BLANKWHENZERO || LA4_0 == DATEFORMAT || (LA4_0 >= EXTERNAL && LA4_0 <= FIXEDARRAY) || (LA4_0 >= GLOBAL && LA4_0 <= GROUPUSAGENATIONAL) || LA4_0 == JUSTIFIEDRIGHT || LA4_0 == PICTURE || LA4_0 == REDEFINES || LA4_0 == SIGN || LA4_0 == SYNCHRONIZED || (LA4_0 >= USAGE && LA4_0 <= VARARRAY))) {
                        alt4 = 1;
                    }

                    switch (alt4) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:57:53: clauses
                        {
                            pushFollow(FOLLOW_clauses_in_data_description_entry147);
                            clauses();
                            state._fsp--;

                        }
                        break;

                        default:
                            break loop4;
                    }
                }

                // com\\legstar\\cobol\\CobolStructureEmitter.g:57:62: ( data_entry[$data_entry::dataEntry.getChildren()] )*
                loop5:
                while (true) {
                    int alt5 = 2;
                    int LA5_0 = input.LA(1);
                    if (((LA5_0 >= CONDITION && LA5_0 <= DATA_ITEM) || LA5_0 == RENAME)) {
                        alt5 = 1;
                    }

                    switch (alt5) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:57:63: data_entry[$data_entry::dataEntry.getChildren()]
                        {
                            pushFollow(FOLLOW_data_entry_in_data_description_entry151);
                            //  LinkedHashMap<String, CobolDataItem> mapCol= makeMap(data_entry_stack.peek().dataEntry.getChildren());
                            CobolDataItem father = data_entry_stack.peek().dataEntry;
                            List<CobolDataItem> list = data_entry_stack.peek().dataEntry.getChildren();
                            //  data_entry(data_entry_stack.peek().dataEntry.getChildren());
                            //data_entry(mapCol);

                            CobolNewDataItem fatherMap = data_entry_stack.peek().dataEntryMap;
                            List<CobolNewDataItem> listMap = data_entry_stack.peek().dataEntryMap.getChildren();

                            childData_entry(list, father, listMap, fatherMap);
                            state._fsp--;

                        }
                        break;

                        default:
                            break loop5;
                    }
                }

                match(input, Token.UP, null);

            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }

    private void childData_entry(List<CobolDataItem> dataEntries, CobolDataItem father,
                                 List<CobolNewDataItem> dataEntriesMap, CobolNewDataItem fatherMap) throws RecognitionException {

        data_entry_stack.push(new CobolStructureNewEmitter.data_entry_scope());
        data_entry_stack.peek().dataEntry = new CobolDataItem();
        data_entry_stack.peek().dataEntryMap = new CobolNewDataItem();


        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:48:5: ( data_description_entry | rename_description_entry | condition_description_entry )
            int alt2 = 3;
            switch (input.LA(1)) {
                case DATA_ITEM: {
                    alt2 = 1;
                }
                break;
                case RENAME: {
                    alt2 = 2;
                }
                break;
                case CONDITION: {
                    alt2 = 3;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 2, 0, input);
                    throw nvae;
            }
            switch (alt2) {
                case 1:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:48:9: data_description_entry
                {
                    pushFollow(FOLLOW_data_description_entry_in_data_entry98);
                    data_description_entry();
                    state._fsp--;

                }
                break;
                case 2:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:49:9: rename_description_entry
                {
                    pushFollow(FOLLOW_rename_description_entry_in_data_entry108);
                    rename_description_entry();
                    state._fsp--;

                }
                break;
                case 3:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:50:9: condition_description_entry
                {
                    pushFollow(FOLLOW_condition_description_entry_in_data_entry118);
                    condition_description_entry();
                    state._fsp--;

                }
                break;

            }


            CobolDataItem old=data_entry_stack.peek().dataEntry;
            String dtatype=old.getCobolUsage();

            father.getChildren().add(data_entry_stack.peek().dataEntry);
            data_entry_stack.peek().dataEntryMap.setName(old.getCobolName());
           // data_entry_stack.peek().dataEntryMap.setPicture(old.getPicture());
            data_entry_stack.peek().dataEntryMap.setType(dtatype);
            String pic=old.getPicture();
            String sizeSt=getPicSize(pic);
            if (sizeSt!=null) {
                data_entry_stack.peek().dataEntryMap.setSize(Integer.valueOf(sizeSt));

            }
            if (dtatype!=null){
                fatherMap.getChildren().add(data_entry_stack.peek().dataEntryMap);
            }


        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
            data_entry_stack.pop();
        }
    }

    private String getPicSize(String pic) {
        try {
            int startIndex =pic.indexOf("(");
            int lastIndex=pic.indexOf(")");
            return  pic.substring(startIndex+1,lastIndex);
        }catch ( Exception e ){
        }
        return null;
    }
    // $ANTLR end "data_entry"

    // $ANTLR end "data_description_entry"

    private LinkedHashMap<String, CobolDataItem> makeMap(List<CobolDataItem> children) {

        LinkedHashMap<String, CobolDataItem> map = new LinkedHashMap<String, CobolDataItem>();
        for (CobolDataItem cbl : children) {
            map.put(cbl.getCobolName(), cbl);

        }
        return map;
    }


    // $ANTLR start "data_item_level"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:60:1: data_item_level : ^( LEVEL DATA_ITEM_LEVEL ) ;
    public final void data_item_level() throws RecognitionException {
        CommonTree DATA_ITEM_LEVEL1 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:61:5: ( ^( LEVEL DATA_ITEM_LEVEL ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:61:9: ^( LEVEL DATA_ITEM_LEVEL )
            {
                match(input, LEVEL, FOLLOW_LEVEL_in_data_item_level179);
                match(input, Token.DOWN, null);
                DATA_ITEM_LEVEL1 = (CommonTree) match(input, DATA_ITEM_LEVEL, FOLLOW_DATA_ITEM_LEVEL_in_data_item_level181);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setLevelNumber(Integer.parseInt((DATA_ITEM_LEVEL1 != null ? DATA_ITEM_LEVEL1.getText() : null)));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "data_item_level"


    // $ANTLR start "data_item_name"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:65:1: data_item_name : ^( NAME DATA_NAME ) ;
    public final void data_item_name() throws RecognitionException {
        CommonTree DATA_NAME2 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:66:5: ( ^( NAME DATA_NAME ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:66:9: ^( NAME DATA_NAME )
            {
                match(input, NAME, FOLLOW_NAME_in_data_item_name212);
                match(input, Token.DOWN, null);
                DATA_NAME2 = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_data_item_name214);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setCobolName((DATA_NAME2 != null ? DATA_NAME2.getText() : null));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "data_item_name"


    // $ANTLR start "rename_description_entry"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:73:1: rename_description_entry : ^( RENAME rename_level data_item_name ( rename_subject_literal | rename_subject_range ) ) ;
    public final void rename_description_entry() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:74:5: ( ^( RENAME rename_level data_item_name ( rename_subject_literal | rename_subject_range ) ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:74:9: ^( RENAME rename_level data_item_name ( rename_subject_literal | rename_subject_range ) )
            {
                match(input, RENAME, FOLLOW_RENAME_in_rename_description_entry249);
                match(input, Token.DOWN, null);
                pushFollow(FOLLOW_rename_level_in_rename_description_entry251);
                rename_level();
                state._fsp--;

                pushFollow(FOLLOW_data_item_name_in_rename_description_entry253);
                data_item_name();
                state._fsp--;

                // com\\legstar\\cobol\\CobolStructureEmitter.g:74:46: ( rename_subject_literal | rename_subject_range )
                int alt6 = 2;
                int LA6_0 = input.LA(1);
                if ((LA6_0 == LITERAL)) {
                    alt6 = 1;
                } else if ((LA6_0 == RANGE)) {
                    alt6 = 2;
                } else {
                    NoViableAltException nvae =
                            new NoViableAltException("", 6, 0, input);
                    throw nvae;
                }

                switch (alt6) {
                    case 1:
                        // com\\legstar\\cobol\\CobolStructureEmitter.g:74:47: rename_subject_literal
                    {
                        pushFollow(FOLLOW_rename_subject_literal_in_rename_description_entry256);
                        rename_subject_literal();
                        state._fsp--;

                    }
                    break;
                    case 2:
                        // com\\legstar\\cobol\\CobolStructureEmitter.g:74:72: rename_subject_range
                    {
                        pushFollow(FOLLOW_rename_subject_range_in_rename_description_entry260);
                        rename_subject_range();
                        state._fsp--;

                    }
                    break;

                }

                match(input, Token.UP, null);

            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "rename_description_entry"


    // $ANTLR start "rename_level"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:77:1: rename_level : ^( LEVEL RENAMES_LEVEL ) ;
    public final void rename_level() throws RecognitionException {
        CommonTree RENAMES_LEVEL3 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:78:5: ( ^( LEVEL RENAMES_LEVEL ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:78:9: ^( LEVEL RENAMES_LEVEL )
            {
                match(input, LEVEL, FOLLOW_LEVEL_in_rename_level287);
                match(input, Token.DOWN, null);
                RENAMES_LEVEL3 = (CommonTree) match(input, RENAMES_LEVEL, FOLLOW_RENAMES_LEVEL_in_rename_level289);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setLevelNumber(Integer.parseInt((RENAMES_LEVEL3 != null ? RENAMES_LEVEL3.getText() : null)));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "rename_level"


    // $ANTLR start "rename_subject_literal"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:82:1: rename_subject_literal : ^( LITERAL DATA_NAME ) ;
    public final void rename_subject_literal() throws RecognitionException {
        CommonTree DATA_NAME4 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:83:5: ( ^( LITERAL DATA_NAME ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:83:9: ^( LITERAL DATA_NAME )
            {
                match(input, LITERAL, FOLLOW_LITERAL_in_rename_subject_literal320);
                match(input, Token.DOWN, null);
                DATA_NAME4 = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_rename_subject_literal322);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setRenamesSubject((DATA_NAME4 != null ? DATA_NAME4.getText() : null));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "rename_subject_literal"


    // $ANTLR start "rename_subject_range"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:87:1: rename_subject_range : ^( RANGE v= DATA_NAME w= DATA_NAME ) ;
    public final void rename_subject_range() throws RecognitionException {
        CommonTree v = null;
        CommonTree w = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:88:5: ( ^( RANGE v= DATA_NAME w= DATA_NAME ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:88:9: ^( RANGE v= DATA_NAME w= DATA_NAME )
            {
                match(input, RANGE, FOLLOW_RANGE_in_rename_subject_range353);
                match(input, Token.DOWN, null);
                v = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_rename_subject_range357);
                w = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_rename_subject_range361);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setRenamesSubjectRange(new Range((v != null ? v.getText() : null), (w != null ? w.getText() : null)));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "rename_subject_range"


    // $ANTLR start "condition_description_entry"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:95:1: condition_description_entry : ^( CONDITION condition_level data_item_name ( condition_subject_literal | condition_subject_range )+ ) ;
    public final void condition_description_entry() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:96:5: ( ^( CONDITION condition_level data_item_name ( condition_subject_literal | condition_subject_range )+ ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:96:9: ^( CONDITION condition_level data_item_name ( condition_subject_literal | condition_subject_range )+ )
            {
                match(input, CONDITION, FOLLOW_CONDITION_in_condition_description_entry394);
                match(input, Token.DOWN, null);
                pushFollow(FOLLOW_condition_level_in_condition_description_entry396);
                condition_level();
                state._fsp--;

                pushFollow(FOLLOW_data_item_name_in_condition_description_entry398);
                data_item_name();
                state._fsp--;

                // com\\legstar\\cobol\\CobolStructureEmitter.g:96:52: ( condition_subject_literal | condition_subject_range )+
                int cnt7 = 0;
                loop7:
                while (true) {
                    int alt7 = 3;
                    int LA7_0 = input.LA(1);
                    if ((LA7_0 == LITERAL)) {
                        alt7 = 1;
                    } else if ((LA7_0 == RANGE)) {
                        alt7 = 2;
                    }

                    switch (alt7) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:96:53: condition_subject_literal
                        {
                            pushFollow(FOLLOW_condition_subject_literal_in_condition_description_entry401);
                            condition_subject_literal();
                            state._fsp--;

                        }
                        break;
                        case 2:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:96:81: condition_subject_range
                        {
                            pushFollow(FOLLOW_condition_subject_range_in_condition_description_entry405);
                            condition_subject_range();
                            state._fsp--;

                        }
                        break;

                        default:
                            if (cnt7 >= 1) break loop7;
                            EarlyExitException eee = new EarlyExitException(7, input);
                            throw eee;
                    }
                    cnt7++;
                }

                match(input, Token.UP, null);

            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "condition_description_entry"


    // $ANTLR start "condition_level"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:99:1: condition_level : ^( LEVEL CONDITION_LEVEL ) ;
    public final void condition_level() throws RecognitionException {
        CommonTree CONDITION_LEVEL5 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:100:5: ( ^( LEVEL CONDITION_LEVEL ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:100:9: ^( LEVEL CONDITION_LEVEL )
            {
                match(input, LEVEL, FOLLOW_LEVEL_in_condition_level429);
                match(input, Token.DOWN, null);
                CONDITION_LEVEL5 = (CommonTree) match(input, CONDITION_LEVEL, FOLLOW_CONDITION_LEVEL_in_condition_level431);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setLevelNumber(Integer.parseInt((CONDITION_LEVEL5 != null ? CONDITION_LEVEL5.getText() : null)));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "condition_level"


    // $ANTLR start "condition_subject_literal"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:104:1: condition_subject_literal : ^( LITERAL literal ) ;
    public final void condition_subject_literal() throws RecognitionException {
        String literal6 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:105:5: ( ^( LITERAL literal ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:105:9: ^( LITERAL literal )
            {
                match(input, LITERAL, FOLLOW_LITERAL_in_condition_subject_literal462);
                match(input, Token.DOWN, null);
                pushFollow(FOLLOW_literal_in_condition_subject_literal464);
                literal6 = literal();
                state._fsp--;

                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.addConditionLiterals(literal6);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "condition_subject_literal"


    // $ANTLR start "condition_subject_range"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:109:1: condition_subject_range : ^( RANGE v= literal w= literal ) ;
    public final void condition_subject_range() throws RecognitionException {
        String v = null;
        String w = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:110:5: ( ^( RANGE v= literal w= literal ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:110:9: ^( RANGE v= literal w= literal )
            {
                match(input, RANGE, FOLLOW_RANGE_in_condition_subject_range495);
                match(input, Token.DOWN, null);
                pushFollow(FOLLOW_literal_in_condition_subject_range499);
                v = literal();
                state._fsp--;

                pushFollow(FOLLOW_literal_in_condition_subject_range503);
                w = literal();
                state._fsp--;

                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.addConditionRange(new Range(v, w));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "condition_subject_range"


    // $ANTLR start "clauses"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:117:1: clauses : ( redefines_clause | blank_when_zero_clause | external_clause | global_clause | group_usage_clause | justified_clause | occurs_clause | picture_clause | sign_clause | synchronized_clause | usage_clause | value_clause | date_format_clause );
    public final void clauses() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:118:5: ( redefines_clause | blank_when_zero_clause | external_clause | global_clause | group_usage_clause | justified_clause | occurs_clause | picture_clause | sign_clause | synchronized_clause | usage_clause | value_clause | date_format_clause )
            int alt8 = 13;
            switch (input.LA(1)) {
                case REDEFINES: {
                    alt8 = 1;
                }
                break;
                case BLANKWHENZERO: {
                    alt8 = 2;
                }
                break;
                case EXTERNAL: {
                    alt8 = 3;
                }
                break;
                case GLOBAL: {
                    alt8 = 4;
                }
                break;
                case GROUPUSAGENATIONAL: {
                    alt8 = 5;
                }
                break;
                case JUSTIFIEDRIGHT: {
                    alt8 = 6;
                }
                break;
                case FIXEDARRAY:
                case VARARRAY: {
                    alt8 = 7;
                }
                break;
                case PICTURE: {
                    alt8 = 8;
                }
                break;
                case SIGN: {
                    alt8 = 9;
                }
                break;
                case SYNCHRONIZED: {
                    alt8 = 10;
                }
                break;
                case USAGE: {
                    alt8 = 11;
                }
                break;
                case VALUE: {
                    alt8 = 12;
                }
                break;
                case DATEFORMAT: {
                    alt8 = 13;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 8, 0, input);
                    throw nvae;
            }
            switch (alt8) {
                case 1:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:118:9: redefines_clause
                {
                    pushFollow(FOLLOW_redefines_clause_in_clauses536);
                    redefines_clause();
                    state._fsp--;

                }
                break;
                case 2:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:119:9: blank_when_zero_clause
                {
                    pushFollow(FOLLOW_blank_when_zero_clause_in_clauses546);
                    blank_when_zero_clause();
                    state._fsp--;

                }
                break;
                case 3:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:120:9: external_clause
                {
                    pushFollow(FOLLOW_external_clause_in_clauses556);
                    external_clause();
                    state._fsp--;

                }
                break;
                case 4:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:121:9: global_clause
                {
                    pushFollow(FOLLOW_global_clause_in_clauses566);
                    global_clause();
                    state._fsp--;

                }
                break;
                case 5:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:122:9: group_usage_clause
                {
                    pushFollow(FOLLOW_group_usage_clause_in_clauses576);
                    group_usage_clause();
                    state._fsp--;

                }
                break;
                case 6:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:123:9: justified_clause
                {
                    pushFollow(FOLLOW_justified_clause_in_clauses586);
                    justified_clause();
                    state._fsp--;

                }
                break;
                case 7:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:124:9: occurs_clause
                {
                    pushFollow(FOLLOW_occurs_clause_in_clauses596);
                    occurs_clause();
                    state._fsp--;

                }
                break;
                case 8:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:125:9: picture_clause
                {
                    pushFollow(FOLLOW_picture_clause_in_clauses606);
                    picture_clause();
                    state._fsp--;

                }
                break;
                case 9:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:126:9: sign_clause
                {
                    pushFollow(FOLLOW_sign_clause_in_clauses616);
                    sign_clause();
                    state._fsp--;

                }
                break;
                case 10:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:127:9: synchronized_clause
                {
                    pushFollow(FOLLOW_synchronized_clause_in_clauses626);
                    synchronized_clause();
                    state._fsp--;

                }
                break;
                case 11:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:128:9: usage_clause
                {
                    pushFollow(FOLLOW_usage_clause_in_clauses636);
                    usage_clause();
                    state._fsp--;

                }
                break;
                case 12:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:129:9: value_clause
                {
                    pushFollow(FOLLOW_value_clause_in_clauses646);
                    value_clause();
                    state._fsp--;

                }
                break;
                case 13:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:130:9: date_format_clause
                {
                    pushFollow(FOLLOW_date_format_clause_in_clauses656);
                    date_format_clause();
                    state._fsp--;

                }
                break;

            }
        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "clauses"


    // $ANTLR start "redefines_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:133:1: redefines_clause : ^( REDEFINES DATA_NAME ) ;
    public final void redefines_clause() throws RecognitionException {
        CommonTree DATA_NAME7 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:134:5: ( ^( REDEFINES DATA_NAME ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:134:9: ^( REDEFINES DATA_NAME )
            {
                match(input, REDEFINES, FOLLOW_REDEFINES_in_redefines_clause677);
                match(input, Token.DOWN, null);
                DATA_NAME7 = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_redefines_clause679);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setRedefines((DATA_NAME7 != null ? DATA_NAME7.getText() : null));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "redefines_clause"


    // $ANTLR start "blank_when_zero_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:138:1: blank_when_zero_clause : BLANKWHENZERO ;
    public final void blank_when_zero_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:139:5: ( BLANKWHENZERO )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:139:9: BLANKWHENZERO
            {
                match(input, BLANKWHENZERO, FOLLOW_BLANKWHENZERO_in_blank_when_zero_clause709);
                data_entry_stack.peek().dataEntry.setBlankWhenZero(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "blank_when_zero_clause"


    // $ANTLR start "external_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:143:1: external_clause : EXTERNAL ;
    public final void external_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:144:5: ( EXTERNAL )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:144:9: EXTERNAL
            {
                match(input, EXTERNAL, FOLLOW_EXTERNAL_in_external_clause738);
                data_entry_stack.peek().dataEntry.setExternal(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "external_clause"


    // $ANTLR start "global_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:148:1: global_clause : GLOBAL ;
    public final void global_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:149:5: ( GLOBAL )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:149:9: GLOBAL
            {
                match(input, GLOBAL, FOLLOW_GLOBAL_in_global_clause767);
                data_entry_stack.peek().dataEntry.setGlobal(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "global_clause"


    // $ANTLR start "group_usage_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:153:1: group_usage_clause : GROUPUSAGENATIONAL ;
    public final void group_usage_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:154:5: ( GROUPUSAGENATIONAL )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:154:9: GROUPUSAGENATIONAL
            {
                match(input, GROUPUSAGENATIONAL, FOLLOW_GROUPUSAGENATIONAL_in_group_usage_clause796);
                data_entry_stack.peek().dataEntry.setGroupUsageNational(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "group_usage_clause"


    // $ANTLR start "justified_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:158:1: justified_clause : JUSTIFIEDRIGHT ;
    public final void justified_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:159:5: ( JUSTIFIEDRIGHT )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:159:9: JUSTIFIEDRIGHT
            {
                match(input, JUSTIFIEDRIGHT, FOLLOW_JUSTIFIEDRIGHT_in_justified_clause825);
                data_entry_stack.peek().dataEntry.setJustifiedRight(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "justified_clause"


    // $ANTLR start "occurs_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:163:1: occurs_clause : ( fixed_length_table | variable_length_table );
    public final void occurs_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:164:5: ( fixed_length_table | variable_length_table )
            int alt9 = 2;
            int LA9_0 = input.LA(1);
            if ((LA9_0 == FIXEDARRAY)) {
                alt9 = 1;
            } else if ((LA9_0 == VARARRAY)) {
                alt9 = 2;
            } else {
                NoViableAltException nvae =
                        new NoViableAltException("", 9, 0, input);
                throw nvae;
            }

            switch (alt9) {
                case 1:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:164:9: fixed_length_table
                {
                    pushFollow(FOLLOW_fixed_length_table_in_occurs_clause854);
                    fixed_length_table();
                    state._fsp--;

                }
                break;
                case 2:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:165:9: variable_length_table
                {
                    pushFollow(FOLLOW_variable_length_table_in_occurs_clause864);
                    variable_length_table();
                    state._fsp--;

                }
                break;

            }
        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "occurs_clause"


    // $ANTLR start "picture_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:168:1: picture_clause : ^( PICTURE PICTURESTRING ) ;
    public final void picture_clause() throws RecognitionException {
        CommonTree PICTURESTRING8 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:169:5: ( ^( PICTURE PICTURESTRING ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:169:9: ^( PICTURE PICTURESTRING )
            {
                match(input, PICTURE, FOLLOW_PICTURE_in_picture_clause884);
                match(input, Token.DOWN, null);
                PICTURESTRING8 = (CommonTree) match(input, PICTURESTRING, FOLLOW_PICTURESTRING_in_picture_clause886);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setPicture((PICTURESTRING8 != null ? PICTURESTRING8.getText() : null));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "picture_clause"


    // $ANTLR start "sign_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:173:1: sign_clause : ^( SIGN ( sign_leading_clause )? ( sign_trailing_clause )? ) ;
    public final void sign_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:174:5: ( ^( SIGN ( sign_leading_clause )? ( sign_trailing_clause )? ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:174:9: ^( SIGN ( sign_leading_clause )? ( sign_trailing_clause )? )
            {
                match(input, SIGN, FOLLOW_SIGN_in_sign_clause917);
                if (input.LA(1) == Token.DOWN) {
                    match(input, Token.DOWN, null);
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:174:16: ( sign_leading_clause )?
                    int alt10 = 2;
                    int LA10_0 = input.LA(1);
                    if ((LA10_0 == LEADING)) {
                        alt10 = 1;
                    }
                    switch (alt10) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:174:16: sign_leading_clause
                        {
                            pushFollow(FOLLOW_sign_leading_clause_in_sign_clause919);
                            sign_leading_clause();
                            state._fsp--;

                        }
                        break;

                    }

                    // com\\legstar\\cobol\\CobolStructureEmitter.g:174:37: ( sign_trailing_clause )?
                    int alt11 = 2;
                    int LA11_0 = input.LA(1);
                    if ((LA11_0 == TRAILING)) {
                        alt11 = 1;
                    }
                    switch (alt11) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:174:37: sign_trailing_clause
                        {
                            pushFollow(FOLLOW_sign_trailing_clause_in_sign_clause922);
                            sign_trailing_clause();
                            state._fsp--;

                        }
                        break;

                    }

                    match(input, Token.UP, null);
                }

                data_entry_stack.peek().dataEntry.setSign(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "sign_clause"


    // $ANTLR start "sign_leading_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:178:1: sign_leading_clause : ^( LEADING ( separate_clause )? ) ;
    public final void sign_leading_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:179:5: ( ^( LEADING ( separate_clause )? ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:179:9: ^( LEADING ( separate_clause )? )
            {
                match(input, LEADING, FOLLOW_LEADING_in_sign_leading_clause958);
                if (input.LA(1) == Token.DOWN) {
                    match(input, Token.DOWN, null);
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:179:19: ( separate_clause )?
                    int alt12 = 2;
                    int LA12_0 = input.LA(1);
                    if ((LA12_0 == SEPARATE)) {
                        alt12 = 1;
                    }
                    switch (alt12) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:179:19: separate_clause
                        {
                            pushFollow(FOLLOW_separate_clause_in_sign_leading_clause960);
                            separate_clause();
                            state._fsp--;

                        }
                        break;

                    }

                    match(input, Token.UP, null);
                }

                data_entry_stack.peek().dataEntry.setSignLeading(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "sign_leading_clause"


    // $ANTLR start "sign_trailing_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:183:1: sign_trailing_clause : ^( TRAILING ( separate_clause )? ) ;
    public final void sign_trailing_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:184:5: ( ^( TRAILING ( separate_clause )? ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:184:9: ^( TRAILING ( separate_clause )? )
            {
                match(input, TRAILING, FOLLOW_TRAILING_in_sign_trailing_clause996);
                if (input.LA(1) == Token.DOWN) {
                    match(input, Token.DOWN, null);
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:184:20: ( separate_clause )?
                    int alt13 = 2;
                    int LA13_0 = input.LA(1);
                    if ((LA13_0 == SEPARATE)) {
                        alt13 = 1;
                    }
                    switch (alt13) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:184:20: separate_clause
                        {
                            pushFollow(FOLLOW_separate_clause_in_sign_trailing_clause998);
                            separate_clause();
                            state._fsp--;

                        }
                        break;

                    }

                    match(input, Token.UP, null);
                }

                data_entry_stack.peek().dataEntry.setSignLeading(false);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "sign_trailing_clause"


    // $ANTLR start "separate_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:188:1: separate_clause : SEPARATE ;
    public final void separate_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:189:5: ( SEPARATE )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:189:9: SEPARATE
            {
                match(input, SEPARATE, FOLLOW_SEPARATE_in_separate_clause1033);
                data_entry_stack.peek().dataEntry.setSignSeparate(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "separate_clause"


    // $ANTLR start "synchronized_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:193:1: synchronized_clause : ^( SYNCHRONIZED ( LEFT | RIGHT )? ) ;
    public final void synchronized_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:194:5: ( ^( SYNCHRONIZED ( LEFT | RIGHT )? ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:194:9: ^( SYNCHRONIZED ( LEFT | RIGHT )? )
            {
                match(input, SYNCHRONIZED, FOLLOW_SYNCHRONIZED_in_synchronized_clause1063);
                if (input.LA(1) == Token.DOWN) {
                    match(input, Token.DOWN, null);
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:194:24: ( LEFT | RIGHT )?
                    int alt14 = 2;
                    int LA14_0 = input.LA(1);
                    if ((LA14_0 == LEFT || LA14_0 == RIGHT)) {
                        alt14 = 1;
                    }
                    switch (alt14) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:
                        {
                            if (input.LA(1) == LEFT || input.LA(1) == RIGHT) {
                                input.consume();
                                state.errorRecovery = false;
                            } else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                throw mse;
                            }
                        }
                        break;

                    }

                    match(input, Token.UP, null);
                }

                data_entry_stack.peek().dataEntry.setSynchronized(true);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "synchronized_clause"


    // $ANTLR start "usage_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:198:1: usage_clause : ( ^( USAGE BINARY ) | ^( USAGE SINGLEFLOAT ) | ^( USAGE DOUBLEFLOAT ) | ^( USAGE PACKEDDECIMAL ) | ^( USAGE NATIVEBINARY ) | ^( USAGE DISPLAY ) | ^( USAGE DISPLAY1 ) | ^( USAGE INDEX ) | ^( USAGE NATIONAL ) | ^( USAGE POINTER ) | ^( USAGE PROCEDUREPOINTER ) | ^( USAGE FUNCTIONPOINTER ) );
    public final void usage_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:199:5: ( ^( USAGE BINARY ) | ^( USAGE SINGLEFLOAT ) | ^( USAGE DOUBLEFLOAT ) | ^( USAGE PACKEDDECIMAL ) | ^( USAGE NATIVEBINARY ) | ^( USAGE DISPLAY ) | ^( USAGE DISPLAY1 ) | ^( USAGE INDEX ) | ^( USAGE NATIONAL ) | ^( USAGE POINTER ) | ^( USAGE PROCEDUREPOINTER ) | ^( USAGE FUNCTIONPOINTER ) )
            int alt15 = 12;
            int LA15_0 = input.LA(1);
            if ((LA15_0 == USAGE)) {
                int LA15_1 = input.LA(2);
                if ((LA15_1 == DOWN)) {
                    switch (input.LA(3)) {
                        case BINARY: {
                            alt15 = 1;
                        }
                        break;
                        case SINGLEFLOAT: {
                            alt15 = 2;
                        }
                        break;
                        case DOUBLEFLOAT: {
                            alt15 = 3;
                        }
                        break;
                        case PACKEDDECIMAL: {
                            alt15 = 4;
                        }
                        break;
                        case NATIVEBINARY: {
                            alt15 = 5;
                        }
                        break;
                        case DISPLAY: {
                            alt15 = 6;
                        }
                        break;
                        case DISPLAY1: {
                            alt15 = 7;
                        }
                        break;
                        case INDEX: {
                            alt15 = 8;
                        }
                        break;
                        case NATIONAL: {
                            alt15 = 9;
                        }
                        break;
                        case POINTER: {
                            alt15 = 10;
                        }
                        break;
                        case PROCEDUREPOINTER: {
                            alt15 = 11;
                        }
                        break;
                        case FUNCTIONPOINTER: {
                            alt15 = 12;
                        }
                        break;
                        default:
                            int nvaeMark = input.mark();
                            try {
                                for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
                                    input.consume();
                                }
                                NoViableAltException nvae =
                                        new NoViableAltException("", 15, 2, input);
                                throw nvae;
                            } finally {
                                input.rewind(nvaeMark);
                            }
                    }
                } else {
                    int nvaeMark = input.mark();
                    try {
                        input.consume();
                        NoViableAltException nvae =
                                new NoViableAltException("", 15, 1, input);
                        throw nvae;
                    } finally {
                        input.rewind(nvaeMark);
                    }
                }

            } else {
                NoViableAltException nvae =
                        new NoViableAltException("", 15, 0, input);
                throw nvae;
            }

            switch (alt15) {
                case 1:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:199:9: ^( USAGE BINARY )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1103);
                    match(input, Token.DOWN, null);
                    match(input, BINARY, FOLLOW_BINARY_in_usage_clause1105);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.BINARY);
                }
                break;
                case 2:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:201:9: ^( USAGE SINGLEFLOAT )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1127);
                    match(input, Token.DOWN, null);
                    match(input, SINGLEFLOAT, FOLLOW_SINGLEFLOAT_in_usage_clause1129);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.SINGLEFLOAT);
                }
                break;
                case 3:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:203:9: ^( USAGE DOUBLEFLOAT )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1151);
                    match(input, Token.DOWN, null);
                    match(input, DOUBLEFLOAT, FOLLOW_DOUBLEFLOAT_in_usage_clause1153);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.DOUBLEFLOAT);
                }
                break;
                case 4:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:205:9: ^( USAGE PACKEDDECIMAL )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1175);
                    match(input, Token.DOWN, null);
                    match(input, PACKEDDECIMAL, FOLLOW_PACKEDDECIMAL_in_usage_clause1177);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.PACKEDDECIMAL);
                }
                break;
                case 5:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:207:9: ^( USAGE NATIVEBINARY )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1199);
                    match(input, Token.DOWN, null);
                    match(input, NATIVEBINARY, FOLLOW_NATIVEBINARY_in_usage_clause1201);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.NATIVEBINARY);
                }
                break;
                case 6:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:209:9: ^( USAGE DISPLAY )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1223);
                    match(input, Token.DOWN, null);
                    match(input, DISPLAY, FOLLOW_DISPLAY_in_usage_clause1225);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.DISPLAY);
                }
                break;
                case 7:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:211:9: ^( USAGE DISPLAY1 )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1247);
                    match(input, Token.DOWN, null);
                    match(input, DISPLAY1, FOLLOW_DISPLAY1_in_usage_clause1249);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.DISPLAY1);
                }
                break;
                case 8:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:213:9: ^( USAGE INDEX )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1271);
                    match(input, Token.DOWN, null);
                    match(input, INDEX, FOLLOW_INDEX_in_usage_clause1273);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.INDEX);
                }
                break;
                case 9:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:215:9: ^( USAGE NATIONAL )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1295);
                    match(input, Token.DOWN, null);
                    match(input, NATIONAL, FOLLOW_NATIONAL_in_usage_clause1297);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.NATIONAL);
                }
                break;
                case 10:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:217:9: ^( USAGE POINTER )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1319);
                    match(input, Token.DOWN, null);
                    match(input, POINTER, FOLLOW_POINTER_in_usage_clause1321);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.POINTER);
                }
                break;
                case 11:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:219:9: ^( USAGE PROCEDUREPOINTER )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1343);
                    match(input, Token.DOWN, null);
                    match(input, PROCEDUREPOINTER, FOLLOW_PROCEDUREPOINTER_in_usage_clause1345);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.PROCEDUREPOINTER);
                }
                break;
                case 12:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:221:9: ^( USAGE FUNCTIONPOINTER )
                {
                    match(input, USAGE, FOLLOW_USAGE_in_usage_clause1367);
                    match(input, Token.DOWN, null);
                    match(input, FUNCTIONPOINTER, FOLLOW_FUNCTIONPOINTER_in_usage_clause1369);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.setUsage(Usage.FUNCTIONPOINTER);
                }
                break;

            }
        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "usage_clause"


    // $ANTLR start "value_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:225:1: value_clause : ^( VALUE value_clause_literal ) ;
    public final void value_clause() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:226:5: ( ^( VALUE value_clause_literal ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:226:9: ^( VALUE value_clause_literal )
            {
                match(input, VALUE, FOLLOW_VALUE_in_value_clause1400);
                match(input, Token.DOWN, null);
                pushFollow(FOLLOW_value_clause_literal_in_value_clause1402);
                value_clause_literal();
                state._fsp--;

                match(input, Token.UP, null);

            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "value_clause"


    // $ANTLR start "value_clause_literal"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:229:1: value_clause_literal : literal ;
    public final void value_clause_literal() throws RecognitionException {
        String literal9 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:230:5: ( literal )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:230:9: literal
            {
                pushFollow(FOLLOW_literal_in_value_clause_literal1426);
                literal9 = literal();
                state._fsp--;

                data_entry_stack.peek().dataEntry.setValue(literal9);
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "value_clause_literal"


    // $ANTLR start "literal"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:234:1: literal returns [String value] : ( FLOAT_LITERAL | DECIMAL_LITERAL | INT | SIGNED_INT | ALPHANUM_LITERAL_STRING | HEX_LITERAL_STRING | ZERO_LITERAL_STRING | DBCS_LITERAL_STRING | NATIONAL_LITERAL_STRING | NATIONAL_HEX_LITERAL_STRING | ZERO_CONSTANT | SPACE_CONSTANT | HIGH_VALUE_CONSTANT | LOW_VALUE_CONSTANT | QUOTE_CONSTANT | ALL_CONSTANT (v= ALPHANUM_LITERAL_STRING |v= ZERO_CONSTANT |v= SPACE_CONSTANT |v= HIGH_VALUE_CONSTANT |v= LOW_VALUE_CONSTANT |v= QUOTE_CONSTANT |v= NULL_CONSTANT ) | NULL_CONSTANT );
    public final String literal() throws RecognitionException {
        String value = null;


        CommonTree v = null;
        CommonTree FLOAT_LITERAL10 = null;
        CommonTree DECIMAL_LITERAL11 = null;
        CommonTree INT12 = null;
        CommonTree SIGNED_INT13 = null;
        CommonTree ALPHANUM_LITERAL_STRING14 = null;
        CommonTree HEX_LITERAL_STRING15 = null;
        CommonTree ZERO_LITERAL_STRING16 = null;
        CommonTree DBCS_LITERAL_STRING17 = null;
        CommonTree NATIONAL_LITERAL_STRING18 = null;
        CommonTree NATIONAL_HEX_LITERAL_STRING19 = null;
        CommonTree ZERO_CONSTANT20 = null;
        CommonTree SPACE_CONSTANT21 = null;
        CommonTree HIGH_VALUE_CONSTANT22 = null;
        CommonTree LOW_VALUE_CONSTANT23 = null;
        CommonTree QUOTE_CONSTANT24 = null;
        CommonTree ALL_CONSTANT25 = null;
        CommonTree NULL_CONSTANT26 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:235:5: ( FLOAT_LITERAL | DECIMAL_LITERAL | INT | SIGNED_INT | ALPHANUM_LITERAL_STRING | HEX_LITERAL_STRING | ZERO_LITERAL_STRING | DBCS_LITERAL_STRING | NATIONAL_LITERAL_STRING | NATIONAL_HEX_LITERAL_STRING | ZERO_CONSTANT | SPACE_CONSTANT | HIGH_VALUE_CONSTANT | LOW_VALUE_CONSTANT | QUOTE_CONSTANT | ALL_CONSTANT (v= ALPHANUM_LITERAL_STRING |v= ZERO_CONSTANT |v= SPACE_CONSTANT |v= HIGH_VALUE_CONSTANT |v= LOW_VALUE_CONSTANT |v= QUOTE_CONSTANT |v= NULL_CONSTANT ) | NULL_CONSTANT )
            int alt17 = 17;
            switch (input.LA(1)) {
                case FLOAT_LITERAL: {
                    alt17 = 1;
                }
                break;
                case DECIMAL_LITERAL: {
                    alt17 = 2;
                }
                break;
                case INT: {
                    alt17 = 3;
                }
                break;
                case SIGNED_INT: {
                    alt17 = 4;
                }
                break;
                case ALPHANUM_LITERAL_STRING: {
                    alt17 = 5;
                }
                break;
                case HEX_LITERAL_STRING: {
                    alt17 = 6;
                }
                break;
                case ZERO_LITERAL_STRING: {
                    alt17 = 7;
                }
                break;
                case DBCS_LITERAL_STRING: {
                    alt17 = 8;
                }
                break;
                case NATIONAL_LITERAL_STRING: {
                    alt17 = 9;
                }
                break;
                case NATIONAL_HEX_LITERAL_STRING: {
                    alt17 = 10;
                }
                break;
                case ZERO_CONSTANT: {
                    alt17 = 11;
                }
                break;
                case SPACE_CONSTANT: {
                    alt17 = 12;
                }
                break;
                case HIGH_VALUE_CONSTANT: {
                    alt17 = 13;
                }
                break;
                case LOW_VALUE_CONSTANT: {
                    alt17 = 14;
                }
                break;
                case QUOTE_CONSTANT: {
                    alt17 = 15;
                }
                break;
                case ALL_CONSTANT: {
                    alt17 = 16;
                }
                break;
                case NULL_CONSTANT: {
                    alt17 = 17;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 17, 0, input);
                    throw nvae;
            }
            switch (alt17) {
                case 1:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:235:9: FLOAT_LITERAL
                {
                    FLOAT_LITERAL10 = (CommonTree) match(input, FLOAT_LITERAL, FOLLOW_FLOAT_LITERAL_in_literal1463);
                    value = (FLOAT_LITERAL10 != null ? FLOAT_LITERAL10.getText() : null);
                }
                break;
                case 2:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:237:9: DECIMAL_LITERAL
                {
                    DECIMAL_LITERAL11 = (CommonTree) match(input, DECIMAL_LITERAL, FOLLOW_DECIMAL_LITERAL_in_literal1483);
                    value = (DECIMAL_LITERAL11 != null ? DECIMAL_LITERAL11.getText() : null);
                }
                break;
                case 3:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:239:9: INT
                {
                    INT12 = (CommonTree) match(input, INT, FOLLOW_INT_in_literal1503);
                    value = (INT12 != null ? INT12.getText() : null);
                }
                break;
                case 4:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:241:9: SIGNED_INT
                {
                    SIGNED_INT13 = (CommonTree) match(input, SIGNED_INT, FOLLOW_SIGNED_INT_in_literal1523);
                    value = (SIGNED_INT13 != null ? SIGNED_INT13.getText() : null);
                }
                break;
                case 5:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:243:9: ALPHANUM_LITERAL_STRING
                {
                    ALPHANUM_LITERAL_STRING14 = (CommonTree) match(input, ALPHANUM_LITERAL_STRING, FOLLOW_ALPHANUM_LITERAL_STRING_in_literal1543);
                    value = (ALPHANUM_LITERAL_STRING14 != null ? ALPHANUM_LITERAL_STRING14.getText() : null);
                }
                break;
                case 6:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:245:9: HEX_LITERAL_STRING
                {
                    HEX_LITERAL_STRING15 = (CommonTree) match(input, HEX_LITERAL_STRING, FOLLOW_HEX_LITERAL_STRING_in_literal1563);
                    value = (HEX_LITERAL_STRING15 != null ? HEX_LITERAL_STRING15.getText() : null);
                }
                break;
                case 7:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:247:9: ZERO_LITERAL_STRING
                {
                    ZERO_LITERAL_STRING16 = (CommonTree) match(input, ZERO_LITERAL_STRING, FOLLOW_ZERO_LITERAL_STRING_in_literal1583);
                    value = (ZERO_LITERAL_STRING16 != null ? ZERO_LITERAL_STRING16.getText() : null);
                }
                break;
                case 8:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:249:9: DBCS_LITERAL_STRING
                {
                    DBCS_LITERAL_STRING17 = (CommonTree) match(input, DBCS_LITERAL_STRING, FOLLOW_DBCS_LITERAL_STRING_in_literal1603);
                    value = (DBCS_LITERAL_STRING17 != null ? DBCS_LITERAL_STRING17.getText() : null);
                }
                break;
                case 9:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:251:9: NATIONAL_LITERAL_STRING
                {
                    NATIONAL_LITERAL_STRING18 = (CommonTree) match(input, NATIONAL_LITERAL_STRING, FOLLOW_NATIONAL_LITERAL_STRING_in_literal1623);
                    value = (NATIONAL_LITERAL_STRING18 != null ? NATIONAL_LITERAL_STRING18.getText() : null);
                }
                break;
                case 10:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:253:9: NATIONAL_HEX_LITERAL_STRING
                {
                    NATIONAL_HEX_LITERAL_STRING19 = (CommonTree) match(input, NATIONAL_HEX_LITERAL_STRING, FOLLOW_NATIONAL_HEX_LITERAL_STRING_in_literal1643);
                    value = (NATIONAL_HEX_LITERAL_STRING19 != null ? NATIONAL_HEX_LITERAL_STRING19.getText() : null);
                }
                break;
                case 11:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:255:9: ZERO_CONSTANT
                {
                    ZERO_CONSTANT20 = (CommonTree) match(input, ZERO_CONSTANT, FOLLOW_ZERO_CONSTANT_in_literal1663);
                    value = (ZERO_CONSTANT20 != null ? ZERO_CONSTANT20.getText() : null);
                }
                break;
                case 12:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:257:9: SPACE_CONSTANT
                {
                    SPACE_CONSTANT21 = (CommonTree) match(input, SPACE_CONSTANT, FOLLOW_SPACE_CONSTANT_in_literal1683);
                    value = (SPACE_CONSTANT21 != null ? SPACE_CONSTANT21.getText() : null);
                }
                break;
                case 13:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:259:9: HIGH_VALUE_CONSTANT
                {
                    HIGH_VALUE_CONSTANT22 = (CommonTree) match(input, HIGH_VALUE_CONSTANT, FOLLOW_HIGH_VALUE_CONSTANT_in_literal1703);
                    value = (HIGH_VALUE_CONSTANT22 != null ? HIGH_VALUE_CONSTANT22.getText() : null);
                }
                break;
                case 14:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:261:9: LOW_VALUE_CONSTANT
                {
                    LOW_VALUE_CONSTANT23 = (CommonTree) match(input, LOW_VALUE_CONSTANT, FOLLOW_LOW_VALUE_CONSTANT_in_literal1723);
                    value = (LOW_VALUE_CONSTANT23 != null ? LOW_VALUE_CONSTANT23.getText() : null);
                }
                break;
                case 15:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:263:9: QUOTE_CONSTANT
                {
                    QUOTE_CONSTANT24 = (CommonTree) match(input, QUOTE_CONSTANT, FOLLOW_QUOTE_CONSTANT_in_literal1743);
                    value = (QUOTE_CONSTANT24 != null ? QUOTE_CONSTANT24.getText() : null);
                }
                break;
                case 16:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:265:9: ALL_CONSTANT (v= ALPHANUM_LITERAL_STRING |v= ZERO_CONSTANT |v= SPACE_CONSTANT |v= HIGH_VALUE_CONSTANT |v= LOW_VALUE_CONSTANT |v= QUOTE_CONSTANT |v= NULL_CONSTANT )
                {
                    ALL_CONSTANT25 = (CommonTree) match(input, ALL_CONSTANT, FOLLOW_ALL_CONSTANT_in_literal1763);
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:265:22: (v= ALPHANUM_LITERAL_STRING |v= ZERO_CONSTANT |v= SPACE_CONSTANT |v= HIGH_VALUE_CONSTANT |v= LOW_VALUE_CONSTANT |v= QUOTE_CONSTANT |v= NULL_CONSTANT )
                    int alt16 = 7;
                    switch (input.LA(1)) {
                        case ALPHANUM_LITERAL_STRING: {
                            alt16 = 1;
                        }
                        break;
                        case ZERO_CONSTANT: {
                            alt16 = 2;
                        }
                        break;
                        case SPACE_CONSTANT: {
                            alt16 = 3;
                        }
                        break;
                        case HIGH_VALUE_CONSTANT: {
                            alt16 = 4;
                        }
                        break;
                        case LOW_VALUE_CONSTANT: {
                            alt16 = 5;
                        }
                        break;
                        case QUOTE_CONSTANT: {
                            alt16 = 6;
                        }
                        break;
                        case NULL_CONSTANT: {
                            alt16 = 7;
                        }
                        break;
                        default:
                            NoViableAltException nvae =
                                    new NoViableAltException("", 16, 0, input);
                            throw nvae;
                    }
                    switch (alt16) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:265:23: v= ALPHANUM_LITERAL_STRING
                        {
                            v = (CommonTree) match(input, ALPHANUM_LITERAL_STRING, FOLLOW_ALPHANUM_LITERAL_STRING_in_literal1768);
                        }
                        break;
                        case 2:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:265:51: v= ZERO_CONSTANT
                        {
                            v = (CommonTree) match(input, ZERO_CONSTANT, FOLLOW_ZERO_CONSTANT_in_literal1774);
                        }
                        break;
                        case 3:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:265:69: v= SPACE_CONSTANT
                        {
                            v = (CommonTree) match(input, SPACE_CONSTANT, FOLLOW_SPACE_CONSTANT_in_literal1780);
                        }
                        break;
                        case 4:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:265:88: v= HIGH_VALUE_CONSTANT
                        {
                            v = (CommonTree) match(input, HIGH_VALUE_CONSTANT, FOLLOW_HIGH_VALUE_CONSTANT_in_literal1786);
                        }
                        break;
                        case 5:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:265:112: v= LOW_VALUE_CONSTANT
                        {
                            v = (CommonTree) match(input, LOW_VALUE_CONSTANT, FOLLOW_LOW_VALUE_CONSTANT_in_literal1792);
                        }
                        break;
                        case 6:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:265:135: v= QUOTE_CONSTANT
                        {
                            v = (CommonTree) match(input, QUOTE_CONSTANT, FOLLOW_QUOTE_CONSTANT_in_literal1798);
                        }
                        break;
                        case 7:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:265:154: v= NULL_CONSTANT
                        {
                            v = (CommonTree) match(input, NULL_CONSTANT, FOLLOW_NULL_CONSTANT_in_literal1804);
                        }
                        break;

                    }

                    value = (ALL_CONSTANT25 != null ? ALL_CONSTANT25.getText() : null) + ' ' + (v != null ? v.getText() : null);
                }
                break;
                case 17:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:267:9: NULL_CONSTANT
                {
                    NULL_CONSTANT26 = (CommonTree) match(input, NULL_CONSTANT, FOLLOW_NULL_CONSTANT_in_literal1825);
                    value = (NULL_CONSTANT26 != null ? NULL_CONSTANT26.getText() : null);
                }
                break;

            }
        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
        return value;
    }
    // $ANTLR end "literal"


    // $ANTLR start "date_format_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:271:1: date_format_clause : ^( DATEFORMAT DATE_PATTERN ) ;
    public final void date_format_clause() throws RecognitionException {
        CommonTree DATE_PATTERN27 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:272:5: ( ^( DATEFORMAT DATE_PATTERN ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:272:9: ^( DATEFORMAT DATE_PATTERN )
            {
                match(input, DATEFORMAT, FOLLOW_DATEFORMAT_in_date_format_clause1855);
                match(input, Token.DOWN, null);
                DATE_PATTERN27 = (CommonTree) match(input, DATE_PATTERN, FOLLOW_DATE_PATTERN_in_date_format_clause1857);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setDateFormat((DATE_PATTERN27 != null ? DATE_PATTERN27.getText() : null));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "date_format_clause"


    // $ANTLR start "fixed_length_table"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:279:1: fixed_length_table : ^( FIXEDARRAY high_bound ( key_clause )* ( index_clause )* ) ;
    public final void fixed_length_table() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:280:5: ( ^( FIXEDARRAY high_bound ( key_clause )* ( index_clause )* ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:280:9: ^( FIXEDARRAY high_bound ( key_clause )* ( index_clause )* )
            {
                match(input, FIXEDARRAY, FOLLOW_FIXEDARRAY_in_fixed_length_table1892);
                match(input, Token.DOWN, null);
                pushFollow(FOLLOW_high_bound_in_fixed_length_table1894);
                high_bound();
                state._fsp--;

                // com\\legstar\\cobol\\CobolStructureEmitter.g:280:33: ( key_clause )*
                loop18:
                while (true) {
                    int alt18 = 2;
                    int LA18_0 = input.LA(1);
                    if ((LA18_0 == KEY)) {
                        alt18 = 1;
                    }

                    switch (alt18) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:280:33: key_clause
                        {
                            pushFollow(FOLLOW_key_clause_in_fixed_length_table1896);
                            key_clause();
                            state._fsp--;

                        }
                        break;

                        default:
                            break loop18;
                    }
                }

                // com\\legstar\\cobol\\CobolStructureEmitter.g:280:45: ( index_clause )*
                loop19:
                while (true) {
                    int alt19 = 2;
                    int LA19_0 = input.LA(1);
                    if ((LA19_0 == INDEX)) {
                        alt19 = 1;
                    }

                    switch (alt19) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:280:45: index_clause
                        {
                            pushFollow(FOLLOW_index_clause_in_fixed_length_table1899);
                            index_clause();
                            state._fsp--;

                        }
                        break;

                        default:
                            break loop19;
                    }
                }

                match(input, Token.UP, null);

            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "fixed_length_table"


    // $ANTLR start "variable_length_table"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:283:1: variable_length_table : ^( VARARRAY ( low_bound )? high_bound ( key_clause )* ( index_clause )* ) ;
    public final void variable_length_table() throws RecognitionException {
        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:284:5: ( ^( VARARRAY ( low_bound )? high_bound ( key_clause )* ( index_clause )* ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:284:9: ^( VARARRAY ( low_bound )? high_bound ( key_clause )* ( index_clause )* )
            {
                match(input, VARARRAY, FOLLOW_VARARRAY_in_variable_length_table1936);
                match(input, Token.DOWN, null);
                // com\\legstar\\cobol\\CobolStructureEmitter.g:284:20: ( low_bound )?
                int alt20 = 2;
                int LA20_0 = input.LA(1);
                if ((LA20_0 == LBOUND)) {
                    alt20 = 1;
                }
                switch (alt20) {
                    case 1:
                        // com\\legstar\\cobol\\CobolStructureEmitter.g:284:20: low_bound
                    {
                        pushFollow(FOLLOW_low_bound_in_variable_length_table1938);
                        low_bound();
                        state._fsp--;

                    }
                    break;

                }

                pushFollow(FOLLOW_high_bound_in_variable_length_table1941);
                high_bound();
                state._fsp--;

                // com\\legstar\\cobol\\CobolStructureEmitter.g:284:42: ( key_clause )*
                loop21:
                while (true) {
                    int alt21 = 2;
                    int LA21_0 = input.LA(1);
                    if ((LA21_0 == KEY)) {
                        alt21 = 1;
                    }

                    switch (alt21) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:284:42: key_clause
                        {
                            pushFollow(FOLLOW_key_clause_in_variable_length_table1943);
                            key_clause();
                            state._fsp--;

                        }
                        break;

                        default:
                            break loop21;
                    }
                }

                // com\\legstar\\cobol\\CobolStructureEmitter.g:284:54: ( index_clause )*
                loop22:
                while (true) {
                    int alt22 = 2;
                    int LA22_0 = input.LA(1);
                    if ((LA22_0 == INDEX)) {
                        alt22 = 1;
                    }

                    switch (alt22) {
                        case 1:
                            // com\\legstar\\cobol\\CobolStructureEmitter.g:284:54: index_clause
                        {
                            pushFollow(FOLLOW_index_clause_in_variable_length_table1946);
                            index_clause();
                            state._fsp--;

                        }
                        break;

                        default:
                            break loop22;
                    }
                }

                match(input, Token.UP, null);

            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "variable_length_table"


    // $ANTLR start "high_bound"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:287:1: high_bound : ^( HBOUND INT ( depending_on )? ) ;
    public final void high_bound() throws RecognitionException {
        CommonTree INT28 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:288:5: ( ^( HBOUND INT ( depending_on )? ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:288:9: ^( HBOUND INT ( depending_on )? )
            {
                match(input, HBOUND, FOLLOW_HBOUND_in_high_bound1972);
                match(input, Token.DOWN, null);
                INT28 = (CommonTree) match(input, INT, FOLLOW_INT_in_high_bound1974);
                // com\\legstar\\cobol\\CobolStructureEmitter.g:288:22: ( depending_on )?
                int alt23 = 2;
                int LA23_0 = input.LA(1);
                if ((LA23_0 == DEPENDINGON)) {
                    alt23 = 1;
                }
                switch (alt23) {
                    case 1:
                        // com\\legstar\\cobol\\CobolStructureEmitter.g:288:22: depending_on
                    {
                        pushFollow(FOLLOW_depending_on_in_high_bound1976);
                        depending_on();
                        state._fsp--;

                    }
                    break;

                }

                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setMaxOccurs(Integer.parseInt((INT28 != null ? INT28.getText() : null)));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "high_bound"


    // $ANTLR start "depending_on"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:292:1: depending_on : ^( DEPENDINGON DATA_NAME ) ;
    public final void depending_on() throws RecognitionException {
        CommonTree DATA_NAME29 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:293:5: ( ^( DEPENDINGON DATA_NAME ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:293:9: ^( DEPENDINGON DATA_NAME )
            {
                match(input, DEPENDINGON, FOLLOW_DEPENDINGON_in_depending_on2012);
                match(input, Token.DOWN, null);
                DATA_NAME29 = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_depending_on2014);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setDependingOn((DATA_NAME29 != null ? DATA_NAME29.getText() : null));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "depending_on"


    // $ANTLR start "low_bound"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:297:1: low_bound : ^( LBOUND INT ) ;
    public final void low_bound() throws RecognitionException {
        CommonTree INT30 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:298:5: ( ^( LBOUND INT ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:298:9: ^( LBOUND INT )
            {
                match(input, LBOUND, FOLLOW_LBOUND_in_low_bound2047);
                match(input, Token.DOWN, null);
                INT30 = (CommonTree) match(input, INT, FOLLOW_INT_in_low_bound2049);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.setMinOccurs(Integer.parseInt((INT30 != null ? INT30.getText() : null)));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "low_bound"


    // $ANTLR start "key_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:302:1: key_clause : ( ^( KEY ASCENDING_KEYWORD DATA_NAME ) | ^( KEY DESCENDING_KEYWORD DATA_NAME ) );
    public final void key_clause() throws RecognitionException {
        CommonTree DATA_NAME31 = null;
        CommonTree DATA_NAME32 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:303:5: ( ^( KEY ASCENDING_KEYWORD DATA_NAME ) | ^( KEY DESCENDING_KEYWORD DATA_NAME ) )
            int alt24 = 2;
            int LA24_0 = input.LA(1);
            if ((LA24_0 == KEY)) {
                int LA24_1 = input.LA(2);
                if ((LA24_1 == DOWN)) {
                    int LA24_2 = input.LA(3);
                    if ((LA24_2 == ASCENDING_KEYWORD)) {
                        alt24 = 1;
                    } else if ((LA24_2 == DESCENDING_KEYWORD)) {
                        alt24 = 2;
                    } else {
                        int nvaeMark = input.mark();
                        try {
                            for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
                                input.consume();
                            }
                            NoViableAltException nvae =
                                    new NoViableAltException("", 24, 2, input);
                            throw nvae;
                        } finally {
                            input.rewind(nvaeMark);
                        }
                    }

                } else {
                    int nvaeMark = input.mark();
                    try {
                        input.consume();
                        NoViableAltException nvae =
                                new NoViableAltException("", 24, 1, input);
                        throw nvae;
                    } finally {
                        input.rewind(nvaeMark);
                    }
                }

            } else {
                NoViableAltException nvae =
                        new NoViableAltException("", 24, 0, input);
                throw nvae;
            }

            switch (alt24) {
                case 1:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:303:9: ^( KEY ASCENDING_KEYWORD DATA_NAME )
                {
                    match(input, KEY, FOLLOW_KEY_in_key_clause2089);
                    match(input, Token.DOWN, null);
                    match(input, ASCENDING_KEYWORD, FOLLOW_ASCENDING_KEYWORD_in_key_clause2091);
                    DATA_NAME31 = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_key_clause2093);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.addAscendingKey((DATA_NAME31 != null ? DATA_NAME31.getText() : null));
                }
                break;
                case 2:
                    // com\\legstar\\cobol\\CobolStructureEmitter.g:305:9: ^( KEY DESCENDING_KEYWORD DATA_NAME )
                {
                    match(input, KEY, FOLLOW_KEY_in_key_clause2115);
                    match(input, Token.DOWN, null);
                    match(input, DESCENDING_KEYWORD, FOLLOW_DESCENDING_KEYWORD_in_key_clause2117);
                    DATA_NAME32 = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_key_clause2119);
                    match(input, Token.UP, null);

                    data_entry_stack.peek().dataEntry.addDescendingKey((DATA_NAME32 != null ? DATA_NAME32.getText() : null));
                }
                break;

            }
        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "key_clause"


    // $ANTLR start "index_clause"
    // com\\legstar\\cobol\\CobolStructureEmitter.g:309:1: index_clause : ^( INDEX DATA_NAME ) ;
    public final void index_clause() throws RecognitionException {
        CommonTree DATA_NAME33 = null;

        try {
            // com\\legstar\\cobol\\CobolStructureEmitter.g:310:5: ( ^( INDEX DATA_NAME ) )
            // com\\legstar\\cobol\\CobolStructureEmitter.g:310:9: ^( INDEX DATA_NAME )
            {
                match(input, INDEX, FOLLOW_INDEX_in_index_clause2152);
                match(input, Token.DOWN, null);
                DATA_NAME33 = (CommonTree) match(input, DATA_NAME, FOLLOW_DATA_NAME_in_index_clause2154);
                match(input, Token.UP, null);

                data_entry_stack.peek().dataEntry.addIndex((DATA_NAME33 != null ? DATA_NAME33.getText() : null));
            }

        } catch ( RecognitionException re ) {
            reportError(re);
            recover(input, re);
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "index_clause"

    // Delegated rules


    public static final BitSet FOLLOW_data_entry_in_cobdata57 = new BitSet(new long[]{0x0000000000000002L, 0x0080000000C00000L});
    public static final BitSet FOLLOW_data_description_entry_in_data_entry98 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rename_description_entry_in_data_entry108 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_condition_description_entry_in_data_entry118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATA_ITEM_in_data_description_entry140 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_data_item_level_in_data_description_entry142 = new BitSet(new long[]{0x0000000000000008L, 0xD4C2204CC1E00000L, 0x0000000000000001L});
    public static final BitSet FOLLOW_data_item_name_in_data_description_entry144 = new BitSet(new long[]{0x0000000000000008L, 0xD4C2004CC1E00000L, 0x0000000000000001L});
    public static final BitSet FOLLOW_clauses_in_data_description_entry147 = new BitSet(new long[]{0x0000000000000008L, 0xD4C2004CC1E00000L, 0x0000000000000001L});
    public static final BitSet FOLLOW_data_entry_in_data_description_entry151 = new BitSet(new long[]{0x0000000000000008L, 0x0080000000C00000L});
    public static final BitSet FOLLOW_LEVEL_in_data_item_level179 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATA_ITEM_LEVEL_in_data_item_level181 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAME_in_data_item_name212 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATA_NAME_in_data_item_name214 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RENAME_in_rename_description_entry249 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_rename_level_in_rename_description_entry251 = new BitSet(new long[]{0x0000000000000000L, 0x0000200000000000L});
    public static final BitSet FOLLOW_data_item_name_in_rename_description_entry253 = new BitSet(new long[]{0x0000000000000000L, 0x0020100000000000L});
    public static final BitSet FOLLOW_rename_subject_literal_in_rename_description_entry256 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_rename_subject_range_in_rename_description_entry260 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LEVEL_in_rename_level287 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_RENAMES_LEVEL_in_rename_level289 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LITERAL_in_rename_subject_literal320 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATA_NAME_in_rename_subject_literal322 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RANGE_in_rename_subject_range353 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATA_NAME_in_rename_subject_range357 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_DATA_NAME_in_rename_subject_range361 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CONDITION_in_condition_description_entry394 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_condition_level_in_condition_description_entry396 = new BitSet(new long[]{0x0000000000000000L, 0x0000200000000000L});
    public static final BitSet FOLLOW_data_item_name_in_condition_description_entry398 = new BitSet(new long[]{0x0000000000000000L, 0x0020100000000000L});
    public static final BitSet FOLLOW_condition_subject_literal_in_condition_description_entry401 = new BitSet(new long[]{0x0000000000000008L, 0x0020100000000000L});
    public static final BitSet FOLLOW_condition_subject_range_in_condition_description_entry405 = new BitSet(new long[]{0x0000000000000008L, 0x0020100000000000L});
    public static final BitSet FOLLOW_LEVEL_in_condition_level429 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CONDITION_LEVEL_in_condition_level431 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LITERAL_in_condition_subject_literal462 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_literal_in_condition_subject_literal464 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RANGE_in_condition_subject_range495 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_literal_in_condition_subject_range499 = new BitSet(new long[]{0x2004B04C00400050L, 0x00000001020C0208L});
    public static final BitSet FOLLOW_literal_in_condition_subject_range503 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_redefines_clause_in_clauses536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_blank_when_zero_clause_in_clauses546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_external_clause_in_clauses556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_global_clause_in_clauses566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_group_usage_clause_in_clauses576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_justified_clause_in_clauses586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_occurs_clause_in_clauses596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_picture_clause_in_clauses606 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_sign_clause_in_clauses616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_synchronized_clause_in_clauses626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_usage_clause_in_clauses636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_value_clause_in_clauses646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_date_format_clause_in_clauses656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REDEFINES_in_redefines_clause677 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATA_NAME_in_redefines_clause679 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_BLANKWHENZERO_in_blank_when_zero_clause709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_EXTERNAL_in_external_clause738 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GLOBAL_in_global_clause767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GROUPUSAGENATIONAL_in_group_usage_clause796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_JUSTIFIEDRIGHT_in_justified_clause825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_fixed_length_table_in_occurs_clause854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_variable_length_table_in_occurs_clause864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PICTURE_in_picture_clause884 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_PICTURESTRING_in_picture_clause886 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SIGN_in_sign_clause917 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_sign_leading_clause_in_sign_clause919 = new BitSet(new long[]{0x0000000000000008L, 0x2000000000000000L});
    public static final BitSet FOLLOW_sign_trailing_clause_in_sign_clause922 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LEADING_in_sign_leading_clause958 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_separate_clause_in_sign_leading_clause960 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TRAILING_in_sign_trailing_clause996 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_separate_clause_in_sign_trailing_clause998 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SEPARATE_in_separate_clause1033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SYNCHRONIZED_in_synchronized_clause1063 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1103 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_BINARY_in_usage_clause1105 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1127 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_SINGLEFLOAT_in_usage_clause1129 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1151 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DOUBLEFLOAT_in_usage_clause1153 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1175 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_PACKEDDECIMAL_in_usage_clause1177 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1199 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_NATIVEBINARY_in_usage_clause1201 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1223 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DISPLAY_in_usage_clause1225 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1247 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DISPLAY1_in_usage_clause1249 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1271 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_INDEX_in_usage_clause1273 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1295 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_NATIONAL_in_usage_clause1297 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1319 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_POINTER_in_usage_clause1321 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1343 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_PROCEDUREPOINTER_in_usage_clause1345 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_USAGE_in_usage_clause1367 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_FUNCTIONPOINTER_in_usage_clause1369 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_VALUE_in_value_clause1400 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_value_clause_literal_in_value_clause1402 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_literal_in_value_clause_literal1426 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOAT_LITERAL_in_literal1463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DECIMAL_LITERAL_in_literal1483 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_literal1503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SIGNED_INT_in_literal1523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALPHANUM_LITERAL_STRING_in_literal1543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HEX_LITERAL_STRING_in_literal1563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ZERO_LITERAL_STRING_in_literal1583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DBCS_LITERAL_STRING_in_literal1603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NATIONAL_LITERAL_STRING_in_literal1623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NATIONAL_HEX_LITERAL_STRING_in_literal1643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ZERO_CONSTANT_in_literal1663 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPACE_CONSTANT_in_literal1683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HIGH_VALUE_CONSTANT_in_literal1703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOW_VALUE_CONSTANT_in_literal1723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTE_CONSTANT_in_literal1743 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALL_CONSTANT_in_literal1763 = new BitSet(new long[]{0x2004100800000040L, 0x0000000000040200L});
    public static final BitSet FOLLOW_ALPHANUM_LITERAL_STRING_in_literal1768 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ZERO_CONSTANT_in_literal1774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPACE_CONSTANT_in_literal1780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HIGH_VALUE_CONSTANT_in_literal1786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOW_VALUE_CONSTANT_in_literal1792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTE_CONSTANT_in_literal1798 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NULL_CONSTANT_in_literal1804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NULL_CONSTANT_in_literal1825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATEFORMAT_in_date_format_clause1855 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATE_PATTERN_in_date_format_clause1857 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FIXEDARRAY_in_fixed_length_table1892 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_high_bound_in_fixed_length_table1894 = new BitSet(new long[]{0x0000000000000008L, 0x000000A000000000L});
    public static final BitSet FOLLOW_key_clause_in_fixed_length_table1896 = new BitSet(new long[]{0x0000000000000008L, 0x000000A000000000L});
    public static final BitSet FOLLOW_index_clause_in_fixed_length_table1899 = new BitSet(new long[]{0x0000000000000008L, 0x0000002000000000L});
    public static final BitSet FOLLOW_VARARRAY_in_variable_length_table1936 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_low_bound_in_variable_length_table1938 = new BitSet(new long[]{0x0000000000000000L, 0x0000001000000000L});
    public static final BitSet FOLLOW_high_bound_in_variable_length_table1941 = new BitSet(new long[]{0x0000000000000008L, 0x000000A000000000L});
    public static final BitSet FOLLOW_key_clause_in_variable_length_table1943 = new BitSet(new long[]{0x0000000000000008L, 0x000000A000000000L});
    public static final BitSet FOLLOW_index_clause_in_variable_length_table1946 = new BitSet(new long[]{0x0000000000000008L, 0x0000002000000000L});
    public static final BitSet FOLLOW_HBOUND_in_high_bound1972 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_INT_in_high_bound1974 = new BitSet(new long[]{0x0000000000000008L, 0x0000000004000000L});
    public static final BitSet FOLLOW_depending_on_in_high_bound1976 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DEPENDINGON_in_depending_on2012 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATA_NAME_in_depending_on2014 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LBOUND_in_low_bound2047 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_INT_in_low_bound2049 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_KEY_in_key_clause2089 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ASCENDING_KEYWORD_in_key_clause2091 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_DATA_NAME_in_key_clause2093 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_KEY_in_key_clause2115 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DESCENDING_KEYWORD_in_key_clause2117 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_DATA_NAME_in_key_clause2119 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_INDEX_in_index_clause2152 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DATA_NAME_in_index_clause2154 = new BitSet(new long[]{0x0000000000000008L});
}

