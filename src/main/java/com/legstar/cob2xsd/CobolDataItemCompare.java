package com.legstar.cob2xsd;

import com.legstar.cobol.model.CobolDataItem;

import java.util.*;

public class CobolDataItemCompare implements Comparator<CobolDataItem> {
    public int compare(CobolDataItem o1, CobolDataItem o2) {

        return new Integer(o1.getSrceLine()).compareTo(new Integer(o2.getSrceLine()));
    }
}
