/*******************************************************************************
 * Copyright (c) 2010 LegSem.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Contributors:
 *     LegSem - initial API and implementation
 ******************************************************************************/
package com.legstar.cob2xsd;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.legstar.antlr.RecognizerException;
import com.legstar.cobol.model.CobolNewDataItem;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.legstar.cobol.model.CobolDataItem;

import java.util.*;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

/**
 * COBOL structure to XML schema executable.
 * <p>
 * This is the main class for the executable jar. It takes options from the
 * command line and calls the {@link Cob2Xsd} API.
 * <p>
 * Usage: <code>
 * java -jar legstar-cob2xsd-x.y.z-exe.jar -i&lt;input file or folder&gt; -o&lt;output folder&gt;
 * </code>
 */
public class CopyBookParser {

    /**
     * The version properties file name.
     */
    private static final String VERSION_FILE_NAME = "/com/legstar/cob2xsd/version.properties";

    /**
     * The default input.
     */
    private static final String DEFAULT_INPUT_FOLDER = "cobol";

    /**
     * The default output.
     */
    private static final String DEFAULT_OUTPUT_FOLDER = "schema";

    /**
     * The default output.
     */
    private static final String DEFAULT_CONFIG_FILE = "conf/cob2xsd.properties";

    /**
     * A file containing parameters.
     */
    private File _configFile;

    /**
     * A file or folder containing COBOL code to translate to XSD. Defaults to
     * cobol relative folder.
     */
    private File _input;

    /**
     * A folder containing translated XML Schema. Defaults to schema relative
     * folder.
     */
    private File _output;

    /**
     * With this option turned on, the target namespace from the model will be
     * appended the input file base file name.
     */
    private boolean _appendBaseFileNameToNamespace;

    /**
     * Set of translation options to use.
     */
    private Cob2XsdModel _model;

    /**
     * Line separator (OS specific).
     */
    public static final String LS = System.getProperty("line.separator");

    /**
     * Logger.
     */
    private final Log _log = LogFactory.getLog(getClass());


    public CopyBookParser(final String cobolInput) {

        this.setInput(cobolInput);
    }

    /**
     * @param args translator options. Provides help if no arguments passed.
     */
    public static void main(final String[] args) {
        String cobolFile1 = "C:\\Users\\bwamutala.miantezila\\Desktop\\Cigma\\legstar-cob2xsd-master\\src\\test\\resources\\cobol\\ALLTYPES";
        String cobolFile2 = "C:\\Users\\bwamutala.miantezila\\Desktop\\Cigma\\legstar-cob2xsd-master\\src\\test\\resources\\cobol\\newcopybook-indented-01.cpy";
        String cobolFile3 = "C:/Users/bwamutala.miantezila/Desktop/MyStuff/Book/legstar.avro-master/legstar.avro.generator/src/main/resources/cobol/T1CONTXT";
        String cobolFile4 = "C:\\Users\\bwamutala.miantezila\\Desktop\\Cigma\\ECR_Record.cpy";

        CopyBookParser main = new CopyBookParser(cobolFile2);
        Map<String, CobolNewDataItem> mapTree = main.executeMap();
        System.out.println();
        System.out.println(main.mapToJson(mapTree));


    }


    private String convertToJson(CobolDataItem cbl) {

        String jsonInString = null;
        ObjectMapper mapper = new ObjectMapper();
        //Object to JSON in String
        try {
            jsonInString = mapper.writeValueAsString(cbl);
        } catch ( JsonProcessingException e ) {
            e.printStackTrace();
        }
        return jsonInString;
    }


    public String mapToJson(Map<String, CobolNewDataItem> cbl) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            // Convert object to JSON string
            jsonInString = mapper.writeValueAsString(cbl);

        } catch ( JsonGenerationException e ) {
            e.printStackTrace();
        } catch ( JsonMappingException e ) {
            e.printStackTrace();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        return jsonInString;
    }

    /**
     * Process command line options and run translator.
     * <p>
     * If no options are passed, prints the help. Help is also printed if the
     * command line options are invalid.
     *
     * @param args translator options
     */
    public String execute(final String[] args) {
        String retExc = null;
        try {
            //   Options options = createOptions();
            //  if (collectOptions(options, args)) {
            setDefaults();
            loadModel();
            retExc = execute(getInput(), getOutput());
            //  }
        } catch ( Exception e ) {
            _log.error("COBOL to Xsd translation failure", e);
            throw new RuntimeException(e);
        }
        return retExc;
    }


    private Map<String, CobolNewDataItem> executeMap() {

        Map<String, CobolNewDataItem> retExc = null;
        try {

            setDefaults();
            loadModel();
            retExc = executeMap(getInput(), getOutput());
        } catch ( Exception e ) {
            _log.error("COBOL to Xsd translation failure", e);
            throw new RuntimeException(e);
        }
        return retExc;

    }


    /**
     * Take arguments received on the command line and setup corresponding
     * options.
     * <p>
     * No arguments is valid. It means use the defaults.
     *
     * @param options the expected options
     * @param args    the actual arguments received on the command line
     * @return true if arguments were valid
     * @throws Exception if something goes wrong while parsing arguments
     */
    protected boolean collectOptions(final Options options, final String[] args)
            throws Exception {
        if (args != null && args.length > 0) {
            CommandLineParser parser = new PosixParser();
            CommandLine line = parser.parse(options, args);
            return processLine(line, options);
        }
        return true;
    }

    /**
     * Make sure mandatory parameters have default values.
     */
    protected void setDefaults() {
        if (getConfigFile() == null) {
            setConfigFile(DEFAULT_CONFIG_FILE);
        }
        if (getInput() == null) {
            setInput(DEFAULT_INPUT_FOLDER);
        }
        if (getOutput() == null) {
            setOutput(DEFAULT_OUTPUT_FOLDER);
        }
    }

    /**
     * A configuration file is expected. We load it into the generator model.
     *
     * @throws XsdGenerationException if configuration file missing or file
     *                                corrupt
     */
    protected void loadModel() throws XsdGenerationException {
        try {
            if (getConfigFile() == null) {
                _model = new Cob2XsdModel();
            } else {
                Properties config = new Properties();
                config.load(new FileInputStream(getConfigFile()));
                _model = new Cob2XsdModel(config);
            }
        } catch ( FileNotFoundException e ) {
            throw new XsdGenerationException(e);
        } catch ( IOException e ) {
            throw new XsdGenerationException(e);
        }
    }

    /**
     * @param options options available
     * @throws Exception if help cannot be produced
     */
    protected void produceHelp(final Options options) throws Exception {
        HelpFormatter formatter = new HelpFormatter();
        String version = getVersion();
        formatter.printHelp(
                "java -jar legstar-cob2xsd-"
                        + version.substring(0, version.indexOf(' '))
                        + "-exe.jar followed by:", options);
    }

    /**
     * @return the command line options
     */
    protected Options createOptions() {
        Options options = new Options();

        Option version = new Option("v", "version", false,
                "print the version information and exit");
        options.addOption(version);

        Option help = new Option("h", "help", false,
                "print the options available");
        options.addOption(help);

        Option configFile = new Option("c", "config", true,
                "path to configuration file");
        options.addOption(configFile);

        Option input = new Option("i", "input", true,
                "file or folder holding the COBOL code to translate."
                        + " Name is relative or absolute");
        options.addOption(input);

        Option output = new Option("o", "output", true,
                "folder or file receiving the translated XML schema");
        options.addOption(output);

        Option appendBaseFileNameToNamespace = new Option("a",
                "appendBaseFileNameToNamespace", false,
                "add input base file name to namespace");
        options.addOption(appendBaseFileNameToNamespace);

        return options;
    }

    /**
     * Process the command line options selected.
     *
     * @param line    the parsed command line
     * @param options available
     * @return false if processing needs to stop, true if its ok to continue
     * @throws Exception if line cannot be processed
     */
    protected boolean processLine(final CommandLine line, final Options options)
            throws Exception {
        if (line.hasOption("version")) {
            System.out.println("version " + getVersion());
            return false;
        }
        if (line.hasOption("help")) {
            produceHelp(options);
            return false;
        }
        if (line.hasOption("config")) {
            setConfigFile(line.getOptionValue("config").trim());
        }
        if (line.hasOption("input")) {
            setInput(line.getOptionValue("input").trim());
        }
        if (line.hasOption("output")) {
            setOutput(line.getOptionValue("output").trim());
        }

        if (line.hasOption("appendBaseFileNameToNamespace")) {
            _appendBaseFileNameToNamespace = true;
        }

        return true;
    }

    /**
     * Translate a single file or all files from an input folder. Place results
     * in the output folder.
     *
     * @param input  the input COBOL file or folder
     * @param target the output folder or file where XML schema file must go
     * @throws XsdGenerationException if XML schema cannot be generated
     */
    protected String execute(final File input, final File target)
            throws XsdGenerationException {
        String retXml = null;
        try {
            _log.info("Started translation from COBOL to XML Schema");
            _log.info("Taking COBOL from      : " + input);
            _log.info("Output XML Schema to   : " + target);
            _log.info("Options in effect      : " + getModel().toString());
            _log.info("Append base file name  : "
                    + _appendBaseFileNameToNamespace);

            if (input.isFile()) {
                if (FilenameUtils.getExtension(target.getPath()).length() == 0) {
                    FileUtils.forceMkdir(target);
                }
                retXml = translate(input, target);
            } else {
                FileUtils.forceMkdir(target);
                for (File cobolFile : input.listFiles()) {
                    if (cobolFile.isFile()) {
                        translate(cobolFile, target);
                    }
                }
            }
            _log.info("Finished translation");
        } catch ( IOException e ) {
            throw new XsdGenerationException(e);
        }
        return retXml;
    }


    protected Map<String, CobolNewDataItem> executeMap(File input, File target) throws XsdGenerationException {

        Map<String, CobolNewDataItem> retXml = null;
        try {
            _log.info("Started translation from COBOL to XML Schema");
            _log.info("Taking COBOL from      : " + input);
            _log.info("Output XML Schema to   : " + target);
            _log.info("Options in effect      : " + getModel().toString());
            _log.info("Append base file name  : "
                    + _appendBaseFileNameToNamespace);

            if (input.isFile()) {
                if (FilenameUtils.getExtension(target.getPath()).length() == 0) {
                    FileUtils.forceMkdir(target);
                }
                retXml = translateMap(input, target);
            } else {
                FileUtils.forceMkdir(target);
                for (File cobolFile : input.listFiles()) {
                    if (cobolFile.isFile()) {
                        translateMap(cobolFile, target);
                    }
                }
            }
            _log.info("Finished translation");
        } catch ( IOException e ) {
            throw new XsdGenerationException(e);
        }
        return retXml;
    }


    protected Map<String, CobolNewDataItem> translateMap(final File cobolFile, final File target) throws XsdGenerationException {
        Map<String, CobolNewDataItem> retStr = null;
        try {
            Cob2XsdIO cob2XsdIO = new Cob2XsdIO(getModel());
            retStr = cob2XsdIO.translateMap(cobolFile, target, _appendBaseFileNameToNamespace);
        } catch ( RecognizerException e ) {
            throw new XsdGenerationException(e);
        }
        return retStr;
    }


    /**
     * Translates a single COBOL source file.
     *
     * @param cobolFile COBOL source file
     * @param target    target file or folder
     * @throws XsdGenerationException if parser fails
     */
    protected String translate(final File cobolFile, final File target)
            throws XsdGenerationException {
        try {
            Cob2XsdIO cob2XsdIO = new Cob2XsdIO(getModel());
            return cob2XsdIO.translate(cobolFile, target,
                    _appendBaseFileNameToNamespace);
        } catch ( RecognizerException e ) {
            throw new XsdGenerationException(e);
        }
    }


  /*  protected List<CobolDataItem> translate2(final File cobolFile, final File target)
            throws XsdGenerationException {
        List<CobolDataItem> retStr = null;
        try {
            Cob2XsdIO cob2XsdIO = new Cob2XsdIO(getModel());
            retStr = cob2XsdIO.translate2(cobolFile, target, _appendBaseFileNameToNamespace);
        } catch (RecognizerException e) {
            throw new XsdGenerationException(e);
        }
        return retStr;
    }*/

    /**
     * Pick up the version from the properties file.
     *
     * @return the product version
     * @throws IOException if version cannot be identified
     */
    protected String getVersion() throws IOException {
        InputStream stream = null;
        try {
            Properties version = new Properties();
            stream = CopyBookParser.class.getResourceAsStream(VERSION_FILE_NAME);
            version.load(stream);
            return version.getProperty("version");
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }

    /**
     * @return the file containing parameters
     */
    public File getConfigFile() {
        return _configFile;
    }

    /**
     * Check the config parameter and keep it only if it is valid.
     * <p>
     * If the default properties file is not there, we run with the defaults.
     *
     * @param config a file name (relative or absolute)
     */
    public void setConfigFile(final String config) {
        if (config == null) {
            throw (new IllegalArgumentException(
                    "You must provide a configuration file"));
        }
        File file = new File(config);
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new IllegalArgumentException("Folder " + config
                        + " is not a configuration file");
            }
        } else {
            if (config.equals(DEFAULT_CONFIG_FILE)) {
                file = null;
            } else {
                throw new IllegalArgumentException("Configuration file "
                        + config + " not found");
            }
        }
        setConfigFile(file);
    }

    /**
     * @param configFile the file containing parameters to set
     */
    public void setConfigFile(final File configFile) {
        _configFile = configFile;
    }

    /**
     * Check the input parameter and keep it only if it is valid.
     *
     * @param input a file or folder name (relative or absolute)
     */
    public void setInput(final String input) {
        if (input == null) {
            throw (new IllegalArgumentException(
                    "You must provide a COBOL source folder or file"));
        }
        File file = new File(input);
        if (file.exists()) {
            if (file.isDirectory() && file.list().length == 0) {
                throw new IllegalArgumentException("Folder " + input
                        + " is empty");
            }
        } else {
            throw new IllegalArgumentException("Input file or folder " + input
                    + " not found");
        }
        _input = file;
    }

    /**
     * Check the output parameter and keep it only if it is valid.
     *
     * @param output a file or folder name (relative or absolute)
     */
    public void setOutput(final String output) {
        if (output == null) {
            throw (new IllegalArgumentException(
                    "You must provide a target directory or file"));
        }
        _output = new File(output);
    }

    /**
     * Gather all parameters into a model object.
     *
     * @return a parameter model to be used throughout all code
     */
    public Cob2XsdModel getModel() {
        return _model;
    }

    /**
     * @return the file or folder containing COBOL code to translate to XSD
     */
    public File getInput() {
        return _input;
    }

    /**
     * @return the folder containing translated XML Schema
     */
    public File getOutput() {
        return _output;
    }

}
